README
------
This is the repository for lexical encoding of French MWEs in XMG. 

TOOLS
-------
 * [XMG-2](https://github.com/spetitjean/XMG-2/wiki) metagrammatical framework
 * [FrenchTAG](https://sourcesup.renater.fr/xmg/frenchmetagrammar/) - the XMG (v. 1) metagrammar for French
 * [pythreeview](https://gitlab.com/parmenti/pytreeview) - tree visualiser
 * [Tulipa](https://sourcesup.cru.fr/tulipa/wiki.html) TAG parser
 * [old XMG environment](https://sourcesup.renater.fr/xmg/)
 * simple tree visualiser embedded in XMG: `xmg gui tag-grammar`

INSTALLATION
-------
 * Install XMG following the [guide](https://github.com/spetitjean/XMG-2/wiki)
 * Clone this Gitlab repo to a local directory (choose a name and use it as <tt>your-local-repo</tt>)
       * `git clone git@gitlab.com:agata.savary/mwe-xmg.git your-local-repo`
 * Install the tree viewer [pytreeview](https://gitlab.com/parmenti/pytreeview) (by Yannick)
 * Install [Tulipa](https://sourcesup.renater.fr/frs/?group_id=417) by downloading the .jar
	* `mkdir tulipa-folder`
	* `cd tulipa-folder`
	* Yannick's version: `wget https://sourcesup.renater.fr/frs/shownotes.php?release_id=2842 #replace the URL with the current release`
	* Simon's version: `wget https://github.com/spetitjean/TuLiPA-frames/blob/master/TuLiPA-frames.jar`
	* make an alias 
		* Yannick's version: `echo 'alias tulipa='java -jar tulipa-folder/TuLiPA-2.0.4.jar' >> ~/.bashrc #replace the .jar name with the current version`
		* Simon's version: `echo 'alias tulipa='java -jar ~/Narzedzia/Tulipa/TuLiPA-frames.jar' >> ~/.bashrc

UPDATING
-------
* Update XMG:
	* `cd xmg-folder`
	* `git pull`
	* Install new contributions (if any), e.g.
		* `xmg install contributions/lemmamg/` 
		* `xmg install contributions/lexCompiler/`
		* `xmg install contributions/morphomg/`
		* `xmg install contributions/mphCompiler/`
	* Compile updated compilers (if any)
	    *  `./reinstall_all.sh`
<!-- 		* `cd contributions/lexCompiler/compilers/lex/`
[comment]: 		* `xmg build`
[comment]:		* `cd ../../../../`
[comment]:		* `cd contributions/mphCompiler/compilers/mph/`
[comment]:		* `xmg build`
-->
* Update the metagrammar:
	* `cd your-local-repo`
	* `git pull`
* Update Tulipa
	* `cd tulipa-folder`
	* Yannick's version: `wget https://sourcesup.renater.fr/frs/shownotes.php?release_id=2842` #replace the URL with the current release 
	* Simon's version: `wget https://github.com/spetitjean/TuLiPA-frames/blob/master/TuLiPA-frames.jar`
	* update the alias in needed (see above)

FrenchTAG
-------
 * The currently developed version of the MWE-aware metagrammar is placed in `dev/`
     * Original FrenchTAG files are the <tt>*.mg</tt> files whih do not start with <tt>mwe</tt> e.g. <tt>PredArgs.mg</tt>
     * The classes added for the purpose of MWE encoding are placed in the <tt>*.mg</tt> files whose names start with <tt>mwe</tt>, e.g. <tt>mwePredArgsLex.mg</tt>
     * The classes to valuate are in <tt>mweValuation.mg</tt>
     * The lexicon is in <tt>lemmes.mg</tt> and <tt>morphs.mg</tt>
 * Previous working versions of the MWE-aware metagrammar are in `named-versions/`

Compilation and visualisation of the metagrammar
-------
 * `cd dev/`
 * `xmg compile lex mweLemmasValuation.mg --force` #Compiling lemmas
 * `xmg compile mph mweMorphsValuation.mg --force` #Compiling inflected forms
 * `xmg compile synsem mweValuation.mg --force --notype` #Compiling syntax
 * `pytreeview --mode WEB -i mweValuation.xml`
 * `firefox mweValuation-pytreeview/index.html &`

Parsing with the compiled grammar and Tulipa
-------
 * Help: `tulipa -h`
 * Parsing in the GUI and visualising the derivation tree and the derived tree
	* `tulipa &`
	* select Verbose mode
	* select 
		* Grammar: mweValuation.xml
		* Lemmas: lemmes.xml
		* Morphological entries: morphs.xml
		* Axiom: s
		* Sentence: Jean prend la porte #Replace with the sentence to parse
		* Parse
		* View the idiomatic and the compositional readings of the sentence
 * Parsing in command line and visualising the derivation tree and the derived tree (verbose mode)
	* `tulipa -cyktag -v -g mweValuation.xml -l lemmes.xml -m morphs.xml -a s -s "Jean prend la porte"` #Replace with the sentence to parse

LICENSE
-------
FrenchTAG and mweFrechTAG are distributed under the terms of the [Creative Commons v4 license](http://creativecommons.org/licenses/by/4.0/).


CONTACT
------
agata.savary@univ-tours.fr

