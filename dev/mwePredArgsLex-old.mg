%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized predicate arguments
% These classes are implement lexicalized arguments of verbs
% 16/02/2018
% by Agata Savary and Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
% LexDet + LexNoun subject: la nuit (tombe)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubjectLexDetN
declare
    ?lexSubj ?lexNP
{
    ?lexSubj = mweSubject[];
    ?lexNP = mweDetNoun[SubjDetNode,SubjNode]; %Lexicalized Det+N phrase 
    ?lexSubj.LexSubj =?lexNP.xLexDetN        %Unifying the object with the lexicalized Det+N phrase
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% LexDet + LexNoun passive subject: la page (a été tournée)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalSubjectPassiveLexDetN
import
	mweCanonicalSubject[]
declare
	?lexNP
{
	?lexNP = mweDetNoun[ObjDetNode,ObjNode];   %Lexicalized Det+N phrase
	?xSubj=?lexNP.xN		%Unifying the subject with the lexicalized Det+N phrase
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% LexNoun subject: pierre (qui roule)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubjectLexN
declare
    ?lexSubj ?lexNP
{
    ?lexSubj = mweSubject[];
    ?lexNP = mweNoun[SubjNode]; %Lexicalized Det+N phrase 
    ?lexSubj.LexSubj =?lexNP.xLexDetN        %Unifying the object with the lexicalized Det+N phrase
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DIRECT OBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% LexDet + LexNoun object: tourner la page
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObjectLexDetN
declare
    ?lexObj ?lexNP
{
    ?lexObj = mweObject[];
    ?lexNP = mweDetNoun[ObjDetNode,ObjNode]; %Lexicalized Det+N phrase 
    ?lexObj.LexObj =?lexNP.xLexDetN        %Unifying the object with the lexicalized Det+N phrase
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% LexNoun + LexAdj object: faire profil bas
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObjectLexNAdj
declare
    ?lexObj ?lexNP
{
    ?lexObj = mweObject[];
    ?lexNP = mweNounAdj[ObjNode,ObjAdjNode]; %Lexicalized N+Adj phrase 
    ?lexObj.LexObj =?lexNP.xLexNAdj        %Unifying the object with the lexicalized N+Adj phrase
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% LexNoun object: faire face
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObjectLexN
declare
    ?lexObj ?lexNP
{
    ?lexObj = mweObject[];
    ?lexNP = mweNoun[ObjNode,ObjAdjNode]; %Lexicalized N+Adj phrase 
    ?lexObj.LexObj =?lexNP.xLexNAdj        %Unifying the object with the lexicalized N+Adj phrase
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INDIRECT OBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical indirect object lexicalized with a Det-N group
%%%%%%%%%%%%%%%%%%%%%%%%
class mweIObjectLexDetN
declare 
	?lexIObj ?lexNP
{
	?lexIObj = mweIobject[];
	?lexNP = mweDetNoun[IObjDetNode,IObjNode];   %Lexicalized Det+N phrase
	?lexIObj.xArg=?lexNP.xLexDetN		%Unifying the indirect object with the lexicalized Det+N phrase
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical indirect object lexicalized with a bare N
%%%%%%%%%%%%%%%%%%%%%%%%
class mweIObjectLexN
declare 
	?lexIObj ?lexNP
{
	?lexIObj = mweIobject[];
	?lexNP = mweNoun[IObjNode];   %Lexicalized Det+N phrase
	?lexIObj.xArg=?lexNP.xLexDetN		%Unifying the indirect object with the lexicalized Det+N phrase
}





