%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018

type CAT = {v, sv, sn, sp, p, adj}
type GEN = {m, f}
type FAM = {
  IntransitifActif
}

feature morph: string
feature lemma: string
feature cat  : CAT
feature fam  : FAM
feature gen  : GEN

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class appelle
{
  <morpho> {
    morph <- "appelle";
    lemma <- "appeler";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class aAvoir
{
  <morpho> {
    morph <- "a";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cloue
{
  <morpho> {
    morph <- "cloue";
    lemma <- "clouer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cuit
{
  <morpho> {
    morph <- "cuit";
    lemma <- "cuire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cuitImp
{
  <morpho> {
    morph <- "cuit";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class cuitsImp
{
  <morpho> {
    morph <- "cuits";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- m
   }
}

class cuiteImp
{
  <morpho> {
    morph <- "cuite";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class cuitesImp
{
  <morpho> {
    morph <- "cuites";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- f
   }
}

class eclate
{
  <morpho> {
    morph <- "éclate";
    lemma <- "éclater";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class estV
{
  <morpho> {
    morph <- "est";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class eteV
{
  <morpho> {
    morph <- "été";
    lemma <- "être";
    cat   <- v;
    mode <- ppart
   }
}

class evanouit
{
  <morpho> {
    morph <- "évanouit";
    lemma <- "évanouir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class faitV
{
  <morpho> {
    morph <- "fait";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}


class interesse
{
  <morpho> {
    morph <- "intéresse";
    lemma <- "intéresser";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class joint
{
  <morpho> {
    morph <- "joint";
    lemma <- "joindre";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class jointPpart
{
  <morpho> {
    morph <- "joint";
    lemma <- "joindre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class pousse
{
  <morpho> {
    morph <- "pousse";
    lemma <- "pousser";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class prends
{
  <morpho> {
    morph <- "prends";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    pers <- 1; 
    num <- sg
   }
}

class prenons
{
  <morpho> {
    morph <- "prenons";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind;
    pers <- 1;
    num <- pl
   }
}

class prend
{
  <morpho> {
    morph <- "prend";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class prends2pers
{
  <morpho> {
    morph <- "prends";
    lemma <- "prendre";
    cat   <- v;
    mode <- imp; 
    pers <- 2; 
    num <- sg
   }
}

class pris
{
  <morpho> {
    morph <- "pris";
    lemma <- "prendre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class priseImp
{
  <morpho> {
    morph <- "prise";
    lemma <- "prendre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class sont
{
  <morpho> {
    morph <- "sont";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class tombe
{
  <morpho> {
    morph <- "tombe";
    lemma <- "tomber";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class tourne
{
  <morpho> {
    morph <- "tourne";
    lemma <- "tourner";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class tourneImp
{
  <morpho> {
    morph <- "tourné";
    lemma <- "tourner";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class tourneeImp
{
  <morpho> {
    morph <- "tournée";
    lemma <- "tourner";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class va
{
  <morpho> {
    morph <- "va";
    lemma <- "aller";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class videV
{
  <morpho> {
    morph <- "vide";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 1;
    num <- sg
   }
}

class videsV
{
  <morpho> {
    morph <- "vides";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 2;
    num <- sg
   }
}

class videV3
{
  <morpho> {
    morph <- "vide";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class vidonsV
{
  <morpho> {
    morph <- "vidons";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 1;
    num <- pl
   }
}

class videzV
{
  <morpho> {
    morph <- "videz";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 2;
    num <- pl
   }
}

class videntV
{
  <morpho> {
    morph <- "vident";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class bois
{
  <morpho> {
    morph <- "bois";
    lemma <- "bois";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class boule
{
  <morpho> {
    morph <- "boule";
    lemma <- "boule";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class boules
{
  <morpho> {
    morph <- "boules";
    lemma <- "boule";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class bout
{
  <morpho> {
    morph <- "bout";
    lemma <- "bout";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class carotte
{
  <morpho> {
    morph <- "carotte";
    lemma <- "carotte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class carottes
{
  <morpho> {
    morph <- "carottes";
    lemma <- "carotte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class charbon
{
  <morpho> {
    morph <- "charbon";
    lemma <- "charbon";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class face
{
  <morpho> {
    morph <- "face";
    lemma <- "face";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class faces
{
  <morpho> {
    morph <- "faces";
    lemma <- "face";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class fuite
{
  <morpho> {
    morph <- "fuite";
    lemma <- "fuite";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class Jean
{
  <morpho> {
    morph <- "Jean";
    lemma <- "jean";
    cat   <- n;
    pers <- 3; 
    gen <- f; 
    num <- sg
   }
}

class Marie
{
  <morpho> {
    morph <- "Marie";
    lemma <- "marie";
    cat   <- n;
    pers <- 3; 
    gen <- f; 
    num <- sg
   }
}

class nuit
{
  <morpho> {
    morph <- "nuit";
    lemma <- "nuit";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class page
{
  <morpho> {
    morph <- "page";
    lemma <- "page";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class pages
{
  <morpho> {
    morph <- "pages";
    lemma <- "page";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class porte
{
  <morpho> {
    morph <- "porte";
    lemma <- "porte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class profil
{
  <morpho> {
    morph <- "profil";
    lemma <- "profil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class profils
{
  <morpho> {
    morph <- "profils";
    lemma <- "profil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class geste
{
  <morpho> {
    morph <- "geste";
    lemma <- "geste";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class parole
{
  <morpho> {
    morph <- "parole";
    lemma <- "parole";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class sac
{
  <morpho> {
    morph <- "sac";
    lemma <- "sac";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class sacs
{
  <morpho> {
    morph <- "sacs";
    lemma <- "sac";
    cat <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class secours
{
  <morpho> {
    morph <- "secours";
    lemma <- "secours";
    cat <- n;
    det <- -;
    gen <- m
   }
}

class sol
{
  <morpho> {
    morph <- "sol";
    lemma <- "sol";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class sièclesN
{
  <morpho> {
    morph <- "siècles";
    lemma <- "siècle";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class certainement
{
  <morpho> {
    morph <- "certainement";
    lemma <- "certainement";
    cat   <- adv
   }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class bas
{
  <morpho> {
    morph <- "bas";
    lemma <- "bas";
    cat   <- adj;
    gen <- m
   }
}

class basse
{
  <morpho> {
    morph <- "basse";
    lemma <- "bas";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class basses
{
  <morpho> {
    morph <- "basses";
    lemma <- "bas";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class grande
{
  <morpho> {
    morph <- "grande";
    lemma <- "grand";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class grandes
{
  <morpho> {
    morph <- "grandes";
    lemma <- "grand";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class grand
{
  <morpho> {
    morph <- "grand";
    lemma <- "grand";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class grands
{
  <morpho> {
    morph <- "grands";
    lemma <- "grand";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class importante
{
  <morpho> {
    morph <- "importante";
    lemma <- "important";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class importantes
{
  <morpho> {
    morph <- "importantes";
    lemma <- "important";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class important
{
  <morpho> {
    morph <- "important";
    lemma <- "important";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class importants
{
  <morpho> {
    morph <- "importants";
    lemma <- "important";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class la
{
  <morpho> {
    morph <- "la";
    lemma <- "le";
    cat   <- d;
    gen <- f;
    num <- sg
   }
}

class le
{
  <morpho> {
    morph <- "le";
    lemma <- "le";
    cat   <- d;
    gen <- m;
    num <- sg
   }
}

class les
{
  <morpho> {
    morph <- "les";
    lemma <- "le";
    cat   <- d;
    num <- pl
   }
}

%%%%%
class mon
{
  <morpho> {
    morph <- "mon";
    lemma <- "mon";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

class ma
{
  <morpho> {
    morph <- "ma";
    lemma <- "mon";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

class mes
{
  <morpho> {
    morph <- "mes";
    lemma <- "mon";
    cat   <- d;
    num <- pl;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

%%%%%
class ton
{
  <morpho> {
    morph <- "ton";
    lemma <- "ton";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

class ta
{
  <morpho> {
    morph <- "ta";
    lemma <- "ton";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

class tes
{
  <morpho> {
    morph <- "tes";
    lemma <- "ton";
    cat  <- d;
    num <- pl;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

%%%%%
class son
{
  <morpho> {
    morph <- "son";
    lemma <- "son";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

class sa
{
  <morpho> {
    morph <- "sa";
    lemma <- "son";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

class ses
{
  <morpho> {
    morph <- "tes";
    lemma <- "son";
    cat  <- d;
    num <- pl;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

%%%%%
class notre
{
  <morpho> {
    morph <- "notre";
    lemma <- "notre";
    cat   <- d;
    num <- sg;
    subj-pers <- 1;
    subj-num <- pl  
   }
}

class nos
{
  <morpho> {
    morph <- "nos";
    lemma <- "notre";
    cat  <- d;
    num <- pl;
    subj-pers <- 1;
    subj-num <- pl  
   }
}

%%%%%
class votre
{
  <morpho> {
    morph <- "votre";
    lemma <- "votre";
    cat   <- d;
    num <- sg;
    subj-pers <- 2;
    subj-num <- pl  
   }
}

class vos
{
  <morpho> {
    morph <- "vos";
    lemma <- "votre";
    cat  <- d;
    num <- pl;
    subj-pers <- 2;
    subj-num <- pl  
   }
}

%%%%%
class leur
{
  <morpho> {
    morph <- "leur";
    lemma <- "leur";
    cat  <- d;
    num <- sg;
    subj-pers <- 3;
    subj-num <- pl  
   }
}

class leurs
{
  <morpho> {
    morph <- "leurs";
    lemma <- "leur";
    cat  <- d;
    num <- pl;
    subj-pers <- 3;
    subj-num <- pl  
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class aPrep
{
  <morpho> {
    morph <- "à";
    lemma <- "à";
    cat   <- p
   }
}

class dans
{
  <morpho> {
    morph <- "dans";
    lemma <- "dans";
    cat   <- p
   }
}

class par
{
  <morpho> {
    morph <- "par";
    lemma <- "par";
    cat   <- p
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRONOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class quePron
{
  <morpho> {
    morph <- "que";
    lemma <- "que";
    cat   <- n;
    wh <- +
   }
}

class queC
{
  <morpho> {
    morph <- "que";
    lemma <- "que";
    cat   <- c
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class se
{
  <morpho> {
    morph <- "se";
    lemma <- "se";
    cat   <- cl;
    func <- obj;
    refl <- +;
    pers <- 3
   }
}

class laClitic
{
  <morpho> {
    morph <- "la";
    lemma <- "le";
    cat   <- cl;
    func <- obj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- sg
   }
}

class leClitic
{
  <morpho> {
    morph <- "le";
    lemma <- "le";
    cat   <- cl;
    refl <- -;
    func <- obj;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}

class il
{
  <morpho> {
    morph <- "il";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}

class elle
{
  <morpho> {
    morph <- "elle";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- sg
   }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018

type CAT = {v, sv, sn, sp, p, adj}
type GEN = {m, f}
type FAM = {
  IntransitifActif
}

feature morph: string
feature lemma: string
feature cat  : CAT
feature fam  : FAM
feature gen  : GEN


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
value appelle
value aAvoir
value cloue
value cuit
value cuitImp
value cuitsImp
value cuiteImp
value cuitesImp
value eclate
value estV
value eteV
value evanouit
value faitV
value interesse
value joint
value jointPpart
value pousse
value prends
value prends2pers
value prenons
value prend
value pris
value priseImp
value tombe
value tourne
value tourneImp
value tourneeImp
value sont
value va
value videV
value videsV
value videV3
value vidonsV
value videzV
value videntV

value bois
value boule
value boules
value bout
value carotte
value carottes
value charbon
value face
value faces
value fuite
value Jean
value Marie
value nuit
value page
value pages
value porte
value geste
value parole
value profil
value profils
value sac
value sacs
value sol
value sièclesN


value bas
value basse
value basses
value grand
value grands
value grande
value grandes
value important
value importants
value importante
value importantes

value la
value le
value les
value mon
value ma
value mes
value ton
value ta
value tes
value son
value sa
value ses
value notre
value nos
value votre
value vos
value leur
value leurs

value aPrep
value dans
value par
%value quePron
%value queC
value se
value laClitic
value leClitic
value il
value elle

