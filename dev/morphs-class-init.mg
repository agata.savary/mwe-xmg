

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class appelle
{
  <morpho> {
    morph <- "appelle";
    lemma <- "appeler";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class agir
{
  <morpho> {
    morph <- "agir";
    lemma <- "agir";
    cat   <- v;
    mode <- inf
   }
}

class agit
{
  <morpho> {
    morph <- "agit";
    lemma <- "agir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class agissait
{
  <morpho> {
    morph <- "agissait";
    lemma <- "agir";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- sg
   }
}

class aAvoir
{
  <morpho> {
    morph <- "a";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class avait
{
  <morpho> {
    morph <- "avait";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- sg
   }
}

class avoir
{
  <morpho> {
    morph <- "avoir";
    lemma <- "avoir";
    cat   <- v;
    mode <- inf
   }
}

class ontV
{
  <morpho> {
    morph <- "ont";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class choisirV
{
  <morpho> {
    morph <- "choisir";
    lemma <- "choisir";
    cat   <- v;
    mode <- inf 
   }
}

class cloue
{
  <morpho> {
    morph <- "cloue";
    lemma <- "clouer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class colorierV
{
  <morpho> {
    morph <- "colorier";
    lemma <- "colorier";
    cat   <- v;
    mode <- inf 
   }
}

class cuit
{
  <morpho> {
    morph <- "cuit";
    lemma <- "cuire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cuitImp
{
  <morpho> {
    morph <- "cuit";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class cuitsImp
{
  <morpho> {
    morph <- "cuits";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- m
   }
}

class cuiteImp
{
  <morpho> {
    morph <- "cuite";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class cuitesImp
{
  <morpho> {
    morph <- "cuites";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- f
   }
}

class déciderV
{
  <morpho> {
    morph <- "décider";
    lemma <- "décider";
    cat   <- v;
    mode <- inf 
   }
}

class décideV
{
  <morpho> {
    morph <- "décide";
    lemma <- "décider";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class direV
{
  <morpho> {
    morph <- "dire";
    lemma <- "dire";
    cat   <- v;
    mode <- inf 
   }
}

class doitV
{
  <morpho> {
    morph <- "doit";
    lemma <- "devoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class eclate
{
  <morpho> {
    morph <- "éclate";
    lemma <- "éclater";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class estV
{
  <morpho> {
    morph <- "est";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class étaitV
{
  <morpho> {
    morph <- "était";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- sg
   }
}
class eteV
{
  <morpho> {
    morph <- "été";
    lemma <- "être";
    cat   <- v;
    mode <- ppart
   }
}

class etreV
{
  <morpho> {
    morph <- "être";
    lemma <- "être";
    cat   <- v;
    mode <- inf
   }
}

class evanouit
{
  <morpho> {
    morph <- "évanouit";
    lemma <- "évanouir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class faitV
{
  <morpho> {
    morph <- "fait";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class ferontV
{
  <morpho> {
    morph <- "feront";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- future;
    pers <- 3;
    num <- pl
   }
}

class faisaisV
{
  <morpho> {
    morph <- "faisais";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 1;
    num <- sg
   }
}

class faisaitV
{
  <morpho> {
    morph <- "faisait";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- sg
   }
}

class faisaientV
{
  <morpho> {
    morph <- "faisaient";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- pl
   }
}


class fontV
{
  <morpho> {
    morph <- "font";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class faireV
{
  <morpho> {
    morph <- "faire";
    lemma <- "faire";
    cat   <- v;
    mode <- inf
   }
}


class fautV
{
  <morpho> {
    morph <- "faut";
    lemma <- "falloir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class interesse
{
  <morpho> {
    morph <- "intéresse";
    lemma <- "intéresser";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class joint
{
  <morpho> {
    morph <- "joint";
    lemma <- "joindre";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class jointPpart
{
  <morpho> {
    morph <- "joint";
    lemma <- "joindre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class joueV
{
  <morpho> {
    morph <- "joue";
    lemma <- "jouer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class jouéV
{
  <morpho> {
    morph <- "joué";
    lemma <- "jouer";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class jouentV
{
  <morpho> {
    morph <- "jouent";
    lemma <- "jouer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}


class metV
{
  <morpho> {
    morph <- "met";
    lemma <- "mettre";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class mettreV
{
  <morpho> {
    morph <- "mettre";
    lemma <- "mettre";
    cat   <- v;
    mode <- inf
   }
}

class misV
{
  <morpho> {
    morph <- "mis";
    lemma <- "mettre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class miseV
{
  <morpho> {
    morph <- "mise";
    lemma <- "mettre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}


class misesV
{
  <morpho> {
    morph <- "mises";
    lemma <- "mettre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- f
   }
}

class porteV
{
  <morpho> {
    morph <- "porte";
    lemma <- "porter";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class porterV
{
  <morpho> {
    morph <- "porter";
    lemma <- "porter";
    cat <- v;
    mode <- inf
   }
}


class pousse
{
  <morpho> {
    morph <- "pousse";
    lemma <- "pousser";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class pleut
{
  <morpho> {
    morph <- "pleut";
    lemma <- "pleuvoir";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class prends
{
  <morpho> {
    morph <- "prends";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    pers <- 1; 
    num <- sg
   }
}

class prenons
{
  <morpho> {
    morph <- "prenons";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind;
    pers <- 1;
    num <- pl
   }
}

class prend
{
  <morpho> {
    morph <- "prend";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class prendra
{
  <morpho> {
    morph <- "prendra";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    tense <- future;
    pers <- 3; 
    num <- sg
   }
}

class prendre
{
  <morpho> {
    morph <- "prendre";
    lemma <- "prendre";
    cat   <- v;
    mode <- inf
   }
}

class prends2pers
{
  <morpho> {
    morph <- "prends";
    lemma <- "prendre";
    cat   <- v;
    mode <- imp; 
    pers <- 2; 
    num <- sg
   }
}

class pris
{
  <morpho> {
    morph <- "pris";
    lemma <- "prendre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class priseImp
{
  <morpho> {
    morph <- "prise";
    lemma <- "prendre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class sont
{
  <morpho> {
    morph <- "sont";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class tombe
{
  <morpho> {
    morph <- "tombe";
    lemma <- "tomber";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class tourne
{
  <morpho> {
    morph <- "tourne";
    lemma <- "tourner";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class tourneImp
{
  <morpho> {
    morph <- "tourné";
    lemma <- "tourner";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class tourneeImp
{
  <morpho> {
    morph <- "tournée";
    lemma <- "tourner";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class trouveV
{
  <morpho> {
    morph <- "trouve";
    lemma <- "trouver";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class trouverV
{
  <morpho> {
    morph <- "trouver";
    lemma <- "trouver";
    cat   <- v;
    mode <- inf
   }
}

class tuerV
{
  <morpho> {
    morph <- "tuer";
    lemma <- "tuer";
    cat   <- v;
    mode <- inf
   }
}

class tueV
{
  <morpho> {
    morph <- "tue";
    lemma <- "tuer";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}


class va
{
  <morpho> {
    morph <- "va";
    lemma <- "aller";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class verraV
{
  <morpho> {
    morph <- "verra";
    lemma <- "voir";
    cat   <- v;
    mode <- ind;
    tense <- future;
    pers <- 3;
    num <- sg
   }
}

class veulentV
{
  <morpho> {
    morph <- "veulent";
    lemma <- "vouloir";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class videV
{
  <morpho> {
    morph <- "vide";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 1;
    num <- sg
   }
}

class videsV
{
  <morpho> {
    morph <- "vides";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 2;
    num <- sg
   }
}

class videV3
{
  <morpho> {
    morph <- "vide";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class vidonsV
{
  <morpho> {
    morph <- "vidons";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 1;
    num <- pl
   }
}

class videzV
{
  <morpho> {
    morph <- "videz";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 2;
    num <- pl
   }
}

class videntV
{
  <morpho> {
    morph <- "vident";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class bois
{
  <morpho> {
    morph <- "bois";
    lemma <- "bois";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class boule
{
  <morpho> {
    morph <- "boule";
    lemma <- "boule";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class boules
{
  <morpho> {
    morph <- "boules";
    lemma <- "boule";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class bout
{
  <morpho> {
    morph <- "bout";
    lemma <- "bout";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class carotte
{
  <morpho> {
    morph <- "carotte";
    lemma <- "carotte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class carottes
{
  <morpho> {
    morph <- "carottes";
    lemma <- "carotte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class charbon
{
  <morpho> {
    morph <- "charbon";
    lemma <- "charbon";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class face
{
  <morpho> {
    morph <- "face";
    lemma <- "face";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class faces
{
  <morpho> {
    morph <- "faces";
    lemma <- "face";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class fuite
{
  <morpho> {
    morph <- "fuite";
    lemma <- "fuite";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class Jean
{
  <morpho> {
    morph <- "Jean";
    lemma <- "jean";
    cat   <- n;
    pers <- 3; 
    gen <- m; 
    num <- sg
   }
}

class Marie
{
  <morpho> {
    morph <- "Marie";
    lemma <- "marie";
    cat   <- n;
    pers <- 3; 
    gen <- f; 
    num <- sg
   }
}

class nuit
{
  <morpho> {
    morph <- "nuit";
    lemma <- "nuit";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class page
{
  <morpho> {
    morph <- "page";
    lemma <- "page";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class pages
{
  <morpho> {
    morph <- "pages";
    lemma <- "page";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class porte
{
  <morpho> {
    morph <- "porte";
    lemma <- "porte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class profil
{
  <morpho> {
    morph <- "profil";
    lemma <- "profil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class profils
{
  <morpho> {
    morph <- "profils";
    lemma <- "profil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class geste
{
  <morpho> {
    morph <- "geste";
    lemma <- "geste";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class parole
{
  <morpho> {
    morph <- "parole";
    lemma <- "parole";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class sac
{
  <morpho> {
    morph <- "sac";
    lemma <- "sac";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class sacs
{
  <morpho> {
    morph <- "sacs";
    lemma <- "sac";
    cat <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class secours
{
  <morpho> {
    morph <- "secours";
    lemma <- "secours";
    cat <- n;
    det <- -;
    gen <- m
   }
}

class sol
{
  <morpho> {
    morph <- "sol";
    lemma <- "sol";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class sièclesN
{
  <morpho> {
    morph <- "siècles";
    lemma <- "siècle";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class aujourdhui
{
  <morpho> {
    morph <- "aujourd'hui";
    lemma <- "aujourd'hui";
    cat   <- adv
   }
}

class certainement
{
  <morpho> {
    morph <- "certainement";
    lemma <- "certainement";
    cat   <- adv
   }
}

class demain
{
  <morpho> {
    morph <- "demain";
    lemma <- "demain";
    cat   <- adv
   }
}

class frequemment
{
  <morpho> {
    morph <- "fréquemment";
    lemma <- "fréquemment";
    cat   <- adv
   }
}

class maintenant
{
  <morpho> {
    morph <- "maintenant";
    lemma <- "maintenant";
    cat   <- adv
   }
}
class recemment
{
  <morpho> {
    morph <- "récemment";
    lemma <- "récemment";
    cat   <- adv
   }
}

class alorsADV
{
  <morpho> {
    morph <- "alors";
    lemma <- "alors";
    cat   <- adv
   }
}

class iciADV
{
  <morpho> {
    morph <- "ici";
    lemma <- "ici";
    cat   <- adv
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class bas
{
  <morpho> {
    morph <- "bas";
    lemma <- "bas";
    cat   <- adj;
    gen <- m
   }
}

class basse
{
  <morpho> {
    morph <- "basse";
    lemma <- "bas";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class basses
{
  <morpho> {
    morph <- "basses";
    lemma <- "bas";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class grande
{
  <morpho> {
    morph <- "grande";
    lemma <- "grand";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class grandes
{
  <morpho> {
    morph <- "grandes";
    lemma <- "grand";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class grand
{
  <morpho> {
    morph <- "grand";
    lemma <- "grand";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class grands
{
  <morpho> {
    morph <- "grands";
    lemma <- "grand";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class importante
{
  <morpho> {
    morph <- "importante";
    lemma <- "important";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class importantes
{
  <morpho> {
    morph <- "importantes";
    lemma <- "important";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class important
{
  <morpho> {
    morph <- "important";
    lemma <- "important";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class importants
{
  <morpho> {
    morph <- "importants";
    lemma <- "important";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class incroyable
{
  <morpho> {
    morph <- "incroyable";
    lemma <- "incroyable";
    cat   <- adj;
    num <- s
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class la
{
  <morpho> {
    morph <- "la";
    lemma <- "le";
    cat   <- d;
    gen <- f;
    num <- sg
   }
}

class le
{
  <morpho> {
    morph <- "le";
    lemma <- "le";
    cat   <- d;
    gen <- m;
    num <- sg
   }
}

class les
{
  <morpho> {
    morph <- "les";
    lemma <- "le";
    cat   <- d;
    num <- pl
   }
}

class cetteDET
{
  <morpho> {
    morph <- "cette";
    lemma <- "ce";
    cat   <- d;
    gen <- f;
    num <- sg
   }
}

class ceDET
{
  <morpho> {
    morph <- "ce";
    lemma <- "ce";
    cat   <- d;
    gen <- m;
    num <- sg
   }
}

class cesDET
{
  <morpho> {
    morph <- "ces";
    lemma <- "ce";
    cat   <- d;
    num <- pl
   }
}

class une
{
  <morpho> {
    morph <- "une";
    lemma <- "un";
    cat   <- d;
    gen <- f;
    num <- sg
   }
}

class un
{
  <morpho> {
    morph <- "un";
    lemma <- "un";
    cat   <- d;
    gen <- m;
    num <- sg
   }
}

class des
{
  <morpho> {
    morph <- "des";
    lemma <- "un";
    cat   <- d;
    num <- pl
   }
}


%%%%%
class mon
{
  <morpho> {
    morph <- "mon";
    lemma <- "mon";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

class ma
{
  <morpho> {
    morph <- "ma";
    lemma <- "mon";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

class mes
{
  <morpho> {
    morph <- "mes";
    lemma <- "mon";
    cat   <- d;
    num <- pl;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

%%%%%
class ton
{
  <morpho> {
    morph <- "ton";
    lemma <- "ton";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

class ta
{
  <morpho> {
    morph <- "ta";
    lemma <- "ton";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

class tes
{
  <morpho> {
    morph <- "tes";
    lemma <- "ton";
    cat  <- d;
    num <- pl;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

%%%%%
class son
{
  <morpho> {
    morph <- "son";
    lemma <- "son";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

class sa
{
  <morpho> {
    morph <- "sa";
    lemma <- "son";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

class ses
{
  <morpho> {
    morph <- "tes";
    lemma <- "son";
    cat  <- d;
    num <- pl;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

%%%%%
class notre
{
  <morpho> {
    morph <- "notre";
    lemma <- "notre";
    cat   <- d;
    num <- sg;
    subj-pers <- 1;
    subj-num <- pl  
   }
}

class nos
{
  <morpho> {
    morph <- "nos";
    lemma <- "notre";
    cat  <- d;
    num <- pl;
    subj-pers <- 1;
    subj-num <- pl  
   }
}

%%%%%
class votre
{
  <morpho> {
    morph <- "votre";
    lemma <- "votre";
    cat   <- d;
    num <- sg;
    subj-pers <- 2;
    subj-num <- pl  
   }
}

class vos
{
  <morpho> {
    morph <- "vos";
    lemma <- "votre";
    cat  <- d;
    num <- pl;
    subj-pers <- 2;
    subj-num <- pl  
   }
}

%%%%%
class leur
{
  <morpho> {
    morph <- "leur";
    lemma <- "leur";
    cat  <- d;
    num <- sg;
    subj-pers <- 3;
    subj-num <- pl  
   }
}

class leurs
{
  <morpho> {
    morph <- "leurs";
    lemma <- "leur";
    cat  <- d;
    num <- pl;
    subj-pers <- 3;
    subj-num <- pl  
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class aPrep
{
  <morpho> {
    morph <- "à";
    lemma <- "à";
    cat   <- p
   }
}

class dans
{
  <morpho> {
    morph <- "dans";
    lemma <- "dans";
    cat   <- p
   }
}

class par
{
  <morpho> {
    morph <- "par";
    lemma <- "par";
    cat   <- p
   }
}

class en
{
  <morpho> {
    morph <- "en";
    lemma <- "en";
    cat   <- p
   }
}

class pour
{
  <morpho> {
    morph <- "pour";
    lemma <- "pour";
    cat   <- p
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRONOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class quePron
{
  <morpho> {
    morph <- "que";
    lemma <- "que";
    cat   <- n;
    wh <- +
   }
}

class queC
{
  <morpho> {
    morph <- "que";
    lemma <- "que";
    cat   <- c
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class seObj
{
  <morpho> {
    morph <- "se";
    lemma <- "se";
    cat   <- cl;
    func <- obj;
    refl <- +;
    pers <- 3
   }
}

class seIobj
{
  <morpho> {
    morph <- "se";
    lemma <- "se";
    cat   <- cl;
    func <- iobj;
    refl <- +;
    pers <- 3
   }
}


class laClitic
{
  <morpho> {
    morph <- "la";
    lemma <- "le";
    cat   <- cl;
    func <- obj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- sg
   }
}

class leClitic
{
  <morpho> {
    morph <- "le";
    lemma <- "le";
    cat   <- cl;
    refl <- -;
    func <- obj;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}

class je
{
  <morpho> {
    morph <- "je";
    lemma <- "je";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 1;
    num <- sg
   }
}

class il
{
  <morpho> {
    morph <- "il";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}

class elle
{
  <morpho> {
    morph <- "elle";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- sg
   }
}

class ils
{
  <morpho> {
    morph <- "ils";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- m;
    num <- pl
   }
}

class elles
{
  <morpho> {
    morph <- "elles";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- pl
   }
}

class celaN
{
  <morpho> {
    morph <- "cela";
    lemma <- "cela";
    cat   <- n;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}
class moiN 	%fait appel à moi
{
  <morpho> {
    morph <- "moi";
    lemma <- "moi";
    cat   <- n;
    pers <- 1;
    num <- sg
   }
}

class toiN 
{
  <morpho> {
    morph <- "toi";
    lemma <- "moi";
    cat   <- n;
    pers <- 2;
    num <- sg
   }
}

class luiN 
{
  <morpho> {
    morph <- "lui";
    lemma <- "moi";
    cat   <- n;
    pers <- 3;
    num <- sg;
    gen <- m
   }
}

class elleN 
{
  <morpho> {
    morph <- "elle";
    lemma <- "moi";
    cat   <- n;
    pers <- 3;
    num <- sg;
    gen <- f
   }
}

class nousN 	%fait appel à nous
{
  <morpho> {
    morph <- "nous";
    lemma <- "moi";
    cat   <- n;
    pers <- 1;
    num <- pl
   }
}

class vousN 	%fait appel à nous
{
  <morpho> {
    morph <- "vous";
    lemma <- "moi";
    cat   <- n;
    pers <- 2;
    num <- pl
   }
}

class euxN 
{
  <morpho> {
    morph <- "eux";
    lemma <- "moi";
    cat   <- n;
    pers <- 3;
    num <- pl;
    gen <- m
   }
}

class ellesN 
{
  <morpho> {
    morph <- "elles";
    lemma <- "moi";
    cat   <- n;
    pers <- 3;
    num <- pl;
    gen <- f
   }
}


class y
{
  <morpho> {
    morph <- "y";
    lemma <- "y";
    cat   <- cl;
    func <- loc;
    refl <- -
   }
}

class enCL
{
  <morpho> {
    morph <- "en";
    lemma <- "en";
    cat   <- cl;
    func <- gen;
    refl <- -
   }
}


