%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018

type CAT = {v, sv, sn, sp, p, adj}
type GEN = {m, f}
type FAM = {
  IntransitifActif
}

feature morph: string
feature lemma: string
feature cat  : CAT
feature fam  : FAM
feature gen  : GEN

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class appelle
{
  <morpho> {
    morph <- "appelle";
    lemma <- "appeler";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class aAvoir
{
  <morpho> {
    morph <- "a";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cloue
{
  <morpho> {
    morph <- "cloue";
    lemma <- "clouer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cuit
{
  <morpho> {
    morph <- "cuit";
    lemma <- "cuire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class cuitImp
{
  <morpho> {
    morph <- "cuit";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class cuitsImp
{
  <morpho> {
    morph <- "cuits";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- m
   }
}

class cuiteImp
{
  <morpho> {
    morph <- "cuite";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class cuitesImp
{
  <morpho> {
    morph <- "cuites";
    lemma <- "cuire";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- f
   }
}

class eclate
{
  <morpho> {
    morph <- "éclate";
    lemma <- "éclater";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class estV
{
  <morpho> {
    morph <- "est";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class eteV
{
  <morpho> {
    morph <- "été";
    lemma <- "être";
    cat   <- v;
    mode <- ppart
   }
}

class evanouit
{
  <morpho> {
    morph <- "évanouit";
    lemma <- "évanouir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class faitV
{
  <morpho> {
    morph <- "fait";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}


class interesse
{
  <morpho> {
    morph <- "intéresse";
    lemma <- "intéresser";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class joint
{
  <morpho> {
    morph <- "joint";
    lemma <- "joindre";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class jointPpart
{
  <morpho> {
    morph <- "joint";
    lemma <- "joindre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class pousse
{
  <morpho> {
    morph <- "pousse";
    lemma <- "pousser";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class prends
{
  <morpho> {
    morph <- "prends";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    pers <- 1; 
    num <- sg
   }
}

class prenons
{
  <morpho> {
    morph <- "prenons";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind;
    pers <- 1;
    num <- pl
   }
}

class prend
{
  <morpho> {
    morph <- "prend";
    lemma <- "prendre";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class prends2pers
{
  <morpho> {
    morph <- "prends";
    lemma <- "prendre";
    cat   <- v;
    mode <- imp; 
    pers <- 2; 
    num <- sg
   }
}

class pris
{
  <morpho> {
    morph <- "pris";
    lemma <- "prendre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class priseImp
{
  <morpho> {
    morph <- "prise";
    lemma <- "prendre";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class sont
{
  <morpho> {
    morph <- "sont";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class tombe
{
  <morpho> {
    morph <- "tombe";
    lemma <- "tomber";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class tourne
{
  <morpho> {
    morph <- "tourne";
    lemma <- "tourner";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class tourneImp
{
  <morpho> {
    morph <- "tourné";
    lemma <- "tourner";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- m
   }
}

class tourneeImp
{
  <morpho> {
    morph <- "tournée";
    lemma <- "tourner";
    cat   <- v;
    mode <- ppart; 
    pp-num <- sg;
    pp-gen <- f
   }
}

class va
{
  <morpho> {
    morph <- "va";
    lemma <- "aller";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class videV
{
  <morpho> {
    morph <- "vide";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 1;
    num <- sg
   }
}

class videsV
{
  <morpho> {
    morph <- "vides";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 2;
    num <- sg
   }
}

class videV3
{
  <morpho> {
    morph <- "vide";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class vidonsV
{
  <morpho> {
    morph <- "vidons";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 1;
    num <- pl
   }
}

class videzV
{
  <morpho> {
    morph <- "videz";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 2;
    num <- pl
   }
}

class videntV
{
  <morpho> {
    morph <- "vident";
    lemma <- "vider";
    cat <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class bois
{
  <morpho> {
    morph <- "bois";
    lemma <- "bois";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class boule
{
  <morpho> {
    morph <- "boule";
    lemma <- "boule";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class boules
{
  <morpho> {
    morph <- "boules";
    lemma <- "boule";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class bout
{
  <morpho> {
    morph <- "bout";
    lemma <- "bout";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class carotte
{
  <morpho> {
    morph <- "carotte";
    lemma <- "carotte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class carottes
{
  <morpho> {
    morph <- "carottes";
    lemma <- "carotte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class charbon
{
  <morpho> {
    morph <- "charbon";
    lemma <- "charbon";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class face
{
  <morpho> {
    morph <- "face";
    lemma <- "face";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class faces
{
  <morpho> {
    morph <- "faces";
    lemma <- "face";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class fuite
{
  <morpho> {
    morph <- "fuite";
    lemma <- "fuite";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class Jean
{
  <morpho> {
    morph <- "Jean";
    lemma <- "jean";
    cat   <- n;
    pers <- 3; 
    gen <- f; 
    num <- sg
   }
}

class Marie
{
  <morpho> {
    morph <- "Marie";
    lemma <- "marie";
    cat   <- n;
    pers <- 3; 
    gen <- f; 
    num <- sg
   }
}

class nuit
{
  <morpho> {
    morph <- "nuit";
    lemma <- "nuit";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class page
{
  <morpho> {
    morph <- "page";
    lemma <- "page";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class pages
{
  <morpho> {
    morph <- "pages";
    lemma <- "page";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class porte
{
  <morpho> {
    morph <- "porte";
    lemma <- "porte";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class profil
{
  <morpho> {
    morph <- "profil";
    lemma <- "profil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class profils
{
  <morpho> {
    morph <- "profils";
    lemma <- "profil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class geste
{
  <morpho> {
    morph <- "geste";
    lemma <- "geste";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class parole
{
  <morpho> {
    morph <- "parole";
    lemma <- "parole";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class sac
{
  <morpho> {
    morph <- "sac";
    lemma <- "sac";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class sacs
{
  <morpho> {
    morph <- "sacs";
    lemma <- "sac";
    cat <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class secours
{
  <morpho> {
    morph <- "secours";
    lemma <- "secours";
    cat <- n;
    det <- -;
    gen <- m
   }
}

class sol
{
  <morpho> {
    morph <- "sol";
    lemma <- "sol";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class sièclesN
{
  <morpho> {
    morph <- "siècles";
    lemma <- "siècle";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class certainement
{
  <morpho> {
    morph <- "certainement";
    lemma <- "certainement";
    cat   <- adv
   }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class bas
{
  <morpho> {
    morph <- "bas";
    lemma <- "bas";
    cat   <- adj;
    gen <- m
   }
}

class basse
{
  <morpho> {
    morph <- "basse";
    lemma <- "bas";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class basses
{
  <morpho> {
    morph <- "basses";
    lemma <- "bas";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class grande
{
  <morpho> {
    morph <- "grande";
    lemma <- "grand";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class grandes
{
  <morpho> {
    morph <- "grandes";
    lemma <- "grand";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class grand
{
  <morpho> {
    morph <- "grand";
    lemma <- "grand";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class grands
{
  <morpho> {
    morph <- "grands";
    lemma <- "grand";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class importante
{
  <morpho> {
    morph <- "importante";
    lemma <- "important";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class importantes
{
  <morpho> {
    morph <- "importantes";
    lemma <- "important";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class important
{
  <morpho> {
    morph <- "important";
    lemma <- "important";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class importants
{
  <morpho> {
    morph <- "importants";
    lemma <- "important";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class la
{
  <morpho> {
    morph <- "la";
    lemma <- "le";
    cat   <- d;
    gen <- f;
    num <- sg
   }
}

class le
{
  <morpho> {
    morph <- "le";
    lemma <- "le";
    cat   <- d;
    gen <- m;
    num <- sg
   }
}

class les
{
  <morpho> {
    morph <- "les";
    lemma <- "le";
    cat   <- d;
    num <- pl
   }
}

%%%%%
class mon
{
  <morpho> {
    morph <- "mon";
    lemma <- "mon";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

class ma
{
  <morpho> {
    morph <- "ma";
    lemma <- "mon";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

class mes
{
  <morpho> {
    morph <- "mes";
    lemma <- "mon";
    cat   <- d;
    num <- pl;
    subj-pers <- 1;
    subj-num <- sg  
   }
}

%%%%%
class ton
{
  <morpho> {
    morph <- "ton";
    lemma <- "ton";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

class ta
{
  <morpho> {
    morph <- "ta";
    lemma <- "ton";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

class tes
{
  <morpho> {
    morph <- "tes";
    lemma <- "ton";
    cat  <- d;
    num <- pl;
    subj-pers <- 2;
    subj-num <- sg  
   }
}

%%%%%
class son
{
  <morpho> {
    morph <- "son";
    lemma <- "son";
    cat   <- d;
    gen <- m;
    num <- sg;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

class sa
{
  <morpho> {
    morph <- "sa";
    lemma <- "son";
    cat   <- d;
    gen <- f;
    num <- sg;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

class ses
{
  <morpho> {
    morph <- "tes";
    lemma <- "son";
    cat  <- d;
    num <- pl;
    subj-pers <- 3;
    subj-num <- sg  
   }
}

%%%%%
class notre
{
  <morpho> {
    morph <- "notre";
    lemma <- "notre";
    cat   <- d;
    num <- sg;
    subj-pers <- 1;
    subj-num <- pl  
   }
}

class nos
{
  <morpho> {
    morph <- "nos";
    lemma <- "notre";
    cat  <- d;
    num <- pl;
    subj-pers <- 1;
    subj-num <- pl  
   }
}

%%%%%
class votre
{
  <morpho> {
    morph <- "votre";
    lemma <- "votre";
    cat   <- d;
    num <- sg;
    subj-pers <- 2;
    subj-num <- pl  
   }
}

class vos
{
  <morpho> {
    morph <- "vos";
    lemma <- "votre";
    cat  <- d;
    num <- pl;
    subj-pers <- 2;
    subj-num <- pl  
   }
}

%%%%%
class leur
{
  <morpho> {
    morph <- "leur";
    lemma <- "leur";
    cat  <- d;
    num <- sg;
    subj-pers <- 3;
    subj-num <- pl  
   }
}

class leurs
{
  <morpho> {
    morph <- "leurs";
    lemma <- "leur";
    cat  <- d;
    num <- pl;
    subj-pers <- 3;
    subj-num <- pl  
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class aPrep
{
  <morpho> {
    morph <- "à";
    lemma <- "à";
    cat   <- p
   }
}

class dans
{
  <morpho> {
    morph <- "dans";
    lemma <- "dans";
    cat   <- p
   }
}

class par
{
  <morpho> {
    morph <- "par";
    lemma <- "par";
    cat   <- p
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRONOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class quePron
{
  <morpho> {
    morph <- "que";
    lemma <- "que";
    cat   <- n;
    wh <- +
   }
}

class queC
{
  <morpho> {
    morph <- "que";
    lemma <- "que";
    cat   <- c
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class se
{
  <morpho> {
    morph <- "se";
    lemma <- "se";
    cat   <- cl;
    func <- obj;
    refl <- +;
    pers <- 3
   }
}

class laClitic
{
  <morpho> {
    morph <- "la";
    lemma <- "le";
    cat   <- cl;
    func <- obj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- sg
   }
}

class leClitic
{
  <morpho> {
    morph <- "le";
    lemma <- "le";
    cat   <- cl;
    refl <- -;
    func <- obj;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}

class il
{
  <morpho> {
    morph <- "il";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}

class elle
{
  <morpho> {
    morph <- "elle";
    lemma <- "il";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- f;
    num <- sg
   }
}




class lN1
{
  <morpho> {
    morph <- "l";
    lemma <- "l";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class lN2
{
  <morpho> {
    morph <- "l";
    lemma <- "l";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class unificationN
{
  <morpho> {
    morph <- "unification";
    lemma <- "unification";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class nN1
{
  <morpho> {
    morph <- "n";
    lemma <- "n";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class nN2
{
  <morpho> {
    morph <- "n";
    lemma <- "n";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class avaitV
{
  <morpho> {
    morph <- "avait";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- sg
   }
}

class lieuN
{
  <morpho> {
    morph <- "lieu";
    lemma <- "lieu";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class dN1
{
  <morpho> {
    morph <- "d";
    lemma <- "d";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class dN2
{
  <morpho> {
    morph <- "d";
    lemma <- "d";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class êtreN
{
  <morpho> {
    morph <- "être";
    lemma <- "être";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class êtreV
{
  <morpho> {
    morph <- "être";
    lemma <- "être";
    cat   <- v;
    mode <- inf
   }
}

class firentV
{
  <morpho> {
    morph <- "firent";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- spast;
    pers <- 3;
    num <- pl
   }
}

class appelN
{
  <morpho> {
    morph <- "appel";
    lemma <- "appel";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class àPREP
{
  <morpho> {
    morph <- "à";
    lemma <- "à";
    cat   <- p
   }
}

class charpentiersN
{
  <morpho> {
    morph <- "charpentiers";
    lemma <- "charpentier";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class deuxN1
{
  <morpho> {
    morph <- "deux";
    lemma <- "deux";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class deuxN2
{
  <morpho> {
    morph <- "deux";
    lemma <- "deux";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class pilotesN1
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "pilote";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class pilotesN2
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "pilote";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class pilotesN
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "pilote";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class pilotesN1
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "pilote";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class pilotesN2
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "pilote";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class pilotesV1
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "piloter";
    cat   <- v;
    mode <- ind;
    pers <- 2;
    num <- sg
   }
}

class pilotesV2
{
  <morpho> {
    morph <- "pilotes";
    lemma <- "piloter";
    cat   <- v;
    mode <- subj;
    pers <- 2;
    num <- sg
   }
}

class commandantsA
{
  <morpho> {
    morph <- "commandants";
    lemma <- "commandant";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class commandantsN
{
  <morpho> {
    morph <- "commandants";
    lemma <- "commandant";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class commandantsN
{
  <morpho> {
    morph <- "commandants";
    lemma <- "commandant";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class eurentV
{
  <morpho> {
    morph <- "eurent";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    tense <- spast;
    pers <- 3;
    num <- pl
   }
}

class faireV
{
  <morpho> {
    morph <- "faire";
    lemma <- "faire";
    cat   <- v;
    mode <- inf
   }
}

class faceN
{
  <morpho> {
    morph <- "face";
    lemma <- "face";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class faceN
{
  <morpho> {
    morph <- "face";
    lemma <- "face";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class faceV1
{
  <morpho> {
    morph <- "face";
    lemma <- "facer";
    cat   <- v;
    mode <- ind;
    pers <- 1;
    num <- sg
   }
}

class faceV2
{
  <morpho> {
    morph <- "face";
    lemma <- "facer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class faceV3
{
  <morpho> {
    morph <- "face";
    lemma <- "facer";
    cat   <- v;
    mode <- subj;
    pers <- 1;
    num <- sg
   }
}

class faceV4
{
  <morpho> {
    morph <- "face";
    lemma <- "facer";
    cat   <- v;
    mode <- subj;
    pers <- 3;
    num <- sg
   }
}

class faceV5
{
  <morpho> {
    morph <- "face";
    lemma <- "facer";
    cat   <- v;
    mode <- imp;
    pers <- 2;
    num <- sg
   }
}

class unN1
{
  <morpho> {
    morph <- "un";
    lemma <- "un";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class unN2
{
  <morpho> {
    morph <- "un";
    lemma <- "un";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class dilemmeN
{
  <morpho> {
    morph <- "dilemme";
    lemma <- "dilemme";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class faisaientV
{
  <morpho> {
    morph <- "faisaient";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- pl
   }
}

class objetN
{
  <morpho> {
    morph <- "objet";
    lemma <- "objet";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class ordreN
{
  <morpho> {
    morph <- "ordre";
    lemma <- "ordre";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class ferontV
{
  <morpho> {
    morph <- "feront";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- future;
    pers <- 3;
    num <- pl
   }
}

class uneN
{
  <morpho> {
    morph <- "une";
    lemma <- "une";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class campagneN
{
  <morpho> {
    morph <- "campagne";
    lemma <- "campagne";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class faisaitV
{
  <morpho> {
    morph <- "faisait";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 3;
    num <- sg
   }
}

class partieN
{
  <morpho> {
    morph <- "partie";
    lemma <- "partie";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class partieA
{
  <morpho> {
    morph <- "partie";
    lemma <- "parti";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class partieV
{
  <morpho> {
    morph <- "partie";
    lemma <- "partir";
    cat   <- v;
    pp-gen <- f;
    pp-num <- sg

   }
}

class dePREP
{
  <morpho> {
    morph <- "de";
    lemma <- "de";
    cat   <- p
   }
}

class sonN
{
  <morpho> {
    morph <- "son";
    lemma <- "son";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class territoireN
{
  <morpho> {
    morph <- "territoire";
    lemma <- "territoire";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class célébritésN
{
  <morpho> {
    morph <- "célébrités";
    lemma <- "célébrité";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class ontV
{
  <morpho> {
    morph <- "ont";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class faitA
{
  <morpho> {
    morph <- "fait";
    lemma <- "fait";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class faitN
{
  <morpho> {
    morph <- "fait";
    lemma <- "fait";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class faitV1
{
  <morpho> {
    morph <- "fait";
    lemma <- "faire";
    cat   <- v;
    pp-gen <- m;
    pp-num <- sg

   }
}

class faitV2
{
  <morpho> {
    morph <- "fait";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class castingN
{
  <morpho> {
    morph <- "casting";
    lemma <- "casting";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class sN1
{
  <morpho> {
    morph <- "s";
    lemma <- "s";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class sN2
{
  <morpho> {
    morph <- "s";
    lemma <- "s";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class agitV1
{
  <morpho> {
    morph <- "agit";
    lemma <- "agir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class agitV2
{
  <morpho> {
    morph <- "agit";
    lemma <- "agir";
    cat   <- v;
    mode <- ind;
    tense <- spast;
    pers <- 3;
    num <- sg
   }
}

class pourN
{
  <morpho> {
    morph <- "pour";
    lemma <- "pour";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class pourPREP
{
  <morpho> {
    morph <- "pour";
    lemma <- "pour";
    cat   <- p
   }
}

class pourPREP
{
  <morpho> {
    morph <- "pour";
    lemma <- "pour";
    cat   <- p
   }
}

class pourPREP
{
  <morpho> {
    morph <- "pour";
    lemma <- "pour";
    cat   <- p
   }
}

class avoirN
{
  <morpho> {
    morph <- "avoir";
    lemma <- "avoir";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class avoirV
{
  <morpho> {
    morph <- "avoir";
    lemma <- "avoir";
    cat   <- v;
    mode <- inf
   }
}

class garantieN
{
  <morpho> {
    morph <- "garantie";
    lemma <- "garantie";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class garantieA
{
  <morpho> {
    morph <- "garantie";
    lemma <- "garanti";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class garantieV
{
  <morpho> {
    morph <- "garantie";
    lemma <- "garantir";
    cat   <- v;
    pp-gen <- f;
    pp-num <- sg

   }
}

class organeN
{
  <morpho> {
    morph <- "organe";
    lemma <- "organe";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class gestionN
{
  <morpho> {
    morph <- "gestion";
    lemma <- "gestion";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class yN1
{
  <morpho> {
    morph <- "y";
    lemma <- "y";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class yN2
{
  <morpho> {
    morph <- "y";
    lemma <- "y";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class aN1
{
  <morpho> {
    morph <- "a";
    lemma <- "a";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class aN2
{
  <morpho> {
    morph <- "a";
    lemma <- "a";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class aV
{
  <morpho> {
    morph <- "a";
    lemma <- "avoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class vingtaineN
{
  <morpho> {
    morph <- "vingtaine";
    lemma <- "vingtaine";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- sg
   }
}

class annéesN
{
  <morpho> {
    morph <- "années";
    lemma <- "année";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class sièclesN
{
  <morpho> {
    morph <- "siècles";
    lemma <- "siècle";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class misA1
{
  <morpho> {
    morph <- "mis";
    lemma <- "mis";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class misA2
{
  <morpho> {
    morph <- "mis";
    lemma <- "mis";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class misV1
{
  <morpho> {
    morph <- "mis";
    lemma <- "mettre";
    cat   <- v;
    pp-gen <- m;
    pp-num <- sg

   }
}

class misV2
{
  <morpho> {
    morph <- "mis";
    lemma <- "mettre";
    cat   <- v;
    pp-gen <- m;
    pp-num <- pl

   }
}

class misV3
{
  <morpho> {
    morph <- "mis";
    lemma <- "mettre";
    cat   <- v;
    mode <- ind;
    tense <- spast;
    pers <- 1;
    num <- sg
   }
}

class misV4
{
  <morpho> {
    morph <- "mis";
    lemma <- "mettre";
    cat   <- v;
    mode <- ind;
    tense <- spast;
    pers <- 2;
    num <- sg
   }
}

class enPREP
{
  <morpho> {
    morph <- "en";
    lemma <- "en";
    cat   <- p
   }
}

class examenN
{
  <morpho> {
    morph <- "examen";
    lemma <- "examen";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class étéN
{
  <morpho> {
    morph <- "été";
    lemma <- "été";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class étéV
{
  <morpho> {
    morph <- "été";
    lemma <- "être";
    cat   <- v;
    pp-gen <- m;
    pp-num <- sg

   }
}

class suspenduA
{
  <morpho> {
    morph <- "suspendu";
    lemma <- "suspendu";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class suspenduV
{
  <morpho> {
    morph <- "suspendu";
    lemma <- "suspendre";
    cat   <- v;
    pp-gen <- m;
    pp-num <- sg

   }
}

class estA1
{
  <morpho> {
    morph <- "est";
    lemma <- "est";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class estA2
{
  <morpho> {
    morph <- "est";
    lemma <- "est";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class estA3
{
  <morpho> {
    morph <- "est";
    lemma <- "est";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class estA4
{
  <morpho> {
    morph <- "est";
    lemma <- "est";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class estN
{
  <morpho> {
    morph <- "est";
    lemma <- "est";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class estV
{
  <morpho> {
    morph <- "est";
    lemma <- "être";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class personnesN
{
  <morpho> {
    morph <- "personnes";
    lemma <- "personne";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class misesN
{
  <morpho> {
    morph <- "mises";
    lemma <- "mises";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class misesV
{
  <morpho> {
    morph <- "mises";
    lemma <- "mettre";
    cat   <- v;
    pp-gen <- f;
    pp-num <- pl

   }
}

class misesA
{
  <morpho> {
    morph <- "mises";
    lemma <- "mis";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class misesN
{
  <morpho> {
    morph <- "mises";
    lemma <- "mise";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class misesV1
{
  <morpho> {
    morph <- "mises";
    lemma <- "miser";
    cat   <- v;
    mode <- ind;
    pers <- 2;
    num <- sg
   }
}

class misesV2
{
  <morpho> {
    morph <- "mises";
    lemma <- "miser";
    cat   <- v;
    mode <- subj;
    pers <- 2;
    num <- sg
   }
}

class parisN
{
  <morpho> {
    morph <- "paris";
    lemma <- "pari";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- pl
   }
}

class sembleV1
{
  <morpho> {
    morph <- "semble";
    lemma <- "sembler";
    cat   <- v;
    mode <- ind;
    pers <- 1;
    num <- sg
   }
}

class sembleV2
{
  <morpho> {
    morph <- "semble";
    lemma <- "sembler";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class sembleV3
{
  <morpho> {
    morph <- "semble";
    lemma <- "sembler";
    cat   <- v;
    mode <- subj;
    pers <- 1;
    num <- sg
   }
}

class sembleV4
{
  <morpho> {
    morph <- "semble";
    lemma <- "sembler";
    cat   <- v;
    mode <- subj;
    pers <- 3;
    num <- sg
   }
}

class sembleV5
{
  <morpho> {
    morph <- "semble";
    lemma <- "sembler";
    cat   <- v;
    mode <- imp;
    pers <- 2;
    num <- sg
   }
}

class mouronN
{
  <morpho> {
    morph <- "mouron";
    lemma <- "mouron";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class passerV
{
  <morpho> {
    morph <- "passer";
    lemma <- "passer";
    cat   <- v;
    mode <- inf
   }
}

class vampireA1
{
  <morpho> {
    morph <- "vampire";
    lemma <- "vampire";
    cat   <- adj;
    gen <- m;
    num <- sg
   }
}

class vampireA2
{
  <morpho> {
    morph <- "vampire";
    lemma <- "vampire";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class vampireN
{
  <morpho> {
    morph <- "vampire";
    lemma <- "vampire";
    cat   <- n;
    det   <- -;
    gen <- m;
    num <- sg
   }
}

class visionsV1
{
  <morpho> {
    morph <- "visions";
    lemma <- "viser";
    cat   <- v;
    mode <- ind;
    tense <- past;
    pers <- 1;
    num <- pl
   }
}

class visionsV2
{
  <morpho> {
    morph <- "visions";
    lemma <- "viser";
    cat   <- v;
    mode <- subj;
    pers <- 1;
    num <- pl
   }
}

class visionsN
{
  <morpho> {
    morph <- "visions";
    lemma <- "vision";
    cat   <- n;
    det   <- -;
    gen <- f;
    num <- pl
   }
}

class fontV
{
  <morpho> {
    morph <- "font";
    lemma <- "faire";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- pl
   }
}

class intensesA1
{
  <morpho> {
    morph <- "intenses";
    lemma <- "intense";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class intensesA2
{
  <morpho> {
    morph <- "intenses";
    lemma <- "intense";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018

type CAT = {v, sv, sn, sp, p, adj}
type GEN = {m, f}
type FAM = {
  IntransitifActif
}

feature morph: string
feature lemma: string
feature cat  : CAT
feature fam  : FAM
feature gen  : GEN


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
value appelle
value aAvoir
value cloue
value cuit
value cuitImp
value cuitsImp
value cuiteImp
value cuitesImp
value eclate
value estV
value eteV
value evanouit
value faitV
value interesse
value joint
value jointPpart
value pousse
value prends
value prends2pers
value prenons
value prend
value pris
value priseImp
value tombe
value tourne
value tourneImp
value tourneeImp
value sont
value va
value videV
value videsV
value videV3
value vidonsV
value videzV
value videntV

value bois
value boule
value boules
value bout
value carotte
value carottes
value charbon
value face
value faces
value fuite
value Jean
value Marie
value nuit
value page
value pages
value porte
value geste
value parole
value profil
value profils
value sac
value sacs
value sol
value sièclesN


value bas
value basse
value basses
value grand
value grands
value grande
value grandes
value important
value importants
value importante
value importantes

value la
value le
value les
value mon
value ma
value mes
value ton
value ta
value tes
value son
value sa
value ses
value notre
value nos
value votre
value vos
value leur
value leurs

value aPrep
value dans
value par
%value quePron
%value queC
value se
value laClitic
value leClitic
value il
value elle


value lN2
value unificationN
value nN2
value avaitV
value lieuN
value dN2
value êtreN
value êtreV
value firentV
value appelN
value àPREP
value charpentiersN
value deuxN2
value pilotesN2
value pilotesN
value pilotesN2
value pilotesV2
value commandantsA
value commandantsN
value commandantsN
value eurentV
value faireV
value faceN
value faceN
value faceV5
value unN2
value dilemmeN
value faisaientV
value objetN
value ordreN
value ferontV
value uneN
value campagneN
value faisaitV
value partieN
value partieA
value partieV
value dePREP
value sonN
value territoireN
value célébritésN
value ontV
value faitA
value faitN
value faitV2
value castingN
value sN2
value agitV2
value pourN
value pourPREP
value pourPREP
value pourPREP
value avoirN
value avoirV
value garantieN
value garantieA
value garantieV
value organeN
value gestionN
value yN2
value aN2
value aV
value vingtaineN
value annéesN
value sièclesN
value misA2
value misV4
value enPREP
value examenN
value étéN
value étéV
value suspenduA
value suspenduV
value estA4
value estN
value estV
value personnesN
value misesN
value misesV
value misesA
value misesN
value misesV2
value parisN
value sembleV5
value mouronN
value passerV
value vampireA2
value vampireN
value visionsV2
value visionsN
value fontV
value intensesA2
