%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partly or fully saturated (lexicalized) noun phrases 
% These classed are variants of Benoit's noms.mg classed but having lexicalized noun heads and modifiers
% 11/02/2018
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized N: pierre (qui roule), (faire) face
% Based on Benoit's "noun" class without mark=anchor
%%%%%%%%%%%%%%%%%%%%%%%%
class mweLexNoun[idNounNode]
export
	?xLexNPTop ?xLexNHead
declare
	?xLexNPTop ?xLexNHead ?fX ?fY ?fZ ?fT ?fU ?fG ?fK

{
	<syn>{
		node xLexNPTop(color=red)[cat = n, bot = [wh = -, def = ?fT, num = ?fY, gen = ?fZ,pers = 3,neg-adv = ?fG, neg-nom = ?fK]]{
			node xLexNHead (color=red,mark=coanchor,name=idNounNode)[cat = n,top=[def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, neg-adv = ?fG,neg-nom = ?fK, det = -,pers = 3,wh = -,bar = 0]]
                 }
	}
}
%Aucune de version ce-dessous ne parse "Jean prend la porte" comme MWE. Why?
%			node (color=red,mark=nadj,mark=coanchor,name=idNounNode)[cat = n,top=[def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, neg-adv = ?fG,neg-nom = ?fK]]
%			node xNHead (color=red,mark=nadj,mark=coanchor,name=idNounNode)[cat = n,top=[def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, neg-adv = ?fG,neg-nom = ?fK]]
%			node xNHead (color=red,mark=coanchor,name=idNounNode)[cat = n,top=[def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, neg-adv = ?fG,neg-nom = ?fK,modifiable=+]]
%			node xNHead (color=red,mark=coanchor,mark=nadj,name=idNounNode)[cat = n,top=[def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, neg-adv = ?fG,neg-nom = ?fK,modifiable=-]]

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any determined argument with lexicalized N and no determiner: pierre (qui roule), (faire) face
%%%%%%%%%%%%%%%%%%%%%%%%
class mweNoDetLexNoun[idNounNode]
import
	mweLexNoun[idNounNode]
{
	<syn>{
		node xLexNPTop[bot = [det = +]]
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized N and a free determiner: (frapper à) la/sa/cette porte, (appeler à) le/son secours
% Based on Benoit's "noun" class without mark=anchor
%%%%%%%%%%%%%%%%%%%%%%%%
class mweFreeDetLexNoun[idNounNode]
import
	mweLexNoun[idNounNode]
{
	<syn>{
%		node xLexNPTop[bot = [det = -]]
		node xLexNHead[bot = [det = -]]
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Det and a modifiable N
%%%%%%%%%%%%%%%%%%%%%%%%
class mweLexDetLexNoun[idDetNode,idNounNode]
import
	mweNoDetLexNoun[idNounNode]
declare
	?xLexDet ?fY ?fZ

{
	<syn>{
		node xLexNPTop[bot = [num = ?fY, gen = ?fZ]]{
			node xLexDet (color=red,mark=coanchor,name=idDetNode)[cat = d,bot=[num = ?fY,gen = ?fZ]]
			node xLexNHead[cat = n,top=[num = ?fY,gen = ?fZ]]
                 }
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Prep and a modifiable N
% False, prepositions are handled by prep2
%%%%%%%%%%%%%%%%%%%%%%%%
%class mweLexPrepLexNoun[idPrepNode,idNounNode]
%import
%	mweNoDetLexNoun[idNounNode]
%declare
%	xLexPrep ?fY ?fZ
%
%{
%	<syn>{
%		node xLexNPTop[bot = [num = ?fY, gen = ?fZ]]{
%			node xLexPrep (color=red,mark=coanchor,name=idPrepNode)[cat = p,bot=[num = ?fY,gen = ?fZ]]
%			node xLexNHead[cat = n,top=[num = ?fY,gen = ?fZ]]
%                 }
%	}
%}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized N and Adj
% Based on Benoit's "noun" class without mark=anchor and with det=-
%%%%%%%%%%%%%%%%%%%%%%%%
class mweLexNounLexAdj[idNounNode,idAdjNode]
import
	mweNoDetLexNoun[idNounNode]
declare
	?xLexAdj ?fY ?fZ

{
	<syn>{
		node xLexNPTop[bot = [num = ?fY, gen = ?fZ]]{
			node xLexNHead[cat = n,top=[num = ?fY,gen = ?fZ]]
			node xLexAdj (color=red,mark=coanchor,name=idAdjNode)[cat = adj,bot=[num = ?fY,gen = ?fZ]]
                 }
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Det and a non-modifiable noun N
%%%%%%%%%%%%%%%%%%%%%%%%
%class mweDetNounNonModif[idDetNode,idNounNode]
%import 
%	mweDetNounModif[idDetNode,idNounNode]
%{
%	<syn>{
%		node xHead(mark=nadj)[bottom=[modifiable=-]]
%	}
%}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Det and a N (modifiable or not)
%class mweDetNoun[idDetNode,idNounNode]
%{
%	mweDetNounModif[idDetNode,idNounNode]
%	|mweDetNounNonModif[idDetNode,idNounNode]
%}



