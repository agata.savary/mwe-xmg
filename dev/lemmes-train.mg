%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmaAgirV {
  <lemma> {
    entry <- "agir";
    cat <- v;
    fam <- n0V
  }
}

class LemmaColorierV {
  <lemma> {
    entry <- "colorier";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaChoisirV {
  <lemma> {
    entry <- "choisir";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaDireV {
  <lemma> {
    entry <- "dire";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaIlFautSent {
  <lemma> {
    entry <- "falloir";
    cat <- v;
    fam <- ilVcs1
  }
}

class LemmaIlSeAgitSent {
  <lemma> {
    entry <- "agir";
    cat <- v;
    fam <- mweIlRefClVdes1
  }
}

%class LemmaIlSeAgitPourNSent {
%  <lemma> {
%    entry <- "agir";
%    cat <- v;
%    fam <- mweIlRefClVdes1pn2;
%    filter prep2 = pour
%  }
%}

class LemmaIlSeAgitDeN {
  <lemma> {
    entry <- "agir";
    cat <- v;
    fam <- mweIlRefClVden1
  }
}

class LemmaIlYA {
  <lemma> {
    entry <- "avoir";
    cat <- v;
    fam <- mweIlLocClVn1;
    filter objtype = canonical
  }
}

class LemmeAller
{
  <lemma> {
    entry <- "aller";
    cat   <- v;
    fam   <- n0Vloc1
   }
}

class LemmeAppeler
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeAvoirBar
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- n0Vnbar1
  }
}

class LemmeAvoir
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- n0Vn1
  }
}


class LemmeClouer
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- n0Vn1an2
   }
}

class LemmeCuire
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeEclater
{
  <lemma> {
    entry <- "éclater";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeEtre
{
  <lemma> {
    entry <- "être";
    cat   <- v;
    fam   <- EtreAux	
   }
}

class LemmeEtre
{
  <lemma> {
    entry <- "être";
    cat   <- v;
    fam   <- Copule	
   }
}

class LemmeFaire
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- n0Vn1
   }
}

%class LemmaSeFaireAdj {
%  <lemma> {
%    entry <- "faire";
%    cat <- v;
%    fam <- mweClCopule
%  }
%}

class LemmaGarantirV {
  <lemma> {
    entry <- "garantir";
    cat <- v;
    fam <- n0Vn1an2
  }
}

class LemmeInteresser
{
  <lemma> {
    entry <- "intéresser";
    cat   <- v;
    fam   <- n0Vn1an2
  }
}

class LemmeJoindre
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- n0Vn1an2
  }
}

class LemmaMettreV {
  <lemma> {
    entry <- "mettre";
    cat <- v;
    fam <- n0Vn1pn2
  }
}


class LemmaPasserV1 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0ClV
%    aux <- etre
  }
}

class LemmaPasserV2 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0ClVn1
%    aux <- etre
  }
}

class LemmaPasserV3 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0Vpn1
%    aux <- etre
%    prep <- pour
  }
}

class LemmaPasserV4 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0Vpn1
%    aux <- avoir
%    prep <- pour
  }
}

class LemmaPartirV {
  <lemma> {
    entry <- "partir";
    cat <- v;
    fam <- n0V
  }
}

class LemmaPleuvoirV {
  <lemma> {
    entry <- "pleuvoir";
    cat <- v;
    fam <- ilV
  }
}

class LemmePousser
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmePrendre
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmaSemblerV1 {
  <lemma> {
    entry <- "sembler";
    cat <- v;
    fam <- Copule
  }
}

class LemmaSemblerV2 {
  <lemma> {
    entry <- "sembler";
    cat <- v;
    fam <- SemiAux
  }
}

class LemmaSuspendreV {
  <lemma> {
    entry <- "suspendre";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmeTomber
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- n0V
  }
}

class LemmeTourner
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmeVider
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmaViserV {
  <lemma> {
    entry <- "viser";
    cat <- v;
    fam <- n0Vn1
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IReflVs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeSeEvanouir
{
  <lemma> {
    entry <- "évanouir";
    cat   <- v;
    fam   <- n0ClV
   }
}

class LemmeSeInteresser
{
  <lemma> {
    entry <- "intéresser";
    cat   <- v;
    fam   <- n0ClVpn1
  }
}

class LemmeSeEclater
{
  <lemma> {
    entry <- "éclater";
    cat   <- v;
    fam   <- n0ClV
  }
}

class LemmeSeTrouver
{
  <lemma> {
    entry <- "trouver";
    cat   <- v;
    fam   <- n0ClV
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


class mweLemmeAvoirLieu
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "lieu"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}



class mweLemmeAvoirLieuDeInf  %evoir lieu d'être
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1des2;
    filter dia = active;
    coanchor ObjNode -> "lieu"/n;
    filter subj = free;
    filter obj = lexicalized;
    filter objtype = canonical;
    filter objstruct = lexN;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter sentobject=free
  }
}


class mweLemmeAppelerASecours
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- mwen0Van1;
    filter subj = free;
    filter iobj = lexicalized;
    filter iobjstruct = freeDetLexN;
%    equation iobjdettype = detOrPoss;
    coanchor IObjNode -> "secours"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeClouerAuSol
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexDetLexN;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "sol"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}



class mweLesCarottesSontCuites
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = passive;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "carottes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeFaireFace
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "face"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeFairePartie
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1den2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter genitive = free;
    filter objstruct = lexN;
    coanchor ObjNode -> "partie"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeFaireProfilBas
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexNLexAdj;
    coanchor ObjNode -> "profil"/n;
    coanchor ObjAdjNode -> "bas"/adj;
    equation ObjAdjNode -> gen=m;
    equation ObjAdjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireAppel
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "appel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireAppelAN  %faire appel à quelqu'un
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "appel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireLeObjet
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1den2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    filter genitive = free;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "objet"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


class mweLemmeMettreEnExamen
{
  <lemma> {
    entry <- "mettre";
    cat   <- v;
    fam   <- mwen0Vn1pn2;
    filter subj = free;
    filter obj = free;
    filter obl = lexicalized;
    filter obltype = canonical;
    filter oblstruct = lexN;
    filter prep2 = en;
    coanchor Prep -> "en"/p;
    coanchor OblNode -> "examen"/n;
    equation OblNode -> gen=m;
    equation OblNode -> num=sg;
    equation OblNode -> modifiable=-
  }
}


class mweLaNuitTombe
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- mwen0V;
    filter dia = active;
    filter subj = lexicalized;
    filter subjstruct = lexDetLexN;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "nuit"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}


class mweLemmePousserABout
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexN;
    coanchor IObjNode -> "bout"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmePrendreLaPorte
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "porte"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


class mweLemmePrendreLaFuite
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "fuite"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}


class mweLemmePrendreLaParole
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "parole"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeJoindreLeGesteALaParole
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "geste"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "parole"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeTournerLaPage
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "page"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}

%class mweLemmeMettreTest
%{
%  <lemma> {
%    entry <- "mettre";
%    cat   <- v;
%    fam   <- mwen0Vn1;
%    filter dia = passive;
%    filter passivetype = short;
%    filter subj = free;
%    filter obj = free
%  }
%}


class mweLemmeViderSonSac
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "sac"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}

class mweLemmePorterNom
{
  <lemma> {
    entry <- "porter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "nom"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmeBois
{
  <lemma> {
    entry <- "bois";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBoule
{
  <lemma> {
    entry <- "boule";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBout
{
  <lemma> {
    entry <- "bout";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCarotte
{
  <lemma> {
    entry <- "carotte";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCharbon
{
  <lemma> {
    entry <- "charbon";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFace
{
  <lemma> {
    entry <- "face";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFuite
{
  <lemma> {
    entry <- "fuite";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeGeste
{
  <lemma> {
    entry <- "geste";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeJean
{
  <lemma> {
    entry <- "jean";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeMarie
{
  <lemma> {
    entry <- "marie";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeNuit
{
  <lemma> {
    entry <- "nuit";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePage
{
  <lemma> {
    entry <- "page";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeParole
{
  <lemma> {
    entry <- "parole";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePorte
{
  <lemma> {
    entry <- "porte";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeProfil
{
  <lemma> {
    entry <- "profil";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSac
{
  <lemma> {
    entry <- "sac";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSecours
{
  <lemma> {
    entry <- "secours";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSol
{
  <lemma> {
    entry <- "sol";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSiècle
{
  <lemma> {
    entry <- "siècle";
    cat   <- n;
    fam   <- noun
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeAujourdhuiSAnte
{
  <lemma> {
    entry <- "aujourd'hui";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeAujourdhuiSPost
{
  <lemma> {
    entry <- "aujourd'hui";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeCertainementS
{
  <lemma> {
    entry <- "certainement";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeCertainementV
{
  <lemma> {
    entry <- "certainement";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeFrequemmentSAnte
{
  <lemma> {
    entry <- "fréquemment";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeFrequemmentSPost
{
  <lemma> {
    entry <- "fréquemment";
    cat   <- adv;
    fam   <- advSPost
  }
}


class LemmeFrequemmentV
{
  <lemma> {
    entry <- "fréquemment";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeRecemmentSAnte
{
  <lemma> {
    entry <- "récemment";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeRecemmentSPost
{
  <lemma> {
    entry <- "récemment";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeRecemmentV
{
  <lemma> {
    entry <- "récemment";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeAlorsAdvS
{
  <lemma> {
    entry <- "alors";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeAlorsAdvV
{
  <lemma> {
    entry <- "alors";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeIciAdvS
{
  <lemma> {
    entry <- "ici";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeIciAdvV
{
  <lemma> {
    entry <- "ici";
    cat   <- adv;
    fam   <- advVPost
  }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmeBasPred
{
  <lemma> {
    entry <- "bas";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeGrandPred
{
  <lemma> {
    entry <- "grand";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeGrandAttr
{
  <lemma> {
    entry <- "grand";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}

class LemmeBasAttr
{
  <lemma> {
    entry <- "bas";
    cat   <- adj;
    fam   <- mweAdjAttrRight
  }
}
class LemmeImportantPred
{
  <lemma> {
    entry <- "important";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeImportantAttr
{
  <lemma> {
    entry <- "important";
    cat   <- adj;
    fam   <- n0vA
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeLe
{
  <lemma> {
    entry <- "le";
    cat   <- d;
    fam   <- stddeterminer
  }
}

class LemmeUn
{
  <lemma> {
    entry <- "un";
    cat   <- d;
    fam   <- stddeterminer
  }
}
class LemmeMonPossDet
{
  <lemma> {
    entry <- "mon";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeTonPossDet
{
  <lemma> {
    entry <- "ton";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeSonPossDet
{
  <lemma> {
    entry <- "son";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeNotrePossDet
{
  <lemma> {
    entry <- "notre";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeVotrePossDet
{
  <lemma> {
    entry <- "votre";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeLeurPossDet
{
  <lemma> {
    entry <- "leur";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeA
{
  <lemma> {
    entry <- "à";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeAPrep
{
  <lemma> {
    entry <- "a";
    cat   <- p;
    fam   <- PrepositionalPhrase
  }
}

class LemmeAPrepLoc
{
  <lemma> {
    entry <- "à";
    cat   <- p;
    fam   <- prepLoc
  }
}

class LemmeDans
{
  <lemma> {
    entry <- "dans";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeEn
{
  <lemma> {
    entry <- "en";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmePar
{
  <lemma> {
    entry <- "par";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeParVoid
{
  <lemma> {
    entry <- "par";
    cat   <- p;
    fam   <- void
  }
}

class LemmePour
{
  <lemma> {
    entry <- "pour";
    cat   <- p;
    fam   <- s0Pn1
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRONOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%class LemmeQuePron
%{
%  <lemma> {
%    entry <- "que";
%    cat   <- n;
%    fam   <- pronoun
%  }
%}

%class LemmeQueC
%{
%  <lemma> {
%    entry <- "que";
%    cat   <- c;
%    fam   <- none
%  }
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeSe
{
  <lemma> {
    entry <- "se";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeJe
{
  <lemma> {
    entry <- "je";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeIl
{
  <lemma> {
    entry <- "il";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeMoiN
{
  <lemma> {
    entry <- "moi";
    cat   <- n;
    fam   <- pronoun
  }
}

class LemmeLeClitic
{
  <lemma> {
    entry <- "le";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeYClitic
{
  <lemma> {
    entry <- "y";
    cat   <- cl;
    fam   <- CliticT
  }
}



class LemmaLaN {
  <lemma> {
    entry <- "la";
    cat <- n;
    fam <- noun
  }
}

class LemmaUnificationN {
  <lemma> {
    entry <- "unification";
    cat <- n;
    fam <- noun
  }
}

class LemmaLieuN {
  <lemma> {
    entry <- "lieu";
    cat <- n;
    fam <- noun
  }
}

class LemmaêtreN {
  <lemma> {
    entry <- "être";
    cat <- n;
    fam <- noun
  }
}

class LemmaAppelN {
  <lemma> {
    entry <- "appel";
    cat <- n;
    fam <- noun
  }
}

class LemmaCharpentierA {
  <lemma> {
    entry <- "charpentier";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaCharpentierN {
  <lemma> {
    entry <- "charpentier";
    cat <- n;
    fam <- noun
  }
}

class LemmaCharpentierN {
  <lemma> {
    entry <- "charpentier";
    cat <- n;
    fam <- noun
  }
}

class LemmaPiloteN {
  <lemma> {
    entry <- "pilote";
    cat <- n;
    fam <- noun
  }
}

class LemmaPiloteN {
  <lemma> {
    entry <- "pilote";
    cat <- n;
    fam <- noun
  }
}

class LemmaPiloteN {
  <lemma> {
    entry <- "pilote";
    cat <- n;
    fam <- noun
  }
}

class LemmaCommandantA {
  <lemma> {
    entry <- "commandant";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaCommandantN {
  <lemma> {
    entry <- "commandant";
    cat <- n;
    fam <- noun
  }
}

class LemmaCommandantN {
  <lemma> {
    entry <- "commandant";
    cat <- n;
    fam <- noun
  }
}

class LemmaFaceN {
  <lemma> {
    entry <- "face";
    cat <- n;
    fam <- noun
  }
}

class LemmaFaceN {
  <lemma> {
    entry <- "face";
    cat <- n;
    fam <- noun
  }
}

class LemmaUnN {
  <lemma> {
    entry <- "un";
    cat <- n;
    fam <- noun
  }
}

class LemmaDilemmeN {
  <lemma> {
    entry <- "dilemme";
    cat <- n;
    fam <- noun
  }
}

class LemmaObjetN {
  <lemma> {
    entry <- "objet";
    cat <- n;
    fam <- noun
  }
}

class LemmaOrdreN {
  <lemma> {
    entry <- "ordre";
    cat <- n;
    fam <- noun
  }
}

class LemmaDN {
  <lemma> {
    entry <- "d";
    cat <- n;
    fam <- noun
  }
}

class LemmaUneN {
  <lemma> {
    entry <- "une";
    cat <- n;
    fam <- noun
  }
}

class LemmaCampagneN {
  <lemma> {
    entry <- "campagne";
    cat <- n;
    fam <- noun
  }
}

class LemmaPartieN {
  <lemma> {
    entry <- "partie";
    cat <- n;
    fam <- noun
  }
}

class LemmaPartiA {
  <lemma> {
    entry <- "parti";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaSonN {
  <lemma> {
    entry <- "son";
    cat <- n;
    fam <- noun
  }
}

class LemmaTerritoireN {
  <lemma> {
    entry <- "territoire";
    cat <- n;
    fam <- noun
  }
}

class LemmaCélébritéN {
  <lemma> {
    entry <- "célébrité";
    cat <- n;
    fam <- noun
  }
}

class LemmaFaitA {
  <lemma> {
    entry <- "fait";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaFaitN {
  <lemma> {
    entry <- "fait";
    cat <- n;
    fam <- noun
  }
}

class LemmaCastingN {
  <lemma> {
    entry <- "casting";
    cat <- n;
    fam <- noun
  }
}

class LemmaAvoirN {
  <lemma> {
    entry <- "avoir";
    cat <- n;
    fam <- noun
  }
}

class LemmaGarantieN {
  <lemma> {
    entry <- "garantie";
    cat <- n;
    fam <- noun
  }
}

class LemmaGarantiA {
  <lemma> {
    entry <- "garanti";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaOrganeN {
  <lemma> {
    entry <- "organe";
    cat <- n;
    fam <- noun
  }
}

class LemmaYN {
  <lemma> {
    entry <- "y";
    cat <- n;
    fam <- noun
  }
}

class LemmaAN {
  <lemma> {
    entry <- "a";
    cat <- n;
    fam <- noun
  }
}

class LemmaAnnéeN {
  <lemma> {
    entry <- "année";
    cat <- n;
    fam <- noun
  }
}

class LemmaSiècleN {
  <lemma> {
    entry <- "siècle";
    cat <- n;
    fam <- noun
  }
}

class LemmaMisA {
  <lemma> {
    entry <- "mis";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaExamenN {
  <lemma> {
    entry <- "examen";
    cat <- n;
    fam <- noun
  }
}

class LemmaJeanN {
  <lemma> {
    entry <- "Jean";
    cat <- n;
    fam <- noun
  }
}

class LemmaJeanN {
  <lemma> {
    entry <- "jean";
    cat <- n;
    fam <- noun
  }
}

class LemmaétéN {
  <lemma> {
    entry <- "été";
    cat <- n;
    fam <- noun
  }
}

class LemmaSuspenduA {
  <lemma> {
    entry <- "suspendu";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaEstA {
  <lemma> {
    entry <- "est";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaEstN {
  <lemma> {
    entry <- "est";
    cat <- n;
    fam <- noun
  }
}

class LemmaPersonneN {
  <lemma> {
    entry <- "personne";
    cat <- n;
    fam <- noun
  }
}

class LemmaMisesN {
  <lemma> {
    entry <- "mises";
    cat <- n;
    fam <- noun
  }
}

class LemmaMisA {
  <lemma> {
    entry <- "mis";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaMiseN {
  <lemma> {
    entry <- "mise";
    cat <- n;
    fam <- noun
  }
}

class LemmaPariN {
  <lemma> {
    entry <- "pari";
    cat <- n;
    fam <- noun
  }
}

class LemmaMouronN {
  <lemma> {
    entry <- "mouron";
    cat <- n;
    fam <- noun
  }
}

class LemmaPourN {
  <lemma> {
    entry <- "pour";
    cat <- n;
    fam <- noun
  }
}

class LemmaVampireA {
  <lemma> {
    entry <- "vampire";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaVampireN {
  <lemma> {
    entry <- "vampire";
    cat <- n;
    fam <- noun
  }
}

class LemmaVisionN {
  <lemma> {
    entry <- "vision";
    cat <- n;
    fam <- noun
  }
}

class LemmaIntenseA {
  <lemma> {
    entry <- "intense";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Valuation part
% Agata Savary 4 March 2018

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


