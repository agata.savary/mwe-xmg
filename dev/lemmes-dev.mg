%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018
% MWE lemmas: mweLemmaIlFautSent, mweLemmaIlSeAgitSent, mweLemmaIlSeAgitDeN, mweLemmaIlYA, mweLemmaPleuvoirV, mweLemmeSeEvanouir, mweLemmeSeInteresser, mweLemmeSeEclater, mweLemmeSeTrouver, mweLemmeAvoirLieu, mweLemmeAvoirLieuDeInf, mweLemmeAppelerASecours, mweLemmeClouerAuSol, mweLesCarottesSontCuites, mweLemmeFaireFace,  mweLemmeFairePartie, mweLemmeFaireProfilBas, mweLemmeFaireAppel, mweLemmeFaireAppelAN, mweLemmeFaireLeObjet, mweLemmeMettreEnExamen, mweLaNuitTombe, mweLemmePousserABout, mweLemmePrendreLaPorte, mweLemmePrendreLaFuite, mweLemmePrendreLaParole, mweLemmeJoindreLeGesteALaParole, mweLemmeTournerLaPage, mweLemmeViderSonSac, mweLemmePorterNom
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmaAgirV {
  <lemma> {
    entry <- "agir";
    cat <- v;
    fam <- n0V
  }
}

class LemmaColorierV {
  <lemma> {
    entry <- "colorier";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaChoisirV {
  <lemma> {
    entry <- "choisir";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaDireV {
  <lemma> {
    entry <- "dire";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmeAller
{
  <lemma> {
    entry <- "aller";
    cat   <- v;
    fam   <- n0Vloc1
   }
}

class LemmeAppeler
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeAvoirBar
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- n0Vnbar1
  }
}

class LemmeAvoir
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- n0Vn1
  }
}


class LemmeClouer
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- n0Vn1an2
   }
}

class LemmeCuire
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeDéciderDeInf
{
  <lemma> {
    entry <- "décider";
    cat   <- v;
    fam   <- n0Vdes1
   }
}

class LemmaDevoirV {
  <lemma> {
    entry <- "devoir";
    cat <- v;
    fam <- SemiAux
  }
}

class LemmeEclater
{
  <lemma> {
    entry <- "éclater";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeEtre
{
  <lemma> {
    entry <- "être";
    cat   <- v;
    fam   <- EtreAux	
   }
}

class LemmeEtre
{
  <lemma> {
    entry <- "être";
    cat   <- v;
    fam   <- Copule	
   }
}

class LemmeFaire
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeFaireInf
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- n0Vcs1
   }
}

%class LemmaSeFaireAdj {
%  <lemma> {
%    entry <- "faire";
%    cat <- v;
%    fam <- mweClCopule
%  }
%}

class LemmaGarantirV {
  <lemma> {
    entry <- "garantir";
    cat <- v;
    fam <- n0Vn1an2
  }
}

class LemmeInteresser
{
  <lemma> {
    entry <- "intéresser";
    cat   <- v;
    fam   <- n0Vn1an2
  }
}

class LemmeJoindre
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- n0Vn1an2
  }
}

class LemmeJouer
{
  <lemma> {
    entry <- "jouer";
    cat   <- v;
    fam   <- n0Vn1
  }
}


class LemmaMettreV {
  <lemma> {
    entry <- "mettre";
    cat <- v;
    fam <- n0Vn1pn2
  }
}


class LemmaPasserV1 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0ClV
%    aux <- etre
  }
}

class LemmaPasserV2 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0ClVn1
%    aux <- etre
  }
}

class LemmaPasserV3 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0Vpn1
%    aux <- etre
%    prep <- pour
  }
}

class LemmaPasserV4 {
  <lemma> {
    entry <- "passer";
    cat <- v;
    fam <- n0Vpn1
%    aux <- avoir
%    prep <- pour
  }
}

class LemmaPartirV {
  <lemma> {
    entry <- "partir";
    cat <- v;
    fam <- n0V
  }
}

class mweLemmaPleuvoirV {
  <lemma> {
    entry <- "pleuvoir";
    cat <- v;
    fam <- ilV
  }
}

class LemmePorterV
{
  <lemma> {
    entry <- "porter";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmePousser
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmePrendre
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmaSemblerV1 {
  <lemma> {
    entry <- "sembler";
    cat <- v;
    fam <- Copule
  }
}

class LemmaSemblerV2 {
  <lemma> {
    entry <- "sembler";
    cat <- v;
    fam <- SemiAux
  }
}

class LemmaSuspendreV {
  <lemma> {
    entry <- "suspendre";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmeTomber
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- n0V
  }
}

class LemmeTourner
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmeTuer
{
  <lemma> {
    entry <- "tuer";
    cat   <- v;
    fam   <- n0Vn1
  }
}


class LemmeVider
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmaViserV {
  <lemma> {
    entry <- "viser";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmeVoirInf
{
  <lemma> {
    entry <- "voir";
    cat   <- v;
    fam   <- n0Vn1cs2
   }
}

class LemmaVouloirV {
  <lemma> {
    entry <- "vouloir";
    cat <- v;
    fam <- SemiAux
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IReflVs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweLemmeSeEvanouir
{
  <lemma> {
    entry <- "évanouir";
    cat   <- v;
    fam   <- n0ClV
   }
}

class mweLemmeSeInteresser
{
  <lemma> {
    entry <- "intéresser";
    cat   <- v;
    fam   <- n0ClVpn1
  }
}

class mweLemmeSeEclater
{
  <lemma> {
    entry <- "éclater";
    cat   <- v;
    fam   <- n0ClV
  }
}

class mweLemmeSeTrouver
{
  <lemma> {
    entry <- "trouver";
    cat   <- v;
    fam   <- n0ClV
  }
}

class mweLemmeSeFaireInf
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0ClVs1Inf
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweLemmaIlFautSent {
  <lemma> {
    entry <- "falloir";
    cat <- v;
    fam <- ilVcs1
  }
}

class mweLemmaIlFautN {
  <lemma> {
    entry <- "falloir";
    cat <- v;
    fam <- mweIlVn1
  }
}

class mweLemmaIlFautDeN {
  <lemma> {
    entry <- "falloir";
    cat <- v;
    fam <- mwen0IlVden1
  }
}

class mweLemmaIlSeAgitSent {
  <lemma> {
    entry <- "agir";
    cat <- v;
    fam <- mweIlRefClVdes1
  }
}

%class LemmaIlSeAgitPourNSent {
%  <lemma> {
%    entry <- "agir";
%    cat <- v;
%    fam <- mweIlRefClVdes1pn2;
%    filter prep2 = pour
%  }
%}

class mweLemmaIlSeAgitDeN {
  <lemma> {
    entry <- "agir";
    cat <- v;
    fam <- mweIlRefClVden1
  }
}

class mweLemmaIlYAN {  %il y a Pierre
  <lemma> {
    entry <- "avoir";
    cat <- v;
    fam <- mweIlLocClVn1
  }
}

class mweLemmaIlYAGen {  %il y a de la concurrence, il y en a
  <lemma> {
    entry <- "avoir";
    cat <- v;
    fam <- mweIlLocClVden1
  }
}

class mweLemmaIlYANbar {   %Il y a contrôle
  <lemma> {
    entry <- "avoir";
    cat <- v;
    fam <- mweIlLocClVnbar1
  }
}


class mweLemmeAvoirLieu
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "lieu"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=+;
    filter objtype = canonical
  }
}



class mweLemmeAvoirLieuDeInf  %evoir lieu d'être
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1des2;
    filter dia = active;
    coanchor ObjNode -> "lieu"/n;
    filter subj = free;
    filter obj = lexicalized;
    filter objtype = canonical;
    filter objstruct = lexN;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter sentobject=free
  }
}


class mweLemmeAppelerASecours
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- mwen0Van1;
    filter subj = free;
    filter iobj = lexicalized;
    filter iobjstruct = freeDetLexN;
%    equation iobjdettype = detOrPoss;
    coanchor IObjNode -> "secours"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeClouerAuSol
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexDetLexN;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "sol"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}



class mweLesCarottesSontCuites
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = passive;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "carottes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeFaireFace
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "face"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeFairePartie
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1den2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter genitive = free;
    filter objstruct = lexN;
    coanchor ObjNode -> "partie"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeFaireProfilBas
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexNLexAdj;
    coanchor ObjNode -> "profil"/n;
    coanchor ObjAdjNode -> "bas"/adj;
    equation ObjAdjNode -> gen=m;
    equation ObjAdjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireAppel
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "appel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireAppelAN  %faire appel à quelqu'un
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "appel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireLeObjet
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1den2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    filter genitive = free;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "objet"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeJouerRole
{
  <lemma> {
    entry <- "jouer";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "rôle"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}

class mweLemmeMettreEnExamen
{
  <lemma> {
    entry <- "mettre";
    cat   <- v;
    fam   <- mwen0Vn1pn2;
    filter subj = free;
    filter obj = free;
    filter obl = lexicalized;
    filter obltype = canonical;
    filter oblstruct = lexN;
    filter prep2 = en;
    coanchor Prep -> "en"/p;
    coanchor OblNode -> "examen"/n;
    equation OblNode -> gen=m;
    equation OblNode -> num=sg;
    equation OblNode -> modifiable=-
  }
}

class mweLemmeMettreFin
{
  <lemma> {
    entry <- "mettre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "fin"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLaNuitTombe
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- mwen0V;
    filter dia = active;
    filter subj = lexicalized;
    filter subjstruct = lexDetLexN;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "nuit"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}


class mweLemmePousserABout
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexN;
    coanchor IObjNode -> "bout"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmePrendreLaPorte
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "porte"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


class mweLemmePrendreLaFuite
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "fuite"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}


class mweLemmePrendreLaParole
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "parole"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeJoindreLeGesteALaParole
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "geste"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "parole"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeTournerLaPage
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "page"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}

%class mweLemmeMettreTest
%{
%  <lemma> {
%    entry <- "mettre";
%    cat   <- v;
%    fam   <- mwen0Vn1;
%    filter dia = passive;
%    filter passivetype = short;
%    filter subj = free;
%    filter obj = free
%  }
%}


class mweLemmeViderSonSac
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "sac"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}

class mweLemmePorterNom
{
  <lemma> {
    entry <- "porter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "nom"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmeBois
{
  <lemma> {
    entry <- "bois";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBoule
{
  <lemma> {
    entry <- "boule";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBout
{
  <lemma> {
    entry <- "bout";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCarotte
{
  <lemma> {
    entry <- "carotte";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCharbon
{
  <lemma> {
    entry <- "charbon";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFace
{
  <lemma> {
    entry <- "face";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFuite
{
  <lemma> {
    entry <- "fuite";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeGeste
{
  <lemma> {
    entry <- "geste";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeJean
{
  <lemma> {
    entry <- "jean";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeMarie
{
  <lemma> {
    entry <- "marie";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeNuit
{
  <lemma> {
    entry <- "nuit";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePage
{
  <lemma> {
    entry <- "page";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeParole
{
  <lemma> {
    entry <- "parole";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePorte
{
  <lemma> {
    entry <- "porte";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeProfil
{
  <lemma> {
    entry <- "profil";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSac
{
  <lemma> {
    entry <- "sac";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSecours
{
  <lemma> {
    entry <- "secours";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSol
{
  <lemma> {
    entry <- "sol";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSiècle
{
  <lemma> {
    entry <- "siècle";
    cat   <- n;
    fam   <- noun
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeAujourdhuiSAnte
{
  <lemma> {
    entry <- "aujourd'hui";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeAujourdhuiSPost
{
  <lemma> {
    entry <- "aujourd'hui";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeCertainementS
{
  <lemma> {
    entry <- "certainement";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeCertainementV
{
  <lemma> {
    entry <- "certainement";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeDemainSAnte
{
  <lemma> {
    entry <- "demain";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeDemainSPost
{
  <lemma> {
    entry <- "demain";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeDemainV
{
  <lemma> {
    entry <- "demain";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeFrequemmentSAnte
{
  <lemma> {
    entry <- "fréquemment";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeFrequemmentSPost
{
  <lemma> {
    entry <- "fréquemment";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeFrequemmentV
{
  <lemma> {
    entry <- "fréquemment";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeMaintenantSAnte
{
  <lemma> {
    entry <- "maintenant";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeMaintenantSPost
{
  <lemma> {
    entry <- "maintenant";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeMaintenantV
{
  <lemma> {
    entry <- "maintenant";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeRecemmentSAnte
{
  <lemma> {
    entry <- "récemment";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeRecemmentSPost
{
  <lemma> {
    entry <- "récemment";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeRecemmentV
{
  <lemma> {
    entry <- "récemment";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeAlorsAdvS
{
  <lemma> {
    entry <- "alors";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeAlorsAdvV
{
  <lemma> {
    entry <- "alors";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeIciAdvS
{
  <lemma> {
    entry <- "ici";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeIciAdvV
{
  <lemma> {
    entry <- "ici";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeIciAdvSPost
{
  <lemma> {
    entry <- "ici";
    cat   <- adv;
    fam   <- advSPost
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmeBasPred
{
  <lemma> {
    entry <- "bas";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeGrandPred
{
  <lemma> {
    entry <- "grand";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeGrandAttr
{
  <lemma> {
    entry <- "grand";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}

class LemmeBasAttr
{
  <lemma> {
    entry <- "bas";
    cat   <- adj;
    fam   <- mweAdjAttrRight
  }
}

class LemmeIncroyableDeInf
{
  <lemma> {
    entry <- "incroyable";
    cat   <- adj;
    fam   <- n0vAdes1
  }
}

class LemmeImportantPred
{
  <lemma> {
    entry <- "important";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeImportantAttr
{
  <lemma> {
    entry <- "important";
    cat   <- adj;
    fam   <- n0vA
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeLe
{
  <lemma> {
    entry <- "le";
    cat   <- d;
    fam   <- stddeterminer
  }
}

class LemmeCeDET
{
  <lemma> {
    entry <- "ce";
    cat   <- d;
    fam   <- stddeterminer
  }
}

class LemmeUn
{
  <lemma> {
    entry <- "un";
    cat   <- d;
    fam   <- stddeterminer
  }
}
class LemmeMonPossDet
{
  <lemma> {
    entry <- "mon";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeTonPossDet
{
  <lemma> {
    entry <- "ton";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeSonPossDet
{
  <lemma> {
    entry <- "son";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeNotrePossDet
{
  <lemma> {
    entry <- "notre";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeVotrePossDet
{
  <lemma> {
    entry <- "votre";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeLeurPossDet
{
  <lemma> {
    entry <- "leur";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeA
{
  <lemma> {
    entry <- "à";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeAPrep
{
  <lemma> {
    entry <- "a";
    cat   <- p;
    fam   <- PrepositionalPhrase
  }
}

class LemmeAPrepLoc
{
  <lemma> {
    entry <- "à";
    cat   <- p;
    fam   <- prepLoc
  }
}

class LemmeDans
{
  <lemma> {
    entry <- "dans";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeEn
{
  <lemma> {
    entry <- "en";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmePar
{
  <lemma> {
    entry <- "par";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeParVoid
{
  <lemma> {
    entry <- "par";
    cat   <- p;
    fam   <- void
  }
}

class LemmePour
{
  <lemma> {
    entry <- "pour";
    cat   <- p;
    fam   <- s0Pn1
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRONOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%class LemmeQuePron
%{
%  <lemma> {
%    entry <- "que";
%    cat   <- n;
%    fam   <- pronoun
%  }
%}

%class LemmeQueC
%{
%  <lemma> {
%    entry <- "que";
%    cat   <- c;
%    fam   <- none
%  }
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeSe
{
  <lemma> {
    entry <- "se";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeJe
{
  <lemma> {
    entry <- "je";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeIl
{
  <lemma> {
    entry <- "il";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeCelaN
{
  <lemma> {
    entry <- "cela";
    cat   <- n;
    fam   <- pronoun
  }
}

class LemmeEnCl
{
  <lemma> {
    entry <- "en";
    cat   <- cl;
    fam   <- CliticT
  }
}


class LemmeMoiN
{
  <lemma> {
    entry <- "moi";
    cat   <- n;
    fam   <- pronoun
  }
}

class LemmeLeClitic
{
  <lemma> {
    entry <- "le";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeYClitic
{
  <lemma> {
    entry <- "y";
    cat   <- cl;
    fam   <- CliticT
  }
}



class LemmaUneN {
  <lemma> {
    entry <- "une";
    cat <- n;
    fam <- noun
  }
}

class LemmaévolutionN {
  <lemma> {
    entry <- "évolution";
    cat <- n;
    fam <- noun
  }
}

class LemmaLieuN {
  <lemma> {
    entry <- "lieu";
    cat <- n;
    fam <- noun
  }
}

class LemmaAppelN {
  <lemma> {
    entry <- "appel";
    cat <- n;
    fam <- noun
  }
}

class LemmaServiceN {
  <lemma> {
    entry <- "service";
    cat <- n;
    fam <- noun
  }
}

class LemmaObjetN {
  <lemma> {
    entry <- "objet";
    cat <- n;
    fam <- noun
  }
}

class LemmaCritiqueA {
  <lemma> {
    entry <- "critique";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaCritiqueN {
  <lemma> {
    entry <- "critique";
    cat <- n;
    fam <- noun
  }
}

class LemmaCritiqueN {
  <lemma> {
    entry <- "critique";
    cat <- n;
    fam <- noun
  }
}

class LemmaCritiqueN {
  <lemma> {
    entry <- "critique";
    cat <- n;
    fam <- noun
  }
}

class LemmaDireN {
  <lemma> {
    entry <- "dire";
    cat <- n;
    fam <- noun
  }
}

class LemmaUnN {
  <lemma> {
    entry <- "un";
    cat <- n;
    fam <- noun
  }
}

class LemmaNomN {
  <lemma> {
    entry <- "nom";
    cat <- n;
    fam <- noun
  }
}

class LemmaTubeN {
  <lemma> {
    entry <- "tube";
    cat <- n;
    fam <- noun
  }
}

class LemmaCarteN {
  <lemma> {
    entry <- "carte";
    cat <- n;
    fam <- noun
  }
}

class LemmaYN {
  <lemma> {
    entry <- "y";
    cat <- n;
    fam <- noun
  }
}

class LemmaAN {
  <lemma> {
    entry <- "a";
    cat <- n;
    fam <- noun
  }
}

class LemmaConcurrenceN {
  <lemma> {
    entry <- "concurrence";
    cat <- n;
    fam <- noun
  }
}

class LemmaSaintA {
  <lemma> {
    entry <- "saint";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaSaintN {
  <lemma> {
    entry <- "saint";
    cat <- n;
    fam <- noun
  }
}

class LemmaPorteA {
  <lemma> {
    entry <- "porte";
    cat <- adj;
    fam <- mweAdjAttrRight
  }
}

class LemmaPorteN {
  <lemma> {
    entry <- "porte";
    cat <- n;
    fam <- noun
  }
}

class LemmaPariN {
  <lemma> {
    entry <- "pari";
    cat <- n;
    fam <- noun
  }
}

class LemmaSonN {
  <lemma> {
    entry <- "son";
    cat <- n;
    fam <- noun
  }
}

class LemmaTombeauN {
  <lemma> {
    entry <- "tombeau";
    cat <- n;
    fam <- noun
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Valuation part
% Agata Savary 4 March 2018

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

value LemmaAgirV
value LemmeAller
value LemmeAppeler
value LemmeAvoir
value LemmeAvoirBar
value LemmaColorierV
value LemmaChoisirV
value LemmaDevoirV
value LemmaDireV
value LemmeEclater
value LemmeEtre
value LemmeClouer
value LemmeCuire
value LemmeDéciderDeInf
value LemmeFaire
value LemmeFaireInf
%value LemmaSeFaireAdj
value LemmaGarantirV
value LemmeJouer
value LemmeInteresser
value LemmeJoindre
value LemmaMettreV
%value mweLemmeMettreTest
value LemmaPasserV1
value LemmaPasserV2
value LemmaPasserV3
value LemmaPasserV4
value LemmaPartirV
value mweLemmaPleuvoirV
value LemmePorterV
value LemmePousser
value LemmePrendre
value LemmaSemblerV1
value LemmaSemblerV2
value LemmaSuspendreV
value LemmeTomber
value LemmeTourner
value LemmeTuer
value LemmeVider
value LemmaViserV
value LemmeVoirInf
value LemmaVouloirV

value mweLemmeSeEvanouir
value mweLemmeSeInteresser
value mweLemmeSeEclater
value mweLemmeSeTrouver

value mweLemmaIlFautSent
value mweLemmaIlFautN
value mweLemmaIlFautDeN
value mweLemmaIlSeAgitSent
%value LemmaIlSeAgitPourNSent
value mweLemmaIlSeAgitDeN
value mweLemmaIlYAN
value mweLemmaIlYAGen
value mweLemmaIlYANbar

%value mweLemmeAllerAuCharbon
%value mweLemmeAvoirLesBoules
value mweLemmeAppelerASecours
value mweLemmeAvoirLieu
value mweLemmeClouerAuSol
value mweLesCarottesSontCuites
value mweLemmeFaireFace
value mweLemmeFaireAppel
value mweLemmeFaireAppelAN
value mweLemmeFaireLeObjet
value mweLemmeFairePartie
value mweLemmeFaireProfilBas
value mweLemmeJouerRole
value mweLemmeMettreEnExamen
value mweLemmeMettreFin
value mweLaNuitTombe
value mweLemmePousserABout
value mweLemmePrendreLaPorte
value mweLemmePrendreLaFuite
value mweLemmePrendreLaParole
value mweLemmeJoindreLeGesteALaParole
value mweLemmeTournerLaPage

value LemmeBois
value LemmeBoule
value LemmeBout
value LemmeCarotte
value LemmeCharbon
value LemmeFace
value LemmeJean
value LemmeMarie
value LemmeNuit
value LemmePage
value LemmePorte
value LemmeGeste
value LemmeParole
value LemmeProfil
value LemmeFuite
value LemmeSac
value LemmeSecours
value LemmeSol
value LemmeSiècle

value LemmeLe
value LemmeCeDET
value LemmeEnCl
value LemmeUn
value LemmeMonPossDet
value LemmeTonPossDet
value LemmeSonPossDet
value LemmeNotrePossDet
value LemmeVotrePossDet
value LemmeLeurPossDet

value LemmeAujourdhuiSAnte
value LemmeAujourdhuiSPost
value LemmeCertainementS
value LemmeCertainementV
value LemmeDemainSAnte
value LemmeDemainSPost
value LemmeDemainV
value LemmeFrequemmentSAnte
value LemmeFrequemmentSPost
value LemmeFrequemmentV
value LemmeMaintenantSAnte
value LemmeMaintenantSPost
value LemmeMaintenantV
value LemmeRecemmentSAnte
value LemmeRecemmentSPost
value LemmeRecemmentV
value LemmeAlorsAdvS
value LemmeAlorsAdvV
value LemmeIciAdvS
value LemmeIciAdvV
value LemmeIciAdvSPost

value LemmeBasPred
value LemmeBasAttr
value LemmeGrandPred
value LemmeGrandAttr
value LemmeImportantPred
value LemmeImportantAttr
value LemmeIncroyableDeInf

value LemmeA
value LemmeAPrep
value LemmeAPrepLoc
value LemmeDans
value LemmeEn
value LemmePar
value LemmeParVoid
value LemmePour

%value LemmeQuePron
%value LemmeQueC
value LemmeSe
value LemmeJe
value LemmeIl
value LemmeCelaN
value LemmeMoiN
value LemmeLeClitic
value LemmeYClitic


value LemmaTubeN
value LemmaDireN
value LemmaNomN
value LemmaServiceN
value LemmaAppelN
value LemmaYN
value LemmaCritiqueN
value LemmaCritiqueA
value LemmaSonN
value LemmaPariN
value LemmaObjetN
value LemmaConcurrenceN
value LemmaAN
value LemmaPorteN
value LemmaPorteA
value LemmaSaintN
value LemmaSaintA
value LemmaCarteN
value LemmaTombeauN
value LemmaLieuN
value LemmaUnN
value LemmaUneN
value LemmaévolutionN
