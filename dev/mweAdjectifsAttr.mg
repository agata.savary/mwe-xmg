%%% Added by Agata Savary
%%% 25 Jan 2018
%%% Attributive adjectives




%% Attributive adjective (on the left or on the right of the noun)
%% e.g. une grande porte, une porte importante
class mweAdjAttr
export xHead xFoot
declare 
	?xR ?xFoot ?xHead ?fX ?fT ?fY ?fZ ?fU ?fW ?fG ?fK
{
	<syn>{
		node ?xR(color=red)[cat = n,bot=[det = ?fX, def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, wh = ?fW,neg-adv = ?fG,neg-nom = ?fK]];
                node ?xHead(color=red,mark=anchor)[cat = adj,bot=[num = ?fY,gen = ?fZ]];
                node ?xFoot(color=red,mark=foot)[cat = n,top=[det = ?fX, def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, wh = ?fW,neg-adv = ?fG, neg-nom = ?fK]];
		?xR -> ?xHead;
		?xR -> ?xFoot	
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Left-modifying adjectives
%% e.g. une grande porte
class mweAdjAttrLeft
import mweAdjAttr[]
{
	<syn>{
		?xHead >> ?xFoot
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Right-modifying adjectives
%% e.g. une porte importante
class mweAdjAttrRight
import mweAdjAttr[]
{
	<syn>{
		?xFoot >> ?xHead	
	}
}


