 include header.mg
 include PredArgs.mg
 include verbes.mg
 include adjectifs.mg
 include noms.mg
 include adverbes.mg
 include misc.mg

include mwePredArgs.mg
include mweVerbesLex.mg
include mweNomsLex.mg
include mweAdjectifsAttr.mg

 
% VALUATION
%%%%%%%%%%%%

value propername
value pronoun	%eux
value noun
value advSAnte 	%certainement
value advVPost
value advSPost %ici
value stddeterminer
%value mwePossDeterminer
value n0Vn1 	%prendre, pousser, colorier
%value n0Van1
%value n0Vcs1	%faire + Inf
%value n0Vn1cs2	%voir N Inf
%value n0Vdes1	%décider de
%value ilV 	%il pleut
%value ilVcs1	%il faut le faire
value n0V 	%tomber
%%value n0Vn1an2 	%joindre, clouer
%%value n0Vn1den2		%obtenir
%%value n0Vloc1	%aller
%%value n0Vnbar1	%avoir
%value PrepositionalPhrase %a le bout
%value s0Pn1
%value SemiAux 	%devoir
value CliticT
%value n0ClV %Jean s'évanouit
%%value n0ClVpn1 %Marie se interesse Jean
%%value n0Vnbar1
%value EtreAux
%value Copule
%value n0vA
%value n0vAdes1 %difficile

%value mweAdjAttrLeft
%value mweAdjAttrRight

%MWEs
% la nuit tombe, Jean tombe
value mwen0V
% prendre la porte, prendre la fuite, prendre la parole, tourner la page, faire profil bas, faire appel
value mwen0Vn1
% appeler à le/son secours
%value mwen0Van1
% For faire face à N, clouer N au sol, pousser N à bout, joindre le geste à la parole, faire appel à N
%value mwen0Vn1an2
% faire l'objet de N
%value mwen0Vn1den2
% avoir lieu de croire
%value mwen0Vn1des2
% il s'agit d N
%value mweIlRefClVden1
%il se agit de Inf
%value mweIlRefClVdes1
%il se agit pour Jean de agit
%false?? value mweIlRefClVdes1pn2
% il y a des années
%value mweIlLocClVn1
%%il ya de la concurrence
%value mweIlLocClVden1
%mettre en examen
%value mwen0Vn1pn2
%%se faire belle
%%value mweClCopule
%%il faut l'accord, il la faut
%value mweIlVn1
%%il y a contrôle
%value mweIlLocClVnbar1
%%il se fait prier
%value mwen0ClVs1Inf
%il faut de la pratique, il en faut
%value mwen0IlVden1




