%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Class part
% Agata Savary 4 March 2018

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeAller
{
  <lemma> {
    entry <- "aller";
    cat   <- v;
    fam   <- n0Vloc1
   }
}

class LemmeAppeler
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeAvoir
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- n0Vnbar1
  }
}

class LemmeClouer
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- n0Vn1an2
   }
}

class LemmeCuire
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeEclater
{
  <lemma> {
    entry <- "éclater";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmeEtre
{
  <lemma> {
    entry <- "être";
    cat   <- v;
    fam   <- EtreAux	
   }
}

class LemmeEtre
{
  <lemma> {
    entry <- "être";
    cat   <- v;
    fam   <- Copule	
   }
}

class LemmeFaire
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- n0Vn1
   }
}


class LemmeInteresser
{
  <lemma> {
    entry <- "intéresser";
    cat   <- v;
    fam   <- n0Vn1an2
  }
}

class LemmeJoindre
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- n0Vn1an2
  }
}

class LemmePousser
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmePrendre
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmeTomber
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- n0V
  }
}

class LemmeTourner
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- n0Vn1
  }
}

class LemmeVider
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- n0Vn1
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IReflVs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeSeEvanouir
{
  <lemma> {
    entry <- "évanouir";
    cat   <- v;
    fam   <- n0ClV
   }
}

class LemmeSeInteresser
{
  <lemma> {
    entry <- "intéresser";
    cat   <- v;
    fam   <- n0ClVpn1
  }
}

class LemmeSeEclater
{
  <lemma> {
    entry <- "éclater";
    cat   <- v;
    fam   <- n0ClV
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%class mweLemmeAllerAuCharbon
%{
%  <lemma> {
%    entry <- "aller";
%    cat   <- v;
%    fam   <- mwen0Van1LexDetN;
%    filter dia = active;
%    coanchor IObjDetNode -> "le"/d;
%    coanchor IObjNode -> "charbon"/n;
%    equation IObjNode -> gen=m;
%    equation IObjNode -> num=sg;
%    equation IObjNode -> modifiable=-
%  }
%}

%class mweLemmeAvoirLesBoules
%{
%  <lemma> {
%    entry <- "avoir";
%    cat   <- v;
%    fam   <- mwedian0Vn1LexDetNActive;
%    coanchor ObjDetNode -> "les"/d;
%    coanchor ObjNode -> "boules"/n 
%    
%  }
%}

%class mweLemmeClouerAuSol
%{
%  <lemma> {
%    entry <- "clouer";
%    cat   <- v;
%    fam   <- mwen0Vn1an2LexDetN;
%    coanchor IObjDetNode -> "le"/d;
%    coanchor IObjNode -> "sol"/n;
%    equation IObjNode -> gen=m;
%    equation IObjNode -> num=sg;
%    equation IObjNode -> modifiable=-
%  }
%}

class mweLemmeAppelerASecours
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- mwen0Van1;
    filter subj = free;
    filter iobj = lexicalized;
    filter iobjstruct = freeDetLexN;
%    equation iobjdettype = detOrPoss;
    coanchor IObjNode -> "secours"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeClouerAuSol
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexDetLexN;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "sol"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


%class mweLesCarottesSontCuites
%{
%  <lemma> {
%    entry <- "cuire";
%    cat   <- v;
%    fam   <- 	mwen0Vn1LexDetN;
%    filter dia = passive;
%    filter passivetype = short;
%    coanchor ObjDetNode -> "les"/d;
%    coanchor ObjNode -> "carottes"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=pl;
%    equation ObjNode -> modifiable=-
%  }
%}

class mweLesCarottesSontCuites
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = passive;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "carottes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


%class mweLemmeFaireFace
%{
%  <lemma> {
%    entry <- "faire";
%    cat   <- v;
%    fam   <- mwen0Vn1LexNan2;
%    filter dia = active;
%    coanchor ObjNode -> "face"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-;
%    filter objtype = canonical
%  }
%}

class mweLemmeFaireFace
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = free;
    filter objstruct = lexN;
    coanchor ObjNode -> "face"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

%class mweLemmeFaireProfilBas
%{
%  <lemma> {
%    entry <- "faire";
%    cat   <- v;
%    fam   <- 	mwen0Vn1LexNAdj;
%    filter dia = active;
%    coanchor ObjNode -> "profil"/n;
%    coanchor ObjAdjNode -> "bas"/adj;
%    equation ObjAdjNode -> gen=m;
%    equation ObjAdjNode -> num=sg;
%    filter objtype = canonical
%  }
%}

class mweLemmeFaireProfilBas
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexNLexAdj;
    coanchor ObjNode -> "profil"/n;
    coanchor ObjAdjNode -> "bas"/adj;
    equation ObjAdjNode -> gen=m;
    equation ObjAdjNode -> num=sg;
    filter objtype = canonical
  }
}


%class mweLaNuitTombe
%{
%  <lemma> {
%    entry <- "tomber";
%    cat   <- v;
%    fam   <- n0LexDetNV;
%    filter dia = active;
%    coanchor SubjDetNode -> "la"/d;
%    coanchor SubjNode -> "nuit"/n;
%    equation SubjNode -> gen=f;
%    equation SubjNode -> num=sg
%  }
%}

class mweLaNuitTombe
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- mwen0V;
    filter dia = active;
    filter subj = lexicalized;
    filter subjstruct = lexDetLexN;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "nuit"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}


% Jean pousse Marie à bout, Marie a été poussée à bout, Marie a été poussée à bout par Jean,
%class mweLemmePousserABout
%{
%  <lemma> {
%    entry <- "pousser";
%    cat   <- v;
%    fam   <- mwen0Vn1an2LexN;
%    coanchor IObjNode -> "bout"/n;
%    equation IObjNode -> gen=m;
%    equation IObjNode -> num=sg;
%    equation IObjNode -> modifiable=-
%  }
%}

class mweLemmePousserABout
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexN;
    coanchor IObjNode -> "bout"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

%class mweLemmePrendreLaPorte
%{
%  <lemma> {
%    entry <- "prendre";
%    cat   <- v;
%    fam   <- 	mwen0Vn1LexDetN;
%    filter dia = active;
%    coanchor ObjDetNode -> "la"/d;
%%Plus tard:    coanchor ObjDetNode -> "la"/d , gen=f, num=sg, modifiable=- ;
%%equation ObjDetNode -> gen=f;
%    coanchor ObjNode -> "porte"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-;
%    filter objtype = canonical
%  }
%}

class mweLemmePrendreLaPorte
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "porte"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


%class mweLemmePrendreLaFuite
%{
%  <lemma> {
%    entry <- "prendre";
%    cat   <- v;
%    fam   <- mwen0Vn1LexDetN;
%    filter dia = active;
%    coanchor ObjDetNode -> "la"/d;
%    coanchor ObjNode -> "fuite"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=sg;
%    filter objtype = canonical
%  }
%}

class mweLemmePrendreLaFuite
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "fuite"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}


%class mweLemmePrendreLaParole
%{
%  <lemma> {
%    entry <- "prendre";
%    cat   <- v;
%    fam   <- mwen0Vn1LexDetN;
%    coanchor ObjDetNode -> "la"/d;
%    coanchor ObjNode -> "parole"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-
%  }
%}

class mweLemmePrendreLaParole
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "parole"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


% Jean a joint le geste à la parole, le geste a été joint à la parole
% *le geste que Jean a joint à la parole
%class mweLemmeJoindreLeGesteALaParole
%{
%  <lemma> {
%    entry <- "joindre";
%    cat   <- v;
%    fam   <- n0Vn1LexDetNan2LexDetN;
%    coanchor ObjDetNode -> "le"/d;
%    coanchor ObjNode -> "geste"/n;
%    equation ObjNode -> gen=m;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-;
%    filter objtype = canonical;
%    coanchor IObjDetNode -> "la"/d;
%    coanchor IObjNode -> "parole"/n;
%    equation IObjNode -> gen=f;
%    equation IObjNode -> num=sg;
%    equation IObjNode -> modifiable=-
%  }
%}

class mweLemmeJoindreLeGesteALaParole
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "geste"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "parole"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


%Jean tourne la page, la page a été tournée par Jean, la page a été tournée, la page que Jean tourne
%class mweLemmeTournerLaPage
%{
%  <lemma> {
%    entry <- "tourner";
%    cat   <- v;
%    fam   <- mwen0Vn1LexDetN;
%    coanchor ObjDetNode -> "la"/d;
%    coanchor ObjNode -> "page"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=sg
%  }
%}

class mweLemmeTournerLaPage
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "page"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}


%class mweLemmeViderSonSac
%{
%  <lemma> {
%    entry <- "vider";
%    cat   <- v;
%    fam   <- mwen0Vn1LexDetN;
%    coanchor ObjNode -> "sac"/n;
%    equation ObjNode -> gen=m;
%    equation ObjNode -> num=sg
%  }
%}

class mweLemmeViderSonSac
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "sac"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmeBois
{
  <lemma> {
    entry <- "bois";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBoule
{
  <lemma> {
    entry <- "boule";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBout
{
  <lemma> {
    entry <- "bout";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCarotte
{
  <lemma> {
    entry <- "carotte";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCharbon
{
  <lemma> {
    entry <- "charbon";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFace
{
  <lemma> {
    entry <- "face";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFuite
{
  <lemma> {
    entry <- "fuite";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeGeste
{
  <lemma> {
    entry <- "geste";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeJean
{
  <lemma> {
    entry <- "jean";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeMarie
{
  <lemma> {
    entry <- "marie";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeNuit
{
  <lemma> {
    entry <- "nuit";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePage
{
  <lemma> {
    entry <- "page";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeParole
{
  <lemma> {
    entry <- "parole";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePorte
{
  <lemma> {
    entry <- "porte";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeProfil
{
  <lemma> {
    entry <- "profil";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSac
{
  <lemma> {
    entry <- "sac";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSecours
{
  <lemma> {
    entry <- "secours";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSol
{
  <lemma> {
    entry <- "sol";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeSiècle
{
  <lemma> {
    entry <- "siècle";
    cat   <- n;
    fam   <- noun
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeCertainementS
{
  <lemma> {
    entry <- "certainement";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeCertainementV
{
  <lemma> {
    entry <- "certainement";
    cat   <- adv;
    fam   <- advVPost
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmeBasPred
{
  <lemma> {
    entry <- "bas";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeGrandPred
{
  <lemma> {
    entry <- "grand";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeGrandAttr
{
  <lemma> {
    entry <- "grand";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}

class LemmeBasAttr
{
  <lemma> {
    entry <- "bas";
    cat   <- adj;
    fam   <- mweAdjAttrRight
  }
}
class LemmeImportantPred
{
  <lemma> {
    entry <- "important";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeImportantAttr
{
  <lemma> {
    entry <- "important";
    cat   <- adj;
    fam   <- n0vA
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeLe
{
  <lemma> {
    entry <- "le";
    cat   <- d;
    fam   <- stddeterminer
  }
}

class LemmeMonPossDet
{
  <lemma> {
    entry <- "mon";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeTonPossDet
{
  <lemma> {
    entry <- "ton";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeSonPossDet
{
  <lemma> {
    entry <- "son";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeNotrePossDet
{
  <lemma> {
    entry <- "notre";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeVotrePossDet
{
  <lemma> {
    entry <- "votre";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}

class LemmeLeurPossDet
{
  <lemma> {
    entry <- "leur";
    cat   <- d;
    fam   <- mwePossDeterminer
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeA
{
  <lemma> {
    entry <- "à";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeAPrep
{
  <lemma> {
    entry <- "a";
    cat   <- p;
    fam   <- PrepositionalPhrase
  }
}

class LemmeAPrepLoc
{
  <lemma> {
    entry <- "à";
    cat   <- p;
    fam   <- prepLoc
  }
}

class LemmeDans
{
  <lemma> {
    entry <- "dans";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmePar
{
  <lemma> {
    entry <- "par";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeParVoid
{
  <lemma> {
    entry <- "par";
    cat   <- p;
    fam   <- void
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRONOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%class LemmeQuePron
%{
%  <lemma> {
%    entry <- "que";
%    cat   <- n;
%    fam   <- pronoun
%  }
%}

%class LemmeQueC
%{
%  <lemma> {
%    entry <- "que";
%    cat   <- c;
%    fam   <- none
%  }
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeSe
{
  <lemma> {
    entry <- "se";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeIl
{
  <lemma> {
    entry <- "il";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeLeClitic
{
  <lemma> {
    entry <- "le";
    cat   <- cl;
    fam   <- CliticT
  }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Valuation part
% Agata Savary 4 March 2018

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

value LemmeAller
value LemmeAppeler
value LemmeAvoir
value LemmeEclater
value LemmeEtre
value LemmeClouer
value LemmeCuire
value LemmeFaire
value LemmeInteresser
value LemmeJoindre
value LemmePousser
value LemmePrendre
value LemmeTomber
value LemmeTourner
value LemmeVider

value LemmeSeEvanouir
value LemmeSeInteresser
value LemmeSeEclater

%value mweLemmeAllerAuCharbon
%value mweLemmeAvoirLesBoules
value mweLemmeAppelerASecours
value mweLemmeClouerAuSol
value mweLesCarottesSontCuites
%value mweLemmeFaireFace
value mweLemmeFaireProfilBas
value mweLaNuitTombe
value mweLemmePousserABout
value mweLemmePrendreLaPorte
value mweLemmePrendreLaFuite
value mweLemmePrendreLaParole
value mweLemmeJoindreLeGesteALaParole
value mweLemmeTournerLaPage

value LemmeBois
value LemmeBoule
value LemmeBout
value LemmeCarotte
value LemmeCharbon
value LemmeFace
value LemmeJean
value LemmeMarie
value LemmeNuit
value LemmePage
value LemmePorte
value LemmeGeste
value LemmeParole
value LemmeProfil
value LemmeFuite
value LemmeSac
value LemmeSecours
value LemmeSol
value LemmeSiècle

value LemmeLe
value LemmeMonPossDet
value LemmeTonPossDet
value LemmeSonPossDet
value LemmeNotrePossDet
value LemmeVotrePossDet
value LemmeLeurPossDet

value LemmeCertainementS
value LemmeCertainementV

value LemmeBasPred
value LemmeBasAttr
value LemmeGrandPred
value LemmeGrandAttr
value LemmeImportantPred
value LemmeImportantAttr

value LemmeA
value LemmeAPrep
value LemmeAPrepLoc
value LemmeDans
value LemmePar
value LemmeParVoid

%value LemmeQuePron
%value LemmeQueC
value LemmeSe
value LemmeIl
value LemmeLeClitic

