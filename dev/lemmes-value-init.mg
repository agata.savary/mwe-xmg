%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while developing the first draft of a metagrammar
% Valuation part
% Agata Savary 4 March 2018

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

value LemmaAgirV
value LemmeAller
value LemmeAppeler
value LemmeAvoir
value LemmeAvoirBar
value LemmaColorierV
value LemmaChoisirV
value LemmaDevoirV
value LemmaDireV
value LemmeEclater
value LemmeEtre
value LemmeClouer
value LemmeCuire
value LemmeDéciderDeInf
value LemmeFaire
value LemmeFaireInf
%value LemmaSeFaireAdj
value LemmaGarantirV
value LemmeJouer
value LemmeInteresser
value LemmeJoindre
value LemmaMettreV
%value mweLemmeMettreTest
value LemmaPasserV1
value LemmaPasserV2
value LemmaPasserV3
value LemmaPasserV4
value LemmaPartirV
value mweLemmaPleuvoirV
value LemmePorterV
value LemmePousser
value LemmePrendre
value LemmaSemblerV1
value LemmaSemblerV2
value LemmaSuspendreV
value LemmeTomber
value LemmeTourner
value LemmeTuer
value LemmeVider
value LemmaViserV
value LemmeVoirInf
value LemmaVouloirV

value mweLemmeSeEvanouir
value mweLemmeSeInteresser
value mweLemmeSeEclater
value mweLemmeSeTrouver

value mweLemmaIlFautSent
value mweLemmaIlFautN
value mweLemmaIlFautDeN
value mweLemmaIlSeAgitSent
%value LemmaIlSeAgitPourNSent
value mweLemmaIlSeAgitDeN
value mweLemmaIlYAN
value mweLemmaIlYAGen
value mweLemmaIlYANbar

%value mweLemmeAllerAuCharbon
%value mweLemmeAvoirLesBoules
value mweLemmeAppelerASecours
value mweLemmeAvoirLieu
value mweLemmeClouerAuSol
value mweLesCarottesSontCuites
value mweLemmeFaireFace
value mweLemmeFaireAppel
value mweLemmeFaireAppelAN
value mweLemmeFaireLeObjet
value mweLemmeFairePartie
value mweLemmeFaireProfilBas
value mweLemmeJouerRole
value mweLemmeMettreEnExamen
value mweLemmeMettreFin
value mweLaNuitTombe
value mweLemmePousserABout
value mweLemmePrendreLaPorte
value mweLemmePrendreLaFuite
value mweLemmePrendreLaParole
value mweLemmeJoindreLeGesteALaParole
value mweLemmeTournerLaPage

value LemmeBois
value LemmeBoule
value LemmeBout
value LemmeCarotte
value LemmeCharbon
value LemmeFace
value LemmeJean
value LemmeMarie
value LemmeNuit
value LemmePage
value LemmePorte
value LemmeGeste
value LemmeParole
value LemmeProfil
value LemmeFuite
value LemmeSac
value LemmeSecours
value LemmeSol
value LemmeSiècle

value LemmeLe
value LemmeCeDET
value LemmeEnCl
value LemmeUn
value LemmeMonPossDet
value LemmeTonPossDet
value LemmeSonPossDet
value LemmeNotrePossDet
value LemmeVotrePossDet
value LemmeLeurPossDet

value LemmeAujourdhuiSAnte
value LemmeAujourdhuiSPost
value LemmeCertainementS
value LemmeCertainementV
value LemmeDemainSAnte
value LemmeDemainSPost
value LemmeDemainV
value LemmeFrequemmentSAnte
value LemmeFrequemmentSPost
value LemmeFrequemmentV
value LemmeMaintenantSAnte
value LemmeMaintenantSPost
value LemmeMaintenantV
value LemmeRecemmentSAnte
value LemmeRecemmentSPost
value LemmeRecemmentV
value LemmeAlorsAdvS
value LemmeAlorsAdvV
value LemmeIciAdvS
value LemmeIciAdvV
value LemmeIciAdvSPost

value LemmeBasPred
value LemmeBasAttr
value LemmeGrandPred
value LemmeGrandAttr
value LemmeImportantPred
value LemmeImportantAttr
value LemmeIncroyableDeInf

value LemmeA
value LemmeAPrep
value LemmeAPrepLoc
value LemmeDans
value LemmeEn
value LemmePar
value LemmeParVoid
value LemmePour

%value LemmeQuePron
%value LemmeQueC
value LemmeSe
value LemmeJe
value LemmeIl
value LemmeCelaN
value LemmeMoiN
value LemmeLeClitic
value LemmeYClitic

