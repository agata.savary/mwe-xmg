%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vebal phrases with free or lexicalized arguments
% These classed are variants of Benoit's verbes.mg classed, but having some non lexicalized and some lexicalized arguments
% 12/02/2018
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0V: Jean tombe, il tombe, la nuit tombe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% Free or lexicalized subject + verb in active voice 
% Based on dian0VActive
%%%%%%%%%%%%%%%%%%%%%%%%
class mwedian0Vactive
{
	mweSubject[]; 
	activeVerbMorphology[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Impersonal subject subject + verb in active voice 
% Based on dian0Vimpersonal
%%%%%%%%%%%%%%%%%%%%%%%%
%class mwedian0Vimpersonal
%{
%	ImpersonalSubject[] ; mweObject[]; activeVerbMorphology[]
%}

%%%%%%%%%%%%%%%%%%%%%%%%
% Free or lexicalized subject + verb
% Based on n0V
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0V
{
	mwedian0Vactive[] *=[dia = active]
%	|mwedian0Vimpersonal[] *=[dia = impers]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1: Jean pousse Marie, Marie tourne la page, la nuit porte conseil, la réunion a lieu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object in active voice
% Based on dian0Vn1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwedian0Vn1Active
{
	mwedian0Vactive[];
	mweObject[]
}

%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object in short passive voice (la page est trournée)
% Based on dian0Vn1ShortPassive
%%%%%%%%%%%%%%%%%%%%%%%%
class mwedian0Vn1ShortPassive
{
%	mweSubjectPassiveLexStruct[];  		%The subject is free or lexicalized
	mweSubjectPassive[];  			%The subject is free or lexicalized
	passiveVerbMorphology[]			%The verb is anchored
} 

%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object in passive voice (la page est trournée par Marie)
% Based on dian0Vn1Passive
%%%%%%%%%%%%%%%%%%%%%%%%
class mwedian0Vn1Passive
{
	mwedian0Vn1ShortPassive[];  	%la page est trournée
	CAgent[]			%par Marie
} 

%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object
% Based on n0Vn1
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1{
	mwedian0Vn1Active[] *=[dia = active]
	| mwedian0Vn1Passive[]  *=[dia = passive, passivetype = full]
%	| dian0Vn1dePassive[]
	| mwedian0Vn1ShortPassive[]  *=[dia = passive, passivetype = short]
%	| dian0Vn1ImpersonalPassive[]
	%| dian0Vn1middle[]
%	| dian0Vn1Reflexive[]		
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Van1: Jean pousse au bout, Marie appelle au secours
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized iobject in active voice
% Based on dian0Van1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwedian0Van1Active
{
	mwedian0Vactive[];
	mweIObject[]
}

%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized iobject
% Based on n0Van1
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Van1{
	mwedian0Van1Active[] *=[dia = active]
%	|dian0Van1Reflexive[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1an2: Jean pousse Marie au bout, Marie fait face aux difficultés, Marie joint le geste à la parole
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object + free/lexicalized iobject
% Based on n0Vn1an2
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1an2{
	mwen0Vn1[];
	mweIObject[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1an2: Ils font l'objet d'un ordre
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object + free/lexicalized genitive object
% Based on n0Vn1den2
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1den2{
	mwen0Vn1[];
	mweGenitive[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1des2: l'unification a lieu d'être
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object + free/lexicalized sentential "de" complement
% Based on n0Vn1des2
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1des2{
	mwen0Vn1[];
	mweSententialDeObject[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1pn2: mettre N en examen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + verb + free/lexicalized object + free/lexicalized oblique object
% Based on n0Vn1pn2
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1pn2
declare
        ?X
{
   {
	mwen0Vn1[];
	mweOblique[] *=[prep = ?X]
   } *=[prep2 = ?X]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweClCopule: les vision se font intenses, il se fait marin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
% Free/lexicalized subject + reflexive clitic + verb + free/lexicalized object adjective
%%%%%%%%%%%%%%%%%%%%%%%%
class mweClCopule
{
	Copule[];
	reflexiveVerbMorphology[]  
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Classes completing the original metagrammar, with not co-anchoring

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlRefClVdes1: il s'agit de une partir (previously: ilClVdes1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlRefClVdes1{ 
	ImpersonalSubject[];
	reflexiveVerbMorphology[];
	SententialDeObject[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlRefClVden1: il s'agit de une porte (previously ilClVden1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlRefClVden1{
	ImpersonalSubject[]; 
	reflexiveVerbMorphology[];      
	Genitive[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlRefClVdes1pn2: il s'agit pour Jean de agir (previously ilClVpn1des2)
% based on n0Vpn1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlRefClVdes1pn2
declare 
	?X
{
  {
	mweIlRefClVdes1[];
	Oblique[] *=[prep = ?X]
  }*=[prep2 = ?X]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlLocClVn1: il y a des siècles 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlLocClVn1{ 
	ImpersonalSubject[];
	CliticLocative[];
	activeVerbMorphology[];
	Object[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlLocClVnbar1: il y a contrôle 
% based on n0Vnbar1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlLocClVnbar1
{
	ImpersonalSubject[];
	CliticLocative[];
	activeVerbMorphology[];
	CanonicalNBar[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlLocClVden1: il y a de la concurrence 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlLocClVden1{ 
	ImpersonalSubject[];
	CliticLocative[];
	activeVerbMorphology[];
	Genitive[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweIlVn1: il faut l'accord 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIlVn1{
	ImpersonalSubject[];
	activeVerbMorphology[];
	Object[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0IlVden1: il faut de la pratique, il en faut
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0IlVden1{
	ImpersonalSubject[];
	activeVerbMorphology[];
	Genitive[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0ClVs1Inf: Jean se fait prier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class mwen0ClVs1Inf{
	Subject[]; 
	reflexiveVerbMorphology[]; 
	CanonicalSententialObjectInFinitive[]
}



