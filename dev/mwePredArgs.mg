%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized predicate arguments
% These classes are variants of Benoit's PredArgs.mg classes (but not imposing substitution/adjunction).
% They serve both free and lexicalized arguments. To handle free arguments, substitution and foot nodes have to be restored (as in the original classes).
% 16/10/2017
% by Agata Savary and Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized subject: free or lexicalized
% Based on CanonicalSubject (without mark=subst)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalSubject
import
        RealisedNonExtractedSubject[]
declare 
	?fG ?fK
{
        <syn>{
	     node xVN [top=[neg-nom = ?fG],bot=[neg-adv = ?fK]];
             node xSubj[cat = n,top=[neg-nom = ?fG,neg-adv = ?fK, wh = -,func=suj]]
	 }
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Relative subject: free or lexicalized
% Based on RelativeSubject (with no mark=foot for xfoot, and xfoot replaced by xRelSubHead)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweRelativeSubject
import 
	BoundedExtraction[]
export
	?xRelSubHead
declare
	?xRel ?fT ?fU ?fW ?fX ?fY ?fZ ?xQui ?xRelSubHead
{
	<syn>{
		node xRel(color=red)[cat = n,bot=[det = ?fX, def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, wh = ?fW, bar = 3]]{
			node xRelSubHead(color=red)[cat=n,top=[det = ?fX, def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, wh = ?fW,bar = @{0,1,2,3}]]
			node xS(mark=nadj)[bot=[mode=ind,wh = -]]{
				node xArg(color=red,extracted= +)[cat=c]{
					node xQui(color=red,mark=flex)[cat=qui]
				}
				node xVN[top = [mode = @{ind,subj}]]
			}
		}
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized subject 
%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubjectLex
export 
	?LexSubj
declare 
	?LexSubj ?Can ?Rel
{
    {
	?Can = mweCanonicalSubject[] *= [subjtype = canonical]
	;
	?LexSubj = ?Can.xSubj
    }
    |
    {
	?Rel = mweRelativeSubject[] *=[subjtype = relative] 
	;
	?LexSubj = ?Rel.xRelSubHead
    }
%	|CleftSubject[]
%	|InfinitiveSubject[]
% 	|ImperativeSubject[]
% 	|InvertedNominalSubject[]
%	|InterrogInvSubject[]
}
%Those alternatives are discarted since they make the lexicalized components disappear:
%	|CliticSubject[]
%	|whSubject[]

%%%%%%%%%%%%%%%%%%%%%%%%%
% Subject lexicalized with a particular structure
%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubjectLexStruct
import
	mweSubjectLex[]
declare
	?lexNP
{
    {
	?lexNP = mweLexDetLexNoun[SubjDetNode,SubjNode] *= [subjstruct=lexDetLexN]
	| ?lexNP = mweFreeDetLexNoun[SubjNode] *= [subjstruct=freeDetLexN] %Later make the distinction of determiners more precise: le/son (detPoss), le/un/son/cette (free)
	| ?lexNP = mweNoDetLexNoun[SubjNode] *= [subjstruct=lexN]
	| ?lexNP = mweLexNounLexAdj[SubjNode,SubjAdjNode] *= [subjstruct=lexNLexAdj]
    };
    ?LexSubj = ?lexNP.xLexNPTop
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Subject: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubject
{
	Subject[] *= [subj=free]
	| mweSubjectLexStruct[] *= [subj=lexicalized]
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Passive subject lexicalized with a particular structure
%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubjectPassiveLexStruct
%import
%	mweSubjectLex[]
export
	?LexSubjPass
declare
	? LexSubjPass ?lexNP ?subLex
{
%   ?Can = mweCanonicalSubject[]; 
   ?subLex = mweSubjectLex[];
   ?LexSubjPass = ?subLex.LexSubj;
    {
	?lexNP = mweLexDetLexNoun[ObjDetNode,ObjNode] *= [objstruct=lexDetLexN]
	| ?lexNP = mweFreeDetLexNoun[ObjNode] *= [objstruct=freeDetLexN] %Later make the distinction of determiners more precise: le/son (detPoss), le/un/son/cette (free)
	| ?lexNP = mweNoDetLexNoun[ObjNode] *= [objstruct=lexN]
	| ?lexNP = mweLexNounLexAdj[ObjNode,ObjAdjNode] *= [objstruct=lexNLexAdj]
    };
    ?LexSubjPass = ?lexNP.xLexNPTop
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Subject in passive voice: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweSubjectPassive
{
	Subject[] *= [obj=free]
	| mweSubjectPassiveLexStruct[] *= [obj=lexicalized]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DIRECT OBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized direct object: free or lexicalized
% Based on CanonicalObject (without mark=subst)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalObject
import
        CanonicalnonSubjectArg[]
declare 
	?fK ?fG
{
        <syn>{
		node xVN[top=[neg-nom = ?fK],bot=[neg-adv = ?fG]];
                node xtop[cat = n,top=[det = +,neg-adv = ?fG,neg-nom = ?fK,func=obj]]
        }
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Unbounded relative clause: free or lexicalized
% Based on UnboundedRelative (without mark=foot for xRelf)
% Future work: modify Benoit's UnboundedRelative class so that it inherits from the class below, just adding mark=foot
%%%%%%%%%%%%%%%%%%%%%%%%
class mweUnboundedRelative
import 
	UnboundedExtraction[]
export
	?xRelf
declare
	?xRelf ?xReltop ?fX ?fT ?fY ?fZ ?fU ?fW
{
	<syn>{
		node xReltop(color=red)[cat = n,bot=[det = ?fX, def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, wh = ?fW,bar = 3]]{
			node xRelf(color=red)[cat = n,top=[det = ?fX, def = ?fT, num = ?fY,gen = ?fZ,pers = ?fU, wh = ?fW,bar=@{0,1,2,3}]]
			node xSe(mark=nadj)
		}
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Relative object: free or lexicalized
% Based on RelativeObject
%%%%%%%%%%%%%%%%%%%%%%%%
class mweRelativeObject
export
	?xRelTopxRelf
declare
	?NCO  ?xQue ?xRelTop ?xRelToptop ?xRelTopxRelf
{
	?xRelTop = mweUnboundedRelative[];
	?xRelToptop = xRelTop.xtop;
	?xRelTopxRelf = ?xRelTop.xRelf;
	<syn>{
		node xRelToptop[cat = c]{
		     node xQue(color = red,mark = flex)[cat = que]
		}	
	};
	?NCO = NonCanonicalObject[];
	?xRelTop.xVN = NCO.xVNAgr;
	?xRelTop.xtop = NCO.xObjAgr
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized object
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObjectLex
export 
	?LexObj
declare 
	?LexObj ?Can ?Rel
{
    { 
       ?Can=mweCanonicalObject[] *=[objtype = canonical]  %Exporte le noeud xtop
       ;
       ?LexObj=?Can.xtop
    }  
%	|reflexiveAccusative[]  %To try with co-anchors
    |
    {
      ?Rel=mweRelativeObject[] *=[objtype = relative]   %Exporte le noeud xRelTopxRelf
      ;
      ?LexObj=?Rel.xRelTopxRelf
     }
%%	|CleftObject[] %To try with co-anchors: c'est la page du deuil qu'elle tourne 
}
%Those alternatives are discarted since they make the lexicalized components disappear:
%%	|CliticObjectII[]
%%	|CliticObject3[] *=[objtype = clitic]  %Clitics can't work with co-anchors (they don't appear in the sentence)
%%	|CliticObject3[] *=[objtype = clitic]
%%	|CliticGenitive[] %L'ingénieur (présente des résultats | en présente)
%%	|whObject[] %Same problem as with clitics

%%%%%%%%%%%%%%%%%%%%%%%%%
% Object lexicalized with a particular structure
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObjectLexStruct
import
	mweObjectLex[]
declare
	?lexNP
{
    {
	?lexNP = mweLexDetLexNoun[ObjDetNode,ObjNode] *= [objstruct=lexDetLexN]
	| ?lexNP = mweFreeDetLexNoun[ObjNode] *= [objstruct=freeDetLexN] %Later make the distinction of determiners more precise: le/son (detPoss), le/un/son/cette (free)
	| ?lexNP = mweNoDetLexNoun[ObjNode] *= [objstruct=lexN]
	| ?lexNP = mweLexNounLexAdj[ObjNode,ObjAdjNode] *= [objstruct=lexNLexAdj]
    };
    ?LexObj = ?lexNP.xLexNPTop
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Object: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweObject
{
	Object[] *= [obj=free]
	| mweObjectLexStruct[] *= [obj=lexicalized]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INDIRECT OBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized PP (not expecting a substitution)
% Based on CanonicalPP (without mark=subst)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalPP
import
        CanonicalnonSubjectArg[]
export
        xArg xX
declare
        ?xArg ?xX ?fG ?fK
{
        <syn>{
                node xtop[cat = pp]{
                        node xX (color=red)[cat = p]
                        node xArg(color=red)[cat = n, top = [det = +]]  

                }
        };
	<syn>{
		node xArg[top = [neg-adv = ?fG,neg-nom = ?fK]];
		node xVN [top = [neg-nom = ?fK],bot = [neg-adv = ?fG]]
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized non oblique PP (not expecting a substitution)
% Based on CanonicalPPnonOblique (imports mweCanonicalPP instead of CanonicalPP)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalPPnonOblique
import 
	mweCanonicalPP[]
export 
	xPrep
declare
	?xPrep
{
	<syn>{
		node xX{
			node xPrep(color=red,mark=flex)
		}
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized indirect object (not expecting a substitution)
% Based on CanonicalIobject (imports mweCanonicalPPnonOblique instead of CanonicalPPnonOblique)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalIObject
import 
	mweCanonicalPPnonOblique[]

{
	<syn>{
		node xPrep[cat = à]; 
		node xArg[top = [func = iobj]]	
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized indirect object
%%%%%%%%%%%%%%%%%%%%%%%%
class mweIObjectLex
export 
	?LexIObj
declare 
	?LexIObj ?Can
{
   {
	?Can = mweCanonicalIObject[] *=[iobjtype = canonical]
	;
	?LexIObj=?Can.xArg
   }
%	|whIobject[]
%	|CliticIobjectII[]
%	|CliticIobject3[]
%	|CliticLocative[] %Jean pense à Marie / y pense (pas de distinction ici) 
%	|reflexiveDative[]
%	|RelativeIobject[]
%	|CleftIobjectOne[]
%	|CleftIobjectTwo[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Indirect object lexicalized with a particular structure
%%%%%%%%%%%%%%%%%%%%%%%%
class mweIObjectLexStruct
import
	mweIObjectLex[]
declare
	?lexNP
{
    {
	?lexNP = mweLexDetLexNoun[IObjDetNode,IObjNode] *= [iobjstruct=lexDetLexN]
	| ?lexNP = mweFreeDetLexNoun[IObjNode] *= [iobjstruct=freeDetLexN] %Later make the distinction of determiners more precise: le/son (detPoss), le/un/son/cette (free)
	| ?lexNP = mweNoDetLexNoun[IObjNode] *= [iobjstruct=lexN]
	| ?lexNP = mweLexNounLexAdj[IObjNode,IObjAdjNode] *= [iobjstruct=lexNLexAdj]
    };
    ?LexIObj = ?lexNP.xLexNPTop
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Indirect object: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweIObject
{
	Iobject[] *= [iobj=free]
	| mweIObjectLexStruct[] *= [iobj=lexicalized]
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OBLIQUE OBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized oblique PP (not expecting a substitution)
% Based on CanonicalOblique (imports mweCanonicalPP instead of CanonicalPP)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalOblique[]
import 
	mweCanonicalPP[]
declare 
	?X

{
	<syn>{
		node xX(color=red, mark=coanchor, name=Prep);
		node xArg[top = [func = obl]]	
	}*=[prep = ?X]
	%% We don't use the ?X anymore
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized oblique object
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObliqueLex[]
export 
	?LexObl
declare 
	?LexObl ?Can
{
   {
	?Can = mweCanonicalOblique[] *=[obltype = canonical]
	;
	?LexObl=?Can.xArg
   }
%	|whOblique[]
%	|RelativeOblique[]
%	|CleftObliqueOne[]
%	|CleftObliqueTwo[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Oblique object lexicalized with a particular structure
%%%%%%%%%%%%%%%%%%%%%%%%
class mweObliqueLexStruct
import
	mweObliqueLex[]
declare
	?lexNP
{
    {

	?lexNP = mweLexDetLexNoun[OblDetNode,OblNode] *= [oblstruct=lexDetLexN]
	| ?lexNP = mweFreeDetLexNoun[OblNode] *= [oblstruct=freeDetLexN] %Later make the distinction of determiners more precise: le/son (detPoss), le/un/son/cette (free)
	| ?lexNP = mweNoDetLexNoun[OblNode] *= [oblstruct=lexN]
	| ?lexNP = mweLexNounLexAdj[OblNode,OblAdjNode] *= [oblstruct=lexNLexAdj]
%	| ?lexNP = mweLexPrepLexNoun[OblPrepNode,OblNode] *= [oblstruct=lexPrepLexN] %Wrong: the preposition is encoded by prep2
    };
    ?LexObl = ?lexNP.xLexNPTop
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Oblique object: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweOblique
{
	Oblique[] *= [obl=free]
	|
	mweObliqueLexStruct[] *= [obl=lexicalized]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SENTENTIAL OBJECTS: avoir lieu d'être
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
% Sentential "de" object: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweSententialDeObject
{
	SententialDeObject[] *= [sentobject=free]
%	| mweSententialDeObjectLexStruct[] *= [sentobject=lexicalized]
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENITIVE OBJECT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
% Genitive object: free or lexicalized
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweGenitive
{
	Genitive[] *= [genitive=free]
%	| mweGenitiveLexStruct[] *= [genitive=lexicalized]
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized genitive object - to test
%%%%%%%%%%%%%%%%%%%%%%%%
%class mweGenitiveLex
%export 
%	?LexGen
%declare 
%	?LexGen ?Can
%{
%   {
%	?Can = CanonicalGenitive[] *=[gentype = canonical]
%	;
%	?LexGen=?Can.xArg
%   }
%
%%	|CliticGenitive[]
%%	|whGenitive[]
%	|RelativeGenitive[]
%%	|CleftGenitiveOne[]
%%	|CleftGenitiveTwo[]
%%	|CleftDont[]
%}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Genitive object lexicalized with a particular structure
%%%%%%%%%%%%%%%%%%%%%%%%
%class mweGenitiveLexStruct
%import
%	mweGenitiveLex[]
%declare
%	?lexNP
%{
%    {
%	?lexNP = mweLexDetLexNoun[IObjDetNode,IObjNode] *= [genstruct=lexDetLexN]
%	| ?lexNP = mweFreeDetLexNoun[IObjNode] *= [genstruct=freeDetLexN] %Later make the distinction of determiners more precise: le/son (detPoss), le/un/son/cette (free)
%	| ?lexNP = mweNoDetLexNoun[IObjNode] *= [genstruct=lexN]
%	| ?lexNP = mweLexNounLexAdj[IObjNode,IObjAdjNode] *= [genstruct=lexNLexAdj]
%    };
%    ?LexGen = ?lexNP.xLexNPTop
%
%}



