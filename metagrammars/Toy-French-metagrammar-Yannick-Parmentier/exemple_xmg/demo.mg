% -------------------------------------------------------
% A FRENCH TOY METAGRAMMAR
% -------------------------------------------------------
% 
% 
% Demo MetaGrammar inspired by B. Crabbe and C. Gardent French TAG
% 
% 
% ASSOCIATED FILES (for TuLiPA): 
% 	demo.lex : lemma database
%	demo.mph : morphological database
%	
%
% Contact: yannick.parmentier@loria.fr
%		
% -------------------------------------------------------


%% Principles instanciation

use color with () dims (syn)
%%use unicity with (extracted = +) dims (syn)

%% Type declarations

type CAT={n,np,v,vn,s,pp,c,p,cl,par,qui}
type PERS=[1..3]
type GENDER={m,f}
type NUMBER={sg,pl}
type MODE={ind,subj}
type TOP=[
       mode : MODE,
       num : NUMBER,
       gen : GENDER,
       pers : PERS]
type BOT=[
       mode : MODE,
       num : NUMBER,
       gen : GENDER,
       pers : PERS]

type MARK={subst,foot,none,nadj,anchor,coanchor,flex}
type COLOR ={red,black,white}

type LABEL !
type IDX !
type NAME = {anc}

%% Property declarations

property color      : COLOR
property mark       : MARK
property extracted  : bool
property name       : NAME

%% Feature declarations

feature cat   : CAT
feature gen   : GENDER
feature num   : NUMBER
feature pers  : PERS
feature mode  : MODE
feature top   : TOP
feature bot   : BOT
feature pp-gen: IDX
feature passive : bool

feature id    : IDX
feature idx   : IDX
feature label : LABEL
feature label0: LABEL
feature rel   : IDX
feature arg0  : IDX
feature arg1  : IDX
feature arg2  : IDX
feature theta0: IDX
feature theta1: IDX
feature theta2: IDX

feature subjectI: IDX
feature subjectL: LABEL
feature objectI : IDX
feature objectL : LABEL
feature cagentI : IDX
feature cagentL : LABEL
feature vbI     : IDX
feature vbL     : LABEL

%% Classes for Verbal Morphology (verbal spine: active, passive)

class VerbalMorphology
export
   ?xS ?xVN ?xV
declare
   ?xS ?xVN ?xV ?I ?L
{
        <syn>{
                node ?xS(color=black)[cat = s, top = [mode=ind]]{
                        node ?xVN(color=black)[cat = v]{
                                node ?xV(name=anc,mark=anchor,color=black)[cat = v,top = [idx=I,label=L]]
                        }
                }
        }*=[vbI=I,vbL = L]
}

class activeVerbMorphology
import
        VerbalMorphology[]
declare
        ?fY ?fZ ?fW
{
        <syn>{
                node ?xVN[bot=[num = ?fY,gen = ?fZ,pers=?fW]]{
                        node ?xV[top=[num = ?fY,gen = ?fZ,pers=?fW]]
                }
        }
}

class passiveVerbMorphology
import
   VerbalMorphology[]
export
   ?xInfl
declare
   ?xInfl ?fX ?fY ?fZ
{
        <syn>{
                     node ?xVN[bot=[num = ?fX, gen = ?fY, pers = ?fZ]]{
                        node ?xInfl(color=black,mark=subst)[cat = v,top=[num = ?
fX, gen = ?fY, pers = ?fZ]]
                        node ?xV(color=black)[cat = v, pp-gen = ?fY]
                     }
        }*=[passive = +]
}

class dian0Vactive[L,E,X]
{
        Subject[X,L] 
	; activeVerbMorphology[]*=[vbI=E,vbL = L]
}

class dian0Vn1Active[L,E,X,Y]
{
        dian0Vactive[L,E,X] 
	; Object[Y,L]
}

class dian0Vn1Passive[L,E,X,Y]
{
        Subject[Y,L] 
	; CAgent[X,L] 
	; passiveVerbMorphology[]*=[vbI=E,vbL = L]
}

class n0V[L,E,X]
{
	unaryRel[]*=[label0=L,arg0=E,arg1=X];
        dian0Vactive[L,E,X]
}

class n0Vn1[L,E,X,Y]
{
	 binaryRel[]*=[label0=L,arg0=E,arg1=X,arg2=Y] ;
	{
         dian0Vn1Active[L,E,X,Y]
         | dian0Vn1Passive[L,E,X,Y]
	}
}

%% Classes for verbal arguments (subject, object)

class  VerbalArgument
export
        ?xS ?xV
declare
        ?xS ?xV
{
        <syn>{
                node ?xS(color=white)[cat = s]{
                        node ?xV(color=white)[cat = v]
                }
        }
}

class  SubjectAgreement
export
        ?xSubjAgr ?xVAgr
declare
        ?xSubjAgr ?xVAgr ?fX ?fY ?fZ
{
        <syn>{
                node ?xSubjAgr[top=[num = ?fX, gen = ?fY, pers = ?fZ]];
                node ?xVAgr[top=[num = ?fX, gen = ?fY, pers = ?fZ]]
        }
}

class  CanSubject
import
        VerbalArgument[]
        SubjectAgreement[]
export
        ?xSubj
declare
        ?xSubj 
{
        <syn>{
                node ?xS[cat = s]{
                        node ?xSubj(color=red)[cat = @{cl,n}]
                        node ?xV[cat = v]
                };
                ?xSubj = ?xSubjAgr;
                ?xV = ?xVAgr
        }
}

class  CanonicalSubject
import
        CanSubject[]
	SubjectSem[]
{
        <syn>{
             node ?xSubj(color=red,mark=subst)[cat = n]
        };
	?xSubj = ?xSem
}

class  CliticSubject
import
        CanSubject[]
	SubjectSem[]
{
        <syn>{
             node ?xSubj(color=red,mark=subst)[cat = cl]
        };
	?xSubj = ?xSem
}

class  RelativeSubject
import
        VerbalArgument[]
        SubjectAgreement[]
	SubjectSem[]	
declare
        ?xSubj ?xfoot ?xRel ?fU ?fY ?fZ ?xQui ?xIdx
{
        <syn>{
                node ?xRel(color=red)[cat = n,bot=[num = ?fY,gen = ?fZ,pers = ?fU, idx = ?xIdx]]{
                        node ?xfoot(color=red,mark=foot)[cat=n,top=[num = ?fY,gen = ?fZ,pers = ?fU, idx = ?xIdx]]
                        node ?xS(mark=nadj){
                                node ?xSubj(color=red,extracted = +)[cat=c]{
                                        node ?xQui(color=red,mark=flex)[cat=qui]
                                }
                                node ?xV
                        }
                }
        };
        ?xfoot = ?xSubjAgr;
        ?xV = ?xVAgr;
	?xfoot = ?xSem
}

class  CanonicalObject
import
        VerbalArgument[]
	ObjectSem[]
export
        ?xObj
declare
        ?xObj
{
        <syn>{
                node ?xS{
                        node ?xV
                        node ?xObj(mark=subst, color=red)[cat = n]
                }
        };
	?xObj = ?xSem
}

class  CanonicalCAgent
import
        VerbalArgument[]
	CAgentSem[]
export
        ?xtop ?xArg ?xX
declare
        ?xtop ?xArg ?xX ?xPrep
{
        <syn>{
                node ?xtop(color=red)[cat = pp]{
                        node ?xX (color=red)[cat = p] {
				node ?xPrep(mark=flex,color=red)[cat=par]
			}
                        node ?xArg(mark=subst,color=red)[cat = n]
                }
        } ;
	?xArg = ?xSem ;	
	<syn>{ ?xS -> ?xtop ; ?xV >> ?xtop }
}


class Subject[I,L]
{
	CanonicalSubject[]*=[subjectI = I, subjectL = L]
	| RelativeSubject[]*=[subjectI = I, subjectL = L]
	| CliticSubject[]*=[subjectI = I, subjectL = L]
}

class Object[I,L]
{
	CanonicalObject[]*=[objectI = I, objectL = L]
}

class CAgent[I,L]
{
	CanonicalCAgent[]*=[cagentI = I, cagentL = L]
}

%% Miscellaneous classes (nouns, determiners, etc)

class propername
import
	nSem[]
declare
        ?xN
{
        <syn>{
                node xN(name=anc,color=red,mark=anchor)[cat = n,bot=[pers = 3]]
        };
	?xN = ?xSem
}

class Copule
declare
        ?xV
{
        <syn>{
                node xV(color=red,mark=anchor)[cat = v]
        }
}

class Clitic
import
	nSem[]
declare
        ?xCl
{
        <syn>{
                node xCl(color=red,mark=anchor)[cat = cl]
        };
	?xCl = ?xSem
}

%% Classes for projecting semantic information

class  basicProperty
export ?E ?Rel
declare ?E ?L0 ?Rel 
      {
        <sem>{
                L0:Rel(E)
              }
              *=[label0=L0, rel=Rel,arg0=E]
          }

% unary relation with event variable
class  unaryRel
import basicProperty[]
declare ?X !L1 ?Theta1
      {
        <sem>{
                 L1:Theta1(E,X)
              }
              *=[arg1=X,theta1=Theta1]
          }

% binary relation with event variable
class  binaryRel
import unaryRel[]
declare ?X !L2 ?Theta2
      {
        <sem>{
                 L2:Theta2(E,X)
              }
              *=[arg2=X,theta2=Theta2]
          }

% Verb arguments
class  SubjectSem
export
        ?xSem
declare
        ?xSem ?X ?L
{
        <syn>{
                node xSem[top=[idx=X,label=L]]
        }*=[subjectI = X,subjectL = L]
}

class  ObjectSem
export
        ?xSem
declare
        ?xSem ?X ?L
{
        <syn>{
                node xSem[top=[idx=X,label=L]]
        }*=[objectI = X,objectL = L]
}

class  CAgentSem
export
        ?xSem
declare
        ?xSem ?X ?L
{
        <syn>{
                node xSem[top=[idx=X,label=L]]
        }*=[cagentI = X,cagentL = L]
}

%% Noun semantics
class  nSem
export
        ?xSem
declare
        ?xSem ?X ?L
{
        <syn>{
                node ?xSem[cat=@{cl,n},top=[idx=X, label=L]]
        };
        basicProperty[]*=[arg0=X]
}

%%%%%%% Valuations

value n0V
value n0Vn1

value Copule
value propername
value Clitic
