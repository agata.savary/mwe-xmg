%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while extending metagrammar examples
% Sarah Pollet May 2019


%%% TODO: replace all exemples by those NOT in lemmes-train.mg 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmaCouperV {
  <lemma> {
    entry <- "couper";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaMacher {
  <lemma> {
    entry <- "mâcher";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaSeMoquer {
  <lemma> {
    entry <- "moquer";
    cat <- v;
    fam <- n0ClVpn1
  }
}



class LemmaSeSerrer {
  <lemma> {
    entry <- "serrer";
    cat <- v;
    fam <- ClCopule
  }
}

class LemmaPrendren0Vn1an2 {
  <lemma> {
    entry <- "prendre";
    cat <- v;
    fam <- n0Vn1an2
  }
}


class LemmaRendreV {
  <lemma> {
    entry <- "rendre";
    cat <- v;
    fam <- n0Vn1
  }
}


class LemmaAvoirn0Vn1an2 {
  <lemma> {
    entry <- "avoir";
    cat <- v;
    fam <- n0Vn1an2
  }
}

class LemmaPlierV {
  <lemma> {
    entry <- "plier";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmeLancerV
{
  <lemma> {
    entry <- "lancer";
    cat   <- v;
    fam   <- n0Vn1
   }
}

class LemmaRevenirV
{
  <lemma> {
    entry <- "revenir";
    cat   <- v;
    fam   <- Copule	
   }
}

class LemmaRevenirn0V
{
  <lemma> {
    entry <- "revenir";
    cat   <- v;
    fam   <- n0V	
   }
}

class LemmaRevenirn0Vden1
{
  <lemma> {
    entry <- "revenir";
    cat   <- v;
    fam   <- n0Vden1	
   }
}


class LemmaNeigerV {
  <lemma> {
    entry <- "neiger";
    cat <- v;
    fam <- ilV
  }
}

class LemmaResterV {
  <lemma> {
    entry <- "rester";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaDonnerV {
  <lemma> {
    entry <- "donner";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaDevoirV {
  <lemma> {
    entry <- "devoir";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaLésinerV {
  <lemma> {
    entry <- "lésiner";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaBattreV {
  <lemma> {
    entry <- "battre";
    cat <- v;
    fam <- n0Vn1
  }
}

class LemmaRonflerV {
  <lemma> {
    entry <- "ronfler";
    cat <- v;
    fam <- n0V
  }
}

class LemmaRespirerV {
  <lemma> {
    entry <- "respirer";
    cat <- v;
    fam <- n0V
  }
}

class LemmaPleurerV {
  <lemma> {
    entry <- "pleurer";
    cat <- v;
    fam <- n0V
  }
}

class LemmaDormirV {
  <lemma> {
    entry <- "dormir";
    cat <- v;
    fam <- n0V
  }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmePont
{
  <lemma> {
    entry <- "pont";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMalle
{
  <lemma> {
    entry <- "malle";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeVache
{
  <lemma> {
    entry <- "vache";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBagage
{
  <lemma> {
    entry <- "bagage";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeJambe
{
  <lemma> {
    entry <- "jambe";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCou
{
  <lemma> {
    entry <- "cou";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMot
{
  <lemma> {
    entry <- "mot";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMouton
{
  <lemma> {
    entry <- "mouton";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeOeil
{
  <lemma> {
    entry <- "oeil";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeLynx
{
  <lemma> {
    entry <- "lynx";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeDé
{
  <lemma> {
    entry <- "dé";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMoyenN
{
  <lemma> {
    entry <- "moyen";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeLarme
{
  <lemma> {
    entry <- "larme";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMarbre
{
  <lemma> {
    entry <- "marbre";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCrocodile
{
  <lemma> {
    entry <- "crocodile";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeNeige
{
  <lemma> {
    entry <- "neige";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePareilleN
{
  <lemma> {
    entry <- "pareille";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCeinture
{
  <lemma> {
    entry <- "ceinture";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeBoeuf
{
  <lemma> {
    entry <- "boeuf";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMadeleine
{
  <lemma> {
    entry <- "madeleine";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeOeuf
{
  <lemma> {
    entry <- "oeuf";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeChien
{
  <lemma> {
    entry <- "chien";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeChandelle
{
  <lemma> {
    entry <- "chandelle";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCompas
{
  <lemma> {
    entry <- "compas";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeLac
{
  <lemma> {
    entry <- "lac";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeMonde
{
  <lemma> {
    entry <- "monde";
    cat   <- n;
    fam   <- noun
  }
}

class LemmePierre
{
  <lemma> {
    entry <- "pierre";
    cat   <- n;
    fam   <- propername
  }
}

class LemmeLangue
{
  <lemma> {
    entry <- "langue";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeGorge
{
  <lemma> {
    entry <- "gorge";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeCorde
{
  <lemma> {
    entry <- "corde";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeFoi
{
  <lemma> {
    entry <- "foi";
    cat   <- n;
    fam   <- noun
  }
}

class LemmeChat
{
  <lemma> {
    entry <- "chat";
    cat   <- n;
    fam   <- noun
  }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeContrairementSAnte
{
  <lemma> {
    entry <- "contrairement";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeContrairementSPost
{
  <lemma> {
    entry <- "contrairement";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeContrairementV
{
  <lemma> {
    entry <- "contrairement";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeAilleursSAnte
{
  <lemma> {
    entry <- "ailleurs";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeAilleursSPost
{
  <lemma> {
    entry <- "ailleurs";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeAilleursV
{
  <lemma> {
    entry <- "ailleurs";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeContrairementSAnte
{
  <lemma> {
    entry <- "contrairement";
    cat   <- adv;
    fam   <- advSAnte
  }
}

class LemmeContrairementSPost
{
  <lemma> {
    entry <- "contrairement";
    cat   <- adv;
    fam   <- advSPost
  }
}

class LemmeContrairementV
{
  <lemma> {
    entry <- "contrairement";
    cat   <- adv;
    fam   <- advVPost
  }
}

class LemmeCommeSPost
{
  <lemma> {
    entry <- "comme";
    cat   <- adv;
    fam   <- advSPost
  }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class LemmePetitPred
{
  <lemma> {
    entry <- "petit";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmePetitAttr
{
  <lemma> {
    entry <- "petit";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}

class LemmeBeauPred
{
  <lemma> {
    entry <- "beau";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeBeauAttr
{
  <lemma> {
    entry <- "beau";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}

class LemmeFierAttr
{
  <lemma> {
    entry <- "fier";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}

class LemmeFortPred
{
  <lemma> {
    entry <- "fort";
    cat   <- adj;
    fam   <- n0vA
  }
}

class LemmeFortAttr
{
  <lemma> {
    entry <- "fort";
    cat   <- adj;
    fam   <- mweAdjAttrLeft
  }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeAu
{
  <lemma> {
    entry <- "au";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeSur
{
  <lemma> {
    entry <- "sur";
    cat   <- p;
    fam   <- s0Pn1
  }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeEn
{
  <lemma> {
    entry <- "en";
    cat   <- cl;
    fam   <- CliticT
  }
}

class LemmeCa
{
  <lemma> {
    entry <- "ça";
    cat   <- cl;
    fam   <- CliticT
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


class mweLemmeIlYADesAnnées
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mweIlLocClVn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    %coanchor clitic -> "y"/cl;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "années"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl
  }
}

class mweLemmeSeFaireBelle
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mweClCopule;
    coanchor bjAdjNode -> "belle"/adj;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}

class mweLemmeCaMeFaitUneBelleJambe
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0ClVpn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexNLexAdj;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjAdjNode -> "belle"/adj;
    coanchor ObjNode -> "jambe"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}



class mweLemmeDevoirUneFiereChandelle
{
  <lemma> {
    entry <- "devoir";
    cat   <- v;
    fam   <- mwen0vA;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexNLexAdj;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjAdjNode -> "fière"/adj;
    coanchor ObjNode -> "chandelle"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}



class mweLemmeSeMoquerDuMonde
{
  <lemma> {
    entry <- "moquer";
    cat   <- v;
    fam   <- mwen0ClVpn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobj = free;
    coanchor ObjDetNode -> "du"/d;
    coanchor ObjNode -> "monde"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}

class mweLemmePleuvoirDesCordes
{
  <lemma> {
    entry <- "pleuvoir";
    cat   <- v;
    fam   <- mweilV;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "cordes"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl
  }
}

class mweLemmeLesChiensNeFontPasDesChats
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1negPas;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "chats"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}



class mweLemmePrendreSesJambesASonCou
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "ses"/d;
    coanchor ObjNode -> "jambes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "son"/d;
    coanchor IObjNode -> "cou"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeSerrerLaCeinture
{
  <lemma> {
    entry <- "serrer";
    cat   <- v;
    fam   <- mweClCopule;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "ceinture"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}

class mweLemmeAvoirUnChatDansLaGorge
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "chat"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "dans"/p;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "gorge"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirLeCompasDansLeOeil
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "compas"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "dans"/p;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "oeil"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmePlierBagage
{
  <lemma> {
    entry <- "plier";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "bagage"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


class mweLemmeRendreLaPareille
{
  <lemma> {
    entry <- "rendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "pareille"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}


class mweLesDesSontLances
{
  <lemma> {
    entry <- "lancer";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = passive;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "dés"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeDonnerSaLangueAuChat
{
  <lemma> {
    entry <- "donner";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "sa"/d;
    coanchor ObjNode -> "langue"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "au"/d;
    coanchor IObjNode -> "chat"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeBattreLesOeufsEnNeige
{
  <lemma> {
    entry <- "battre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "oeufs"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor Prep -> "en"/p;
    coanchor IObjNode -> "neige"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeResterDeMarbre
{
  <lemma> {
    entry <- "rester";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "de"/d;
    coanchor IObjNode -> "marbre"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirDesYeuxDeLynx
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "yeux"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "de"/d;
    coanchor IObjNode -> "lynx"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirFoiEndet  %avoir foi en soi/lui/elle
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1des2;
    filter dia = active;
    coanchor ObjNode -> "foi"/n;
    filter subj = free;
    filter obj = lexicalized;
    filter objtype = canonical;
    filter objstruct = lexN;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter sentobject=free
  }
}

class mweLemmePleurerCommeUneMadeleine
{
  <lemma> {
    entry <- "pleurer";
    cat   <- v;
    fam   <- mwen0Vpn1;
    filter subj = free;
    filter obj = free;
    filter obl = lexicalized;
    filter obltype = canonical;
    filter oblstruct = lexN;
    filter adv2 = comme;
    coanchor Adv -> "comme"/adv;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "une"/d;
    coanchor IObjNode -> "madeleine"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeRevenirASesMoutons
{
  <lemma> {
    entry <- "revenir";
    cat   <- v;
    fam   <- mwen0Vpn1re;
    filter subj = free;
    filter iobj = lexicalized;
    filter iobjstruct = freeDetLexN;
    coanchor IObjDetNode -> "ses"/d;
    filter objstruct = lexDetLexN;
    coanchor IObjNode -> "moutons"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=pl;
    equation IObjNode -> modifiable=-
  }
}


