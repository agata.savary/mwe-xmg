class Jean
{
  <morpho> {
    morph <- "Jean";
    lemma <- "jean";
    cat   <- n;
    pers <- 3; 
    gen <- m; 
    num <- sg
   }
}

class viteAdv
{
  <morpho> {
	morph <-"vite";
	lemma <-"vite";
	cat <- adv
	}
}


class hibiscusN
{
  <morpho> {
	morph <-"hibiscus";
	lemma <-"hibiscus";
	gen <- m;
	det <- -;
	cat <- n
	}
}


class hileN
{
  <morpho> {
	morph <-"hile";
	lemma <-"hile";
	num <- sg;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class hilesN
{
  <morpho> {
	morph <-"hiles";
	lemma <-"hile";
	num <- pl;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class hiloireN
{
  <morpho> {
	morph <-"hiloire";
	lemma <-"hiloire";
	num <- sg;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class hiloiresN
{
  <morpho> {
	morph <-"hiloires";
	lemma <-"hiloire";
	num <- pl;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class souvenirN
{
  <morpho> {
	morph <-"souvenir";
	lemma <-"souvenir";
	num <- sg;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class souvenirV
{
  <morpho> {
	morph <-"souvenir";
	lemma <-"souvenir";
	mode <- inf;
	cat <- v
	}
}


class pleurerV
{
  <morpho> {
	morph <-"pleurer";
	lemma <-"pleurer";
	mode <- inf;
	cat <- v
	}
}


class grelotterV
{
  <morpho> {
	morph <-"grelotter";
	lemma <-"grelotter";
	mode <- inf;
	cat <- v
	}
}


class fuirV
{
  <morpho> {
	morph <-"fuir";
	lemma <-"fuir";
	mode <- inf;
	cat <- v
	}
}


class pleuvoirV
{
  <morpho> {
	morph <-"pleuvoir";
	lemma <-"pleuvoir";
	mode <- inf;
	cat <- v
	}
}


class semblerV
{
  <morpho> {
	morph <-"sembler";
	lemma <-"sembler";
	mode <- inf;
	cat <- v
	}
}


class dormirV
{
  <morpho> {
	morph <-"dormir";
	lemma <-"dormir";
	mode <- inf;
	cat <- v
	}
}


class hormisPrep
{
  <morpho> {
	morph <-"hormis";
	lemma <-"hormis";
	cat <- prep
	}
}


class jointementAdv
{
  <morpho> {
	morph <-"jointement";
	lemma <-"jointement";
	cat <- adv
	}
}


class hierAdv
{
  <morpho> {
	morph <-"hier";
	lemma <-"hier";
	cat <- adv
	}
}


class itouAdv
{
  <morpho> {
	morph <-"itou";
	lemma <-"itou";
	cat <- adv
	}
}


class isolémentAdv
{
  <morpho> {
	morph <-"isolément";
	lemma <-"isolément";
	cat <- adv
	}
}


class isolésAdj
{
  <morpho> {
	morph <-"isolés";
	lemma <-"isoler";
	num <- pl;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arborescentAdj
{
  <morpho> {
	morph <-"arborescent";
	lemma <-"arborescent";
	num <- sg;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arborescenteAdj
{
  <morpho> {
	morph <-"arborescente";
	lemma <-"arborescent";
	num <- sg;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arborescentesAdj
{
  <morpho> {
	morph <-"arborescentes";
	lemma <-"arborescent";
	num <- pl;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arborescentsAdj
{
  <morpho> {
	morph <-"arborescents";
	lemma <-"arborescent";
	num <- pl;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arboretumN
{
  <morpho> {
	morph <-"arboretum";
	lemma <-"arboretum";
	num <- sg;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arboretumsN
{
  <morpho> {
	morph <-"arboretums";
	lemma <-"arboretum";
	num <- pl;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arborezV
{
  <morpho> {
	morph <-"arborez";
	lemma <-"arborer";
	pers <- pl;
	num <- 2;
	mode <- imp;
	tense <- pres;
	cat <- v
	}
}


class arboricoleAdj
{
  <morpho> {
	morph <-"arboricole";
	lemma <-"arboricole";
	num <- sg;
	det <- -;
	cat <- n
	}
}


class arboricoleN
{
  <morpho> {
	morph <-"arboricole";
	lemma <-"arboricole";
	num <- sg;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arboricolesAdj
{
  <morpho> {
	morph <-"arboricoles";
	lemma <-"arboricole";
	num <- pl;
	det <- -;
	cat <- n
	}
}


class arboricolesN
{
  <morpho> {
	morph <-"arboricoles";
	lemma <-"arboricole";
	num <- pl;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arboriculteurN
{
  <morpho> {
	morph <-"arboriculteur";
	lemma <-"arboriculteur";
	num <- sg;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arboriculteursN
{
  <morpho> {
	morph <-"arboriculteurs";
	lemma <-"arboriculteur";
	num <- pl;
	gen <- m;
	det <- -;
	cat <- n
	}
}


class arboricultriceN
{
  <morpho> {
	morph <-"arboricultrice";
	lemma <-"arboriculteur";
	num <- sg;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arboricultricesN
{
  <morpho> {
	morph <-"arboricultrices";
	lemma <-"arboriculteur";
	num <- pl;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arboricultureN
{
  <morpho> {
	morph <-"arboriculture";
	lemma <-"arboriculture";
	num <- sg;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arboriculturesN
{
  <morpho> {
	morph <-"arboricultures";
	lemma <-"arboriculture";
	num <- pl;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arboriezV
{
  <morpho> {
	morph <-"arboriez";
	lemma <-"arborer";
	pers <- pl;
	num <- 2;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class arborionsV
{
  <morpho> {
	morph <-"arborions";
	lemma <-"arborer";
	pers <- pl;
	num <- 1;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class arborisaV
{
  <morpho> {
	morph <-"arborisa";
	lemma <-"arboriser";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- spast;
	cat <- v
	}
}


class arborisaiV
{
  <morpho> {
	morph <-"arborisai";
	lemma <-"arboriser";
	pers <- sg;
	num <- 1;
	mode <- ind;
	tense <- spast;
	cat <- v
	}
}


class arborisaientV
{
  <morpho> {
	morph <-"arborisaient";
	lemma <-"arboriser";
	pers <- pl;
	num <- 3;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class arborisaisV
{
  <morpho> {
	morph <-"arborisais";
	lemma <-"arboriser";
	pers <- sg;
	num <- 1;
	num <- 2;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class arborisaitV
{
  <morpho> {
	morph <-"arborisait";
	lemma <-"arboriser";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class arborisantV
{
  <morpho> {
	morph <-"arborisant";
	lemma <-"arboriser";
	mode <- ppart;
	tense <- pres;
	cat <- v
	}
}


class arborisasV
{
  <morpho> {
	morph <-"arborisas";
	lemma <-"arboriser";
	pers <- sg;
	num <- 2;
	mode <- ind;
	tense <- spast;
	cat <- v
	}
}


class arborisasseV
{
  <morpho> {
	morph <-"arborisasse";
	lemma <-"arboriser";
	pers <- sg;
	num <- 1;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class arborisassentV
{
  <morpho> {
	morph <-"arborisassent";
	lemma <-"arboriser";
	pers <- pl;
	num <- 3;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class arborisassesV
{
  <morpho> {
	morph <-"arborisasses";
	lemma <-"arboriser";
	pers <- sg;
	num <- 2;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class arborisassiezV
{
  <morpho> {
	morph <-"arborisassiez";
	lemma <-"arboriser";
	pers <- pl;
	num <- 2;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class arborisassionsV
{
  <morpho> {
	morph <-"arborisassions";
	lemma <-"arboriser";
	pers <- pl;
	num <- 1;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class arborisationN
{
  <morpho> {
	morph <-"arborisation";
	lemma <-"arborisation";
	num <- sg;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arborisationsN
{
  <morpho> {
	morph <-"arborisations";
	lemma <-"arborisation";
	num <- pl;
	gen <- f;
	det <- -;
	cat <- n
	}
}


class arboriseV
{
  <morpho> {
	morph <-"arborise";
	lemma <-"arboriser";
	pers <- sg;
	num <- 2;
	mode <- imp;
	tense <- pres;
	cat <- v
	}
}


class arborisentV
{
  <morpho> {
	morph <-"arborisent";
	lemma <-"arboriser";
	pers <- pl;
	num <- 3;
	mode <- ind;
	tense <- pres;
	mode <- subj;
	tense <- pres;
	cat <- v
	}
}


class arboriserV
{
  <morpho> {
	morph <-"arboriser";
	lemma <-"arboriser";
	mode <- inf;
	cat <- v
	}
}


class arboriseraV
{
  <morpho> {
	morph <-"arborisera";
	lemma <-"arboriser";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class arboriseraiV
{
  <morpho> {
	morph <-"arboriserai";
	lemma <-"arboriser";
	pers <- sg;
	num <- 1;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class arboriseraientV
{
  <morpho> {
	morph <-"arboriseraient";
	lemma <-"arboriser";
	pers <- pl;
	num <- 3;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class arboriseraisV
{
  <morpho> {
	morph <-"arboriserais";
	lemma <-"arboriser";
	pers <- sg;
	num <- 1;
	num <- 2;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class prenaientV
{
  <morpho> {
	morph <-"prenaient";
	lemma <-"prendre";
	pers <- pl;
	num <- 3;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class prenaisV
{
  <morpho> {
	morph <-"prenais";
	lemma <-"prendre";
	pers <- sg;
	num <- 1;
	num <- 2;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class prenaitV
{
  <morpho> {
	morph <-"prenait";
	lemma <-"prendre";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class prenantV
{
  <morpho> {
	morph <-"prenant";
	lemma <-"prendre";
	mode <- ppart;
	tense <- pres;
	cat <- v
	}
}


class prendV
{
  <morpho> {
	morph <-"prend";
	lemma <-"prendre";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- pres;
	cat <- v
	}
}


class prendraV
{
  <morpho> {
	morph <-"prendra";
	lemma <-"prendre";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class prendraiV
{
  <morpho> {
	morph <-"prendrai";
	lemma <-"prendre";
	pers <- sg;
	num <- 1;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class prendraientV
{
  <morpho> {
	morph <-"prendraient";
	lemma <-"prendre";
	pers <- pl;
	num <- 3;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class prendraisV
{
  <morpho> {
	morph <-"prendrais";
	lemma <-"prendre";
	pers <- sg;
	num <- 1;
	num <- 2;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class prendraitV
{
  <morpho> {
	morph <-"prendrait";
	lemma <-"prendre";
	pers <- sg;
	num <- 3;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class prendrasV
{
  <morpho> {
	morph <-"prendras";
	lemma <-"prendre";
	pers <- sg;
	num <- 2;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class prendreV
{
  <morpho> {
	morph <-"prendre";
	lemma <-"prendre";
	mode <- inf;
	cat <- v
	}
}


class prendrezV
{
  <morpho> {
	morph <-"prendrez";
	lemma <-"prendre";
	pers <- pl;
	num <- 2;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class prendriezV
{
  <morpho> {
	morph <-"prendriez";
	lemma <-"prendre";
	pers <- pl;
	num <- 2;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class prendrionsV
{
  <morpho> {
	morph <-"prendrions";
	lemma <-"prendre";
	pers <- pl;
	num <- 1;
	mode <- cond;
	tense <- pres;
	cat <- v
	}
}


class prendronsV
{
  <morpho> {
	morph <-"prendrons";
	lemma <-"prendre";
	pers <- pl;
	num <- 1;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class prendrontV
{
  <morpho> {
	morph <-"prendront";
	lemma <-"prendre";
	pers <- pl;
	num <- 3;
	mode <- ind;
	tense <- future;
	cat <- v
	}
}


class prendsV
{
  <morpho> {
	morph <-"prends";
	lemma <-"prendre";
	pers <- sg;
	num <- 2;
	mode <- imp;
	tense <- pres;
	cat <- v
	}
}


class prenezV
{
  <morpho> {
	morph <-"prenez";
	lemma <-"prendre";
	pers <- pl;
	num <- 2;
	mode <- imp;
	tense <- pres;
	cat <- v
	}
}


class preniezV
{
  <morpho> {
	morph <-"preniez";
	lemma <-"prendre";
	pers <- pl;
	num <- 2;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class prenionsV
{
  <morpho> {
	morph <-"prenions";
	lemma <-"prendre";
	pers <- pl;
	num <- 1;
	mode <- ind;
	tense <- imp;
	cat <- v
	}
}


class prenneV
{
  <morpho> {
	morph <-"prenne";
	lemma <-"prendre";
	pers <- sg;
	num <- 1;
	num <- 3;
	mode <- subj;
	tense <- pres;
	cat <- v
	}
}


class prennentV
{
  <morpho> {
	morph <-"prennent";
	lemma <-"prendre";
	pers <- pl;
	num <- 3;
	mode <- ind;
	tense <- pres;
	mode <- subj;
	tense <- pres;
	cat <- v
	}
}


class prennesV
{
  <morpho> {
	morph <-"prennes";
	lemma <-"prendre";
	pers <- sg;
	num <- 2;
	mode <- subj;
	tense <- pres;
	cat <- v
	}
}


class prenonsV
{
  <morpho> {
	morph <-"prenons";
	lemma <-"prendre";
	pers <- pl;
	num <- 1;
	mode <- imp;
	tense <- pres;
	cat <- v
	}
}


class prirentV
{
  <morpho> {
	morph <-"prirent";
	lemma <-"prendre";
	pers <- pl;
	num <- 3;
	mode <- ind;
	tense <- spast;
	cat <- v
	}
}


class prisV
{
  <morpho> {
	morph <-"pris";
	lemma <-"prendre";
	mode <- ppart;
	tense <- past;
	cat <- v
	}
}


class priseV
{
  <morpho> {
	morph <-"prise";
	lemma <-"prendre";
	pers <- sg;
	mode <- ppart;
	tense <- past;
	cat <- v
	}
}


class prisesV
{
  <morpho> {
	morph <-"prises";
	lemma <-"prendre";
	pers <- pl;
	mode <- ppart;
	tense <- past;
	cat <- v
	}
}


class prisseV
{
  <morpho> {
	morph <-"prisse";
	lemma <-"prendre";
	pers <- sg;
	num <- 1;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class prissentV
{
  <morpho> {
	morph <-"prissent";
	lemma <-"prendre";
	pers <- pl;
	num <- 3;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class prissesV
{
  <morpho> {
	morph <-"prisses";
	lemma <-"prendre";
	pers <- sg;
	num <- 2;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class prissiezV
{
  <morpho> {
	morph <-"prissiez";
	lemma <-"prendre";
	pers <- pl;
	num <- 2;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class prissionsV
{
  <morpho> {
	morph <-"prissions";
	lemma <-"prendre";
	pers <- pl;
	num <- 1;
	mode <- subj;
	tense <- imp;
	cat <- v
	}
}


class pritV
{
  <morpho> {
	morph <-"prit";
	lemma <-"prendre";
	pers <- sg;
	num <- 3;
	mode <- ind;
	tense <- spast;
	cat <- v
	}
}


