%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicon created manually while extending metagrammar examples
% Sarah Pollet May 2019

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class couper
{
  <morpho> {
    morph <- "couper";
    lemma <- "couper";
    cat   <- v;
    mode <- inf
   }
}

class macher
{
  <morpho> {
    morph <- "mâcher";
    lemma <- "mâcher";
    cat   <- v;
    mode <- inf
   }
}

class Avoirn0Vn1an2
{
  <morpho> {
    morph <- "avoir";
    lemma <- "avoir";
    cat   <- v;
    mode <- inf
   }
}

class devoirV
{
  <morpho> {
    morph <- "devoir";
    lemma <- "devoir";
    cat   <- v;
    mode <- inf
   }
}


class doit
{
  <morpho> {
    morph <- "doit";
    lemma <- "devoir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class rendre
{
  <morpho> {
    morph <- "rendre";
    lemma <- "rendre";
    cat   <- v;
    mode <- inf
   }
}

class revenir
{
  <morpho> {
    morph <- "revenir";
    lemma <- "revenir";
    cat   <- v;
    mode <- inf
   }
}


class revient
{
  <morpho> {
    morph <- "revient";
    lemma <- "revenir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class plier
{
  <morpho> {
    morph <- "plier";
    lemma <- "plier";
    cat   <- v;
    mode <- inf
   }
}

class serrer
{
  <morpho> {
    morph <- "serrer";
    lemma <- "serrer";
    cat   <- v;
    mode <- inf
   }
}


class sert
{
  <morpho> {
    morph <- "sert";
    lemma <- "serrer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}


class plie
{
  <morpho> {
    morph <- "plie";
    lemma <- "plier";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class neigeV
{
  <morpho> {
    morph <- "neige";
    lemma <- "neiger";
    cat   <- v;
    mode <- ind; 
    pers <- 3; 
    num <- sg
   }
}

class coupe
{
  <morpho> {
    morph <- "coupe";
    lemma <- "couper";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class ronfler
{
  <morpho> {
    morph <- "ronfler";
    lemma <- "ronfler";
    cat   <- v;
    mode <- inf
   }
}


class ronfle
{
  <morpho> {
    morph <- "ronfle";
    lemma <- "ronfler";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class respirer
{
  <morpho> {
    morph <- "respirer";
    lemma <- "respirer";
    cat   <- v;
    mode <- inf
   }
}


class respire
{
  <morpho> {
    morph <- "respire";
    lemma <- "respirer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class pleurer
{
  <morpho> {
    morph <- "pleurer";
    lemma <- "pleurer";
    cat   <- v;
    mode <- inf
   }
}


class pleure
{
  <morpho> {
    morph <- "pleure";
    lemma <- "pleurer";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class rester
{
  <morpho> {
    morph <- "rester";
    lemma <- "rester";
    cat   <- v;
    mode <- inf
   }
}


class reste
{
  <morpho> {
    morph <- "reste";
    lemma <- "rester";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}


class donner
{
  <morpho> {
    morph <- "donner";
    lemma <- "donner";
    cat   <- v;
    mode <- inf
   }
}

class moquer
{
  <morpho> {
    morph <- "moquer";
    lemma <- "moquer";
    cat   <- v;
    mode <- inf
   }
}

class donne
{
  <morpho> {
    morph <- "donne";
    lemma <- "donner";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class battre
{
  <morpho> {
    morph <- "battre";
    lemma <- "battre";
    cat   <- v;
    mode <- inf
   }
}

class lésiner
{
  <morpho> {
    morph <- "lésiner";
    lemma <- "lésiner";
    cat   <- v;
    mode <- inf
   }
}

class bat
{
  <morpho> {
    morph <- "bat";
    lemma <- "battre";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class dormir
{
  <morpho> {
    morph <- "dormir";
    lemma <- "dormir";
    cat   <- v;
    mode <- inf
   }
}


class dort
{
  <morpho> {
    morph <- "dort";
    lemma <- "dormir";
    cat   <- v;
    mode <- ind;
    pers <- 3;
    num <- sg
   }
}

class lancésImp
{
  <morpho> {
    morph <- "lancés";
    lemma <- "lancer";
    cat   <- v;
    mode <- ppart; 
    pp-num <- pl;
    pp-gen <- m
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOUNS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class pont
{
  <morpho> {
    morph <- "pont";
    lemma <- "pont";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class marbre
{
  <morpho> {
    morph <- "marbre";
    lemma <- "marbre";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class compas
{
  <morpho> {
    morph <- "compas";
    lemma <- "compas";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class neige
{
  <morpho> {
    morph <- "neige";
    lemma <- "neige";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class mot
{
  <morpho> {
    morph <- "mot";
    lemma <- "mot";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class malle
{
  <morpho> {
    morph <- "malle";
    lemma <- "malle";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class mots
{
  <morpho> {
    morph <- "mots";
    lemma <- "mot";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class gorge
{
  <morpho> {
    morph <- "gorge";
    lemma <- "gorge";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class oeuf
{
  <morpho> {
    morph <- "oeuf";
    lemma <- "oeuf";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class oeufs
{
  <morpho> {
    morph <- "oeufs";
    lemma <- "oeuf";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class bagage
{
  <morpho> {
    morph <- "bagage";
    lemma <- "bagage";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class lac
{
  <morpho> {
    morph <- "lac";
    lemma <- "lac";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class moyen
{
  <morpho> {
    morph <- "moyen";
    lemma <- "moyen";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class moyens
{
  <morpho> {
    morph <- "moyens";
    lemma <- "moyen";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class foi
{
  <morpho> {
    morph <- "foi";
    lemma <- "foi";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}


class larme
{
  <morpho> {
    morph <- "larme";
    lemma <- "larme";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class pareille
{
  <morpho> {
    morph <- "pareille";
    lemma <- "pareille";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class larmes
{
  <morpho> {
    morph <- "larmes";
    lemma <- "larme";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class mouton
{
  <morpho> {
    morph <- "mouton";
    lemma <- "mouton";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class moutons
{
  <morpho> {
    morph <- "moutons";
    lemma <- "mouton";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class oeil
{
  <morpho> {
    morph <- "oeil";
    lemma <- "oeil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class yeux
{
  <morpho> {
    morph <- "yeux";
    lemma <- "oeil";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class lynx
{
  <morpho> {
    morph <- "lynx";
    lemma <- "lynx";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}


class langue
{
  <morpho> {
    morph <- "langue";
    lemma <- "langue";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class madeleine
{
  <morpho> {
    morph <- "madeleine";
    lemma <- "madeleine";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class dé
{
  <morpho> {
    morph <- "dé";
    lemma <- "dé";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class boeuf
{
  <morpho> {
    morph <- "boeuf";
    lemma <- "boeuf";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class dés
{
  <morpho> {
    morph <- "dés";
    lemma <- "dé";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class crocodile
{
  <morpho> {
    morph <- "crocodile";
    lemma <- "crocodile";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class cou
{
  <morpho> {
    morph <- "cou";
    lemma <- "cou";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class chat
{
  <morpho> {
    morph <- "chat";
    lemma <- "chat";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class chats
{
  <morpho> {
    morph <- "chats";
    lemma <- "chat";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class jambe
{
  <morpho> {
    morph <- "jambe";
    lemma <- "jambe";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class chandelle
{
  <morpho> {
    morph <- "chandelle";
    lemma <- "chandelle";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class jambes
{
  <morpho> {
    morph <- "jambes";
    lemma <- "jambe";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class corde
{
  <morpho> {
    morph <- "corde";
    lemma <- "corde";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- sg
   }
}

class cordes
{
  <morpho> {
    morph <- "cordes";
    lemma <- "corde";
    cat   <- n;
    det <- -;
    gen <- f;
    num <- pl
   }
}

class monde
{
  <morpho> {
    morph <- "monde";
    lemma <- "monde";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}


class chien
{
  <morpho> {
    morph <- "chien";
    lemma <- "chien";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- sg
   }
}

class chiens
{
  <morpho> {
    morph <- "chiens";
    lemma <- "chien";
    cat   <- n;
    det <- -;
    gen <- m;
    num <- pl
   }
}

class Pierre
{
  <morpho> {
    morph <- "Pierre";
    lemma <- "pierre";
    cat   <- n;
    pers <- 3; 
    gen <- m; 
    num <- sg
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVERBS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class contrairement
{
  <morpho> {
    morph <- "contrairement";
    lemma <- "contrairement";
    cat   <- adv
   }
}

class ailleurs
{
  <morpho> {
    morph <- "ailleurs";
    lemma <- "ailleurs";
    cat   <- adv
   }
}

class comme
{
  <morpho> {
    morph <- "comme";
    lemma <- "comme";
    cat   <- adv
   }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADJECTIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class fier
{
  <morpho> {
    morph <- "fier";
    lemma <- "fier";
    cat   <- adj;
    gen <- m
   }
}

class fiere
{
  <morpho> {
    morph <- "fière";
    lemma <- "fier";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class petit
{
  <morpho> {
    morph <- "petit";
    lemma <- "petit";
    cat   <- adj;
    gen <- m
   }
}

class petite
{
  <morpho> {
    morph <- "petite";
    lemma <- "petit";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class beau
{
  <morpho> {
    morph <- "beau";
    lemma <- "beau";
    cat   <- adj;
    gen <- m
   }
}

class belle
{
  <morpho> {
    morph <- "belle";
    lemma <- "beau";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class petits
{
  <morpho> {
    morph <- "petits";
    lemma <- "petit";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class petites
{
  <morpho> {
    morph <- "petites";
    lemma <- "petit";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}

class fort
{
  <morpho> {
    morph <- "fort";
    lemma <- "fort";
    cat   <- adj;
    gen <- m
   }
}

class forte
{
  <morpho> {
    morph <- "forte";
    lemma <- "fort";
    cat   <- adj;
    gen <- f;
    num <- sg
   }
}

class forts
{
  <morpho> {
    morph <- "forts";
    lemma <- "fort";
    cat   <- adj;
    gen <- m;
    num <- pl
   }
}

class fortes
{
  <morpho> {
    morph <- "fortes";
    lemma <- "fort";
    cat   <- adj;
    gen <- f;
    num <- pl
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class de
{
  <morpho> {
    morph <- "de";
    lemma <- "un";
    cat   <- d;
    num <- sg
   }
}

class du
{
  <morpho> {
    morph <- "du";
    lemma <- "un";
    cat   <- d;
    num <- sg
   }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPOSITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class au
{
  <morpho> {
    morph <- "au";
    lemma <- "au";
    cat   <- p
   }
}

class sur
{
  <morpho> {
    morph <- "sur";
    lemma <- "sur";
    cat   <- p
   }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLITICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class en
{
  <morpho> {
    morph <- "en";
    lemma <- "en";
    cat   <- cl;
    func <- loc;
    refl <- -
   }
}

class ca
{
  <morpho> {
    morph <- "ça";
    lemma <- "ça";
    cat   <- cl;
    func <- suj;
    refl <- -;
    pers <- 3;
    gen <- m;
    num <- sg
   }
}



