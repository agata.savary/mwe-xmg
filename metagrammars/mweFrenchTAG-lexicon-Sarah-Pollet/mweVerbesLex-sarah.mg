%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vebal phrases with free or lexicalized arguments
% These classed are variants of Benoit's verbes.mg classed, but having some non lexicalized and some lexicalized arguments
% 06/05/2019
% by Sarah Pollet
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vpn1: il pleure comme une madeleine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%class mwen0Vpn1{ 
	%Subject[];
	%mwen0Vpn1[];
	%Object[]
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1negPas: ne pas macher ses mots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1negPas{ 
	mwen0Vn1[];
	mwenegPas[];
	mweObject[]
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mweilV: il pleut des cordes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweilV{
	ilV[];
	Object[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0ClVpn1: se moquer du monde
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0ClVpn1{
	n0ClVpn1[];
	mweObject[]
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwn0vA: devoir une fiere chandelle/ se devoir
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0vA{
	mwen0Vn1[]
	|n0ClVpn1[];
	mweAdjAttrLeft[];
	Object[]
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vn1negPas: les chiens ne font pas des chats 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1negPas{ 
	mwen0Vn1[];
	mwenegPas[];
	mweObject[]
}



class mwenegPas
{
     negPasFinitePost[]
  %| negPasInfAnte[]
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mwen0Vpn1: revenir à ses moutons 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vpn1re{ 
	mwen0Vpn1[];
	mweIObject[]
}




class mwen0Vpn1
declare 
	?X
{
  {
	mwen0Vpn1Subject[];mwen0Vpn1Oblique[]*=[prep = ?X];activeVerbMorphology[]
  }*=[prep1 = ?X]
}




class mwen0Vpn1Oblique[]
{
	CanonicalOblique[]
	%|whOblique[]
	%|RelativeOblique[]
	%|CleftObliqueOne[]
	%|CleftObliqueTwo[]
}




class mwen0Vpn1Subject
{
	CanonicalSubject[]
	%|CliticSubject[]
	%|whSubject[]
	%|RelativeSubject[]
	%|CleftSubject[]
	|InfinitiveSubject[]
 	%|ImperativeSubject[]
 	%|InvertedNominalSubject[]
	%|InterrogInvSubject[]
}




class mweCanonicalIObject
import 
	mweCanonicalPPnonOblique[]

{
	<syn>{
		node xPrep[cat = à]; 
		node xArg[top = [func = iobj]]	
	}
}







%class mwen0Vpn1CanonicalIObject
%import 
	%mweCanonicalPPnonOblique[]
	%mweObjectLex[]
%declare
	%?lexNP xPrep xArg

%{
	%<syn>{
		%node xPrep[cat = à];
		%?lexNP = mweLexDetLexNoun[ObjDetNode,ObjNode] *= [objstruct=lexDetLexN];
		%node xArg[top = [func = iobj]]	
 %};
    %?LexObl = ?lexNP.xLexNPTop
%}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%class mwen0Vpn1
%declare 
	%?X
%{
%  {
	%Subject[];mweOblique[]*=[adv = ?X];activeVerbMorphology[]
 % }*=[adv1 = ?X]
%}



%class mweOblique[]
%{
	%CanonicalOblique[]
	%|whOblique[]
	%|RelativeOblique[]
	%|CleftObliqueOne[]
	%|CleftObliqueTwo[]
%}
