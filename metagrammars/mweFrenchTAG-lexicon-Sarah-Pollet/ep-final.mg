

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Prep
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class LemmeDe
{
  <lemma> {
    entry <- "de";
    cat   <- p;
    fam   <- s0Pn1
  }
}

class LemmeDePrep
{
  <lemma> {
    entry <- "de";
    cat   <- p;
    fam   <- PrepositionalPhrase
  }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%EP provenant du Lexique-Grammaire de Gross
%Sarah (Juillet 2019)
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class mweAvalerDesKilometres
{
  <lemma> {
    entry <- "avaler";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "kilomètres"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mwePrendreSonPied
{
  <lemma> {
    entry <- "prendre";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "son"/d;
    coanchor ObjNode -> "pied"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeFaireLaChevre
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "chèvre"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeFaireLAne
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "âne"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeFaireLAutruche
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "autruche"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBalayerUneCritique
{
  <lemma> {
    entry <- "balayer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "critique"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mwePerdreLesPedales
{
  <lemma> {
    entry <- "perdre";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = passive;
    filter subj = free;
    filter passivetype= short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "pédales"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mwePeterLeFeu
{
  <lemma> {
    entry <- "péter";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "feu"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweCouperLesPonts
{
  <lemma> {
    entry <- "couper";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "ponts"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweAccuserLeCoup
{
  <lemma> {
    entry <- "accuser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "coup"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweCompterLesHeures
{
  <lemma> {
    entry <- "compter";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "heures"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweConjurerLeSort
{
  <lemma> {
    entry <- "conjurer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "sort"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}



class mweDemanderLaLune
{
  <lemma> {
    entry <- "demander";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "lune"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}



class mweEpaterLaGalerie
{
  <lemma> {
    entry <- "épater";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = passive;
    filter subj = free;
    filter passivetype= short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "galerie"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLacherLesRenes
{
  <lemma> {
    entry <- "lâcher";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "rênes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweLeverLePied
{
  <lemma> {
    entry <- "lever";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "pied"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLouperLeCoche
{
  <lemma> {
    entry <- "louper";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "coche"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}




class mwePeignerLaGirafe
{
  <lemma> {
    entry <- "peigner";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "girafe"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mwePerdreLaTete
{
  <lemma> {
    entry <- "perdre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "tête"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}



class mwePisserLeSang
{
  <lemma> {
    entry <- "pisser";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "sang"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweAvalerUneCouleuvre
{
  <lemma> {
    entry <- "avaler";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "couleuvre"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mwePrendreLAir
{
  <lemma> {
    entry <- "prendre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "air"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mwePrendreLaMouche
{
  <lemma> {
    entry <- "prendre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "mouche"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mwePuerLaMerde
{
  <lemma> {
    entry <- "puer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "merde"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweRaserLesMurs
{
  <lemma> {
    entry <- "raser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "murs"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweRecollerLesMorceaux
{
  <lemma> {
    entry <- "recoller";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "morceaux"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweRectifierLeTir
{
  <lemma> {
    entry <- "rectifier";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "tire"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweRemonterLaPente
{
  <lemma> {
    entry <- "remonter";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "pente"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweRespirerLEnnui
{
  <lemma> {
    entry <- "respirer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "ennui"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweRespirerLaSante
{
  <lemma> {
    entry <- "respirer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "santé"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweResserrerLesBoulons
{
  <lemma> {
    entry <- "resserrer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "boulons"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweSauterLePas
{
  <lemma> {
    entry <- "sauter";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "pas"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweSauverLaFace
{
  <lemma> {
    entry <- "sauver";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "face"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweSauverLesMeubles
{
  <lemma> {
    entry <- "sauver";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "meubles"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweSemerLaMort
{
  <lemma> {
    entry <- "semer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "mort"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweSentirLeFauve
{
  <lemma> {
    entry <- "sentir";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "fauve"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweSonderLeTerrain
{
  <lemma> {
    entry <- "sonder";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "terrain"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweTaquinerLeGoujon
{
  <lemma> {
    entry <- "taquiner";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "goujon"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweTenirLaChandelle
{
  <lemma> {
    entry <- "tenir";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "chandelle"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweTrainerLesPieds 

{
  <lemma> {
    entry <- "traîner";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "pieds"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweBoireUneBouteille
{
  <lemma> {
    entry <- "boire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "bouteille"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBoireUnCoup
{
  <lemma> {
    entry <- "boire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "coup"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBoireUnPot
{
  <lemma> {
    entry <- "boire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "pot"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBoireLaTasse
{
  <lemma> {
    entry <- "boire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "tasse"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBoireUnVerre
{
  <lemma> {
    entry <- "boire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "verre"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBrasserDesMillions
{
  <lemma> {
    entry <- "brasser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "millions"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

%class mweBrosserUnPortrait   %(se faire brosser le portrait ?)
%{
%  <lemma> {
%    entry <- "brosser";
%    fam <- mwen0Vn1;
%    cat <- v;
%    filter dia = passive;
%    filter subj = free;
%    filter subj = lexicalized;
%    filter passivetype= short;
%    filter obj = lexicalized;
%    filter objstruct = lexDetLexN;
%    coanchor ObjDetNode -> "le"/d;
%    coanchor ObjNode -> "portrait"/n;
%    equation ObjNode -> gen=m;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-
%  }
%}



class mweChercherDesHistoires
{
  <lemma> {
    entry <- "chercher";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "histoires"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweCourirUnDanger
{
  <lemma> {
    entry <- "courir";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "danger"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


class mweCourirUnRisque
{
  <lemma> {
    entry <- "courir";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "risque"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweChasserLeNaturel
{
  <lemma> {
    entry <- "chasser";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "naturel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweChargerLaMule
{
  <lemma> {
    entry <- "charger";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "mule"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweRabaisserLeCaquet
{
  <lemma> {
    entry <- "rabaisser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "caquet"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweCoulerUnBronze
{
  <lemma> {
    entry <- "couler";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "bronze"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweCouterUnBras      %ça me coute un bras
{
  <lemma> {
    entry <- "coûter";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "bras"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBoireUnCanon
{
  <lemma> {
    entry <- "boire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "canon"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBalancerLaSauce
{
  <lemma> {
    entry <- "balancer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "sauce"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweEnvoyerLaSauce
{
  <lemma> {
    entry <- "envoyer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "sauce"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
} 

class mweBattreSonPlein
{
  <lemma> {
    entry <- "battre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "son"/d;
    coanchor ObjNode -> "plein"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
} 

class mweTaillerUneBavette
{
  <lemma> {
    entry <- "tailler";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "bavette"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
} 


class mweSentirLeFagot
{
  <lemma> {
    entry <- "sentir";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "fagot"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweSucrerLesFraises
{
  <lemma> {
    entry <- "sucrer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "fraises"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweRetournerSaVeste
{
  <lemma> {
    entry <- "retourner";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "sa"/d;
    coanchor ObjNode -> "veste"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}



class mweLemmeTournerLesTalons
{
  <lemma> {
    entry <- "tourner";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "talons"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmePerdreSonSangFroid
{
  <lemma> {
    entry <- "perdre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "son"/d;
    coanchor ObjNode -> "sang-froid"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeFaireLaTete
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "tête"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeFaireLaGueule
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "gueule"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweEtoufferUnScandale
{
  <lemma> {
    entry <- "étouffer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "scandale"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweFaitUnCouac     %(il y a un couac ?)
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = passive;
    filter subj = free;
    filter subj = lexicalized;
    filter passivetype= short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "couac"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweGelerUnCompte
{
  <lemma> {
    entry <- "geler";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "compte"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweGrillerUnStop
{
  <lemma> {
    entry <- "griller";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "stop"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLancerUneMode
{
  <lemma> {
    entry <- "lancer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "mode"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLecherlesVitrines
{
  <lemma> {
    entry <- "lécher";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "vitrines"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweMangerUnMorceau
{
  <lemma> {
    entry <- "manger";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "morceau"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweMonterUnCoup
{
  <lemma> {
    entry <- " monter";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "coup"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mwePerdreUnOeil
{
  <lemma> {
    entry <- "perdre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "oeil"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

%class mwePrendreUnSavon  (se prendre un savon ?)
%{
%  <lemma> {
%    entry <- " prendre";
%    fam <- mwen0Vn1;
 %   cat <- v;
%    filter dia = passive;
%    filter subj = free;
%    filter subj = lexicalized;
%    filter passivetype= short;
%    filter obj = lexicalized;
%    filter objstruct = lexDetLexN;
%    coanchor ObjDetNode -> "?"/d;
%    coanchor ObjNode -> "?"/n;
%    equation ObjNode -> gen=?;
%    equation ObjNode -> num=?;
%    equation ObjNode -> modifiable=-
%  }
%}

%class mwePrendreUneVeste (se prendre une veste ?)
%{
%  <lemma> {
%    entry <- " prendre";
%    fam <- mwen0Vn1;
 %   cat <- v;
%    filter dia = passive;
%    filter subj = free;
%    filter subj = lexicalized;
%    filter passivetype= short;
%    filter obj = lexicalized;
%    filter objstruct = lexDetLexN;
%    coanchor ObjDetNode -> "une"/d;
%    coanchor ObjNode -> "veste"/n;
%    equation ObjNode -> gen=f;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-
%  }
%}

class mweRaviverLaFlamme
{
  <lemma> {
    entry <- "raviver";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "flamme"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweJeterLEponge
{
  <lemma> {
    entry <- "jeter";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "éponge"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweAccuserLeCoup
{
  <lemma> {
    entry <- "accuser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "coup"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweCompterLesHeures
{
  <lemma> {
    entry <- "compter";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "heures"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweConjurerLeSort
{
  <lemma> {
    entry <- "conjurer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "sort"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}




%class mweCouronnerLeTout     (pour couronner le tout?)
%{
%  <lemma> {
%    entry <- "couronner";
%    fam <- mwen0Vn1;
 %   cat <- v;
%    filter dia = active;
%    filter subj = free;
%    filter obj = lexicalized;
%    filter objstruct = lexDetLexN;
%    coanchor ObjDetNode -> "le"/d;
%    coanchor ObjNode -> "tout"/n;
%    equation ObjNode -> gen=m;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-
%  }
%}

class mweCreverLAbces
{
  <lemma> {
    entry <- "crever";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "abcès"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweCroiserLesDoigts
{
  <lemma> {
    entry <- "croiser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "doigts"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


class mweEnfoncerLeClou
{
  <lemma> {
    entry <- "enfoncer";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "clou"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeEtreUnNavet
{
  <lemma> {
    entry <- "être";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "navet"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeRecevoirUneChataigne
{
  <lemma> {
    entry <- "recevoir";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "châtaigne"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}



class mweGagnerLePompon
{
  <lemma> {
    entry <- "gagner";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "pompon"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}



class mweGrillerLesEtapes
{
  <lemma> {
    entry <- "griller";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "étapes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


class mweMettreLesGaz
{
  <lemma> {
    entry <- "mettre";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "gaz"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mwePoserUnLapin
{
  <lemma> {
    entry <- "poser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "lapin"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweNoyerLePoisson
{
  <lemma> {
    entry <- "noyer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = passive;
    filter subj = free;
    filter passivetype= short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "poisson"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mwePasserLaSeconde 
{
  <lemma> {
    entry <- "passer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "seconde"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLesMursOntDesOreilles
{
  <lemma> {
    entry <- "avoir";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = lexDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "les"/d;
    coanchor SubjNode -> "murs"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=pl;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "oreilles"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweLaPatienceADesLimites
{
  <lemma> {
    entry <- "avoir";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = freeDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "patience"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "limites"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}
 
class mweLArgentBruleLesDoigts        %(me brûle les doigts?)
{
  <lemma> {
    entry <- "brûler";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = lexDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "l'"/d;
    coanchor SubjNode -> "argent"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "doigts"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweUneQuestionBruleLaLangue     %(me brûle ?)
{
  <lemma> {
    entry <- "brûler";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = freeDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "une"/d;
    coanchor SubjNode -> "question"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "langue"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLEstomacCrieFamine
{
  <lemma> {
    entry <- "crier";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = freeDetLexN;
    filter objstruct = lexN;
    coanchor SubjDetNode -> "l'"/d;
    coanchor SubjNode -> "estomac"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg;
    coanchor ObjNode -> "famine"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLElevedepasseLeMaitre
{
  <lemma> {
    entry <- "dépasser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = lexDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "l'"/d;
    coanchor SubjNode -> "élève"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "maître"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=f;
    equation ObjNode -> modifiable=-
  }
}

class mweUnDouteEffleureLEsprit   %(m'effleure ?)
{
  <lemma> {
    entry <- "effleurer";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = freeDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "un"/d;
    coanchor SubjNode -> "doute"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "esprit"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLesDeuxfontLaPaire
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = lexDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "les"/d;
    coanchor SubjNode -> "deux"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=pl;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "paire"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweUnIncendieFaitRage
{
  <lemma> {
    entry <- "faire";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = freeDetLexN;
    filter objstruct = lexN;
    coanchor SubjDetNode -> "un"/d;
    coanchor SubjNode -> "incendie"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg;
    coanchor ObjNode -> "rage"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLesEcaillesTombentDesYeux   %pas de class adéquate
{
  <lemma> {
    entry <- "tomber";
    fam <- mwen0Vn1;
    cat <- v;
    filter dia = active;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = lexDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "les"/d;
    coanchor SubjNode -> "écailles"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=pl;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "yeux"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweUnepenseeTraverselEsprit        %(me traverse ?)
{
  <lemma> {
    entry <- "traverser";
    fam <- mwen0Vn1;
    cat <- v;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter subjstruct = lexDetLexN;
    filter objstruct = lexDetLexN;
    coanchor SubjDetNode -> "une"/d;
    coanchor SubjNode -> "pensée"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg;
    coanchor ObjDetNode -> "l'"/d;
    coanchor ObjNode -> "esprit"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}





class mweLemmeLePrixFlambe
{
  <lemma> {
    entry <- "Flamber";
    cat   <- v;
    fam   <- mwen0V;
    filter subj = lexicalized;
    filter subjstruct = freeDetLexN;
    coanchor SubjDetNode -> "le"/d;
    coanchor SubjNode -> "prix"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg
  }
}


class mweLemmeLaLangueFourche
{
  <lemma> {
    entry <- "Fourcher";
    cat   <- v;
    fam   <- mwen0V;
    filter subj = lexicalized;
    filter subjstruct = freeDetLexN;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "langue"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}

class mweLemmeLHeureTourne
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- mwen0V;
    filter dia = active;
    filter subj = lexicalized;
    filter subjstruct = lexDetLexN;
    coanchor SubjDetNode -> "l'"/d;
    coanchor SubjNode -> "heure"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}


%class mweLemmeLaPeurColleALaPeau      (la peur lui colle à la peau ?)
%{
%  <lemma> {
%    entry <- "coller";
%    cat   <- v;
%    fam   <- mwen0Vn1an2;
%    filter subj = free;
%    filter obj = lexicalized;
%    filter iobj = lexicalized;
%    filter iobjstruct = lexDetLexN;
%    coanchor IObjDetNode -> "la"/d;
%    coanchor IObjNode -> "peau"/n;
%    equation IObjNode -> gen=f;
%    equation IObjNode -> num=sg;
%    equation IObjNode -> modifiable=-;
%    filter objtype = canonical;
%    coanchor SubjDetNode -> "la"/d;
%    coanchor SubjNode -> "peur"/n;
%    equation SubjNode -> gen=f;
%    equation SubjNode -> num=sg
%  }
%}

class mweLemmeUnMalheurFrappeALaPorte
{
  <lemma> {
    entry <- "frapper";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter iobjstruct = lexDetLexN;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "porte"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor SubjDetNode -> "un"/d;
    coanchor SubjNode -> "malheur"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg
  }
}

class mweLemmeLeSangMonteALaTete
{
  <lemma> {
    entry <- "monter";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter iobjstruct = lexDetLexN;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "tête"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor SubjDetNode -> "le"/d;
    coanchor SubjNode -> "sang"/n;
    equation SubjNode -> gen=m;
    equation SubjNode -> num=sg
  }
}



class mweLemmePrendreUnBainDeFoule
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = freeDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "bain"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "foule"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmePrendreUnBainDeSoleil
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "bain"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "soleil"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeCouterLaPeauDesFesses
{
  <lemma> {
    entry <- "coûter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetlexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "peau"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "les"/d;
    coanchor IObjNode -> "fesses"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=pl;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeAvoirLeCoupDeFoudre
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "coup"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "foudre"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirUneMemoireDElephant
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "mémoire"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "éléphant"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirUneTailleDeGuepe
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "taille"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "guêpe"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeAvoirUneMemoireDePoisson
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "mémoire"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "poisson"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}
 
class mweLemmeAvoirUneFaimDeLoup
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjNode -> "faim"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "loup"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeCouterLaPeauDuCul
{
  <lemma> {
    entry <- "coûter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "peau"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "cul"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeDiscuterLeBoutDeGras
{
  <lemma> {
    entry <- "discuter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetlexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "bout"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "gras"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeDelierLesCordonsDeSaBourse
{
  <lemma> {
    entry <- "délier";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = freeDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "cordons"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "sa"/d;
    coanchor IObjNode -> "bourse"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeDeterrerLaHacheDeGuerre
{
  <lemma> {
    entry <- "déterrer";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "hache"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "guerre"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeEcouterLaVoixDeLaRaison
{
  <lemma> {
    entry <- "écouter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "voix"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "raison"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeFinirLesFondsDeBouteille
{
  <lemma> {
    entry <- "finir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "fonds"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "bouteille"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeGratterLesFondsDeTiroir
{
  <lemma> {
    entry <- "gratter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "fonds"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "tiroir"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmePorterLaMarqueDuGenie
{
  <lemma> {
    entry <- "porter";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "marque"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "génie"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmePrendreLeCheminDesEcoliers
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "chemin"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "écoliers"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=pl;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmePrendreLaPoudreDEscampette
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "poudre"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "escampette"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeSonnerLeBranlebasDeCombat
{
  <lemma> {
    entry <- "sonner";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "branlebas"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "combat"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeTenirLesCordonsDeLaBourse
{
  <lemma> {
    entry <- "tenir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "cordons"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "bourse"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeVoirLeBoutDuTunnel
{
  <lemma> {
    entry <- "voir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "bout"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "tunnel"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAllongerLaSauce
{
  <lemma> {
    entry <- "allonger";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "sauce"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweBriserLaGlace
{
  <lemma> {
    entry <- "briser";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "glace"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeLaPeurDonneDesAiles         %pas de class adéquate
{
  <lemma> {
    entry <- "peur";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = lexicalized;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "ailes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "peur"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%EP test Sarah (mai 2019)
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class mweLemmeDevoirUneFiereChandelle
{
  <lemma> {
    entry <- "devoir";
    cat   <- v;
    fam   <- mwen0vA;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexNLexAdj;
    coanchor ObjDetNode -> "une"/d;
    coanchor ObjAdjNode -> "fière"/adj;
    coanchor ObjNode -> "chandelle"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmePleuvoirDesCordes
{
  <lemma> {
    entry <- "pleuvoir";
    cat   <- v;
    fam   <- mweilV;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "cordes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl
  }
}

%class mweLemmeLesChiensNeFontPasDesChats
%{
 % <lemma> {
  %  entry <- "faire";
   % cat   <- v;
    %fam   <- 	mwen0Vn1negPas;
   % filter dia = active;
    %filter subj = lexicalized;
    %filter obj = lexicalized;
    %filter objstruct = lexDetLexN;
    %coanchor ObjDetNode -> "des"/d;
    %coanchor ObjNode -> "chats"/n;
    %equation ObjNode -> gen=m;
    %equation ObjNode -> num=pl;
    %equation ObjNode -> modifiable=-;
    %filter objtype = canonical
 % }
%}



class mweLemmePrendreSesJambesASonCou
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "ses"/d;
    coanchor ObjNode -> "jambes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "son"/d;
    coanchor IObjNode -> "cou"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

%class mweLemmeSerrerLaCeinture
%{
%  <lemma> {
 %   entry <- "serrer";
  %  cat   <- v;
   % fam   <- mweClCopule;
    %filter subj = free;
    %filter obj = lexicalized;
    %filter objstruct = lexDetLexN;
    %coanchor ObjDetNode -> "la"/d;
    %coanchor ObjNode -> "ceinture"/n;
    %equation ObjNode -> gen=f;
    %equation ObjNode -> num=sg
  %}
%}

class mweLemmeAvoirUnChatDansLaGorge
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "un"/d;
    coanchor ObjNode -> "chat"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "gorge"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirLeCompasDansLeOeil
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "compas"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "oeil"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


%class mweLemmePlierBagage
%{
%  <lemma> {
%    entry <- "plier";
%    cat   <- v;
%    fam   <- mwen0Vn1;
%    filter dia = active;
%    filter subj = free;
%    filter obj = lexicalized;
%    filter objstruct = lexN;
%    coanchor ObjNode -> "bagage"/n;
%    equation ObjNode -> gen=m;
%    equation ObjNode -> num=sg;
%    equation ObjNode -> modifiable=-;
%    filter objtype = canonical
%  }
%}


class mweLemmeRendreLaPareille
{
  <lemma> {
    entry <- "rendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "pareille"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLesDesSontLances
{
  <lemma> {
    entry <- "lancer";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = passive;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "dés"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}

class mweLemmeDonnerSaLangueAuChat
{
  <lemma> {
    entry <- "donner";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = freeDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "sa"/d;
    coanchor ObjNode -> "langue"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "chat"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeBattreLesOeufsEnNeige
{
  <lemma> {
    entry <- "battre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "oeufs"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor Prep -> "en"/p;
    coanchor IObjNode -> "neige"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeResterDeMarbre
{
  <lemma> {
    entry <- "rester";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexN;
    filter objtype = canonical;
    coanchor IObjNode -> "marbre"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirDesYeuxDeLynx
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "des"/d;
    coanchor ObjNode -> "yeux"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjNode -> "lynx"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeAvoirFoiEndet  %avoir foi en soi/lui/elle
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1des2;
    filter dia = active;
    coanchor ObjNode -> "foi"/n;
    filter subj = free;
    filter obj = lexicalized;
    filter objtype = canonical;
    filter objstruct = lexN;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter sentobject=free
  }
}

%class mweLemmePleurerCommeUneMadeleine
%{
 % <lemma> {
  %  entry <- "pleurer";
   % cat   <- v;
    %fam   <- mwen0Vpn1;
    %filter subj = free;
    %filter obj = free;
    %filter obl = lexicalized;
    %filter obltype = canonical;
    %filter oblstruct = lexN;
    %filter adv2 = comme;
    %coanchor Adv -> "comme"/adv;
    %equation ObjNode -> modifiable=-;
    %filter objtype = canonical;
    %coanchor IObjDetNode -> "une"/d;
    %coanchor IObjNode -> "madeleine"/n;
    %equation IObjNode -> gen=f;
    %equation IObjNode -> num=sg;
    %equation IObjNode -> modifiable=-
 % }
%}


%class mweLemmeRevenirASesMoutons
%{
 % <lemma> {
 %   entry <- "revenir";
 %   cat   <- v;
 %   fam   <- mwen0Vpn1re;
 %   filter subj = free;
 %   filter iobj = lexicalized;
 %   filter iobjstruct = freeDetLexN;
 %   coanchor IObjDetNode -> "ses"/d;
 %   filter objstruct = lexDetLexN;
 %   coanchor IObjNode -> "moutons"/n;
 %   equation IObjNode -> gen=m;
 %   equation IObjNode -> num=pl;
 %   equation IObjNode -> modifiable=-
 % }
%}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Agata Savary (4 March 2018)
% MWEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


class mweLemmeAvoirLieu
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "lieu"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}



class mweLemmeAvoirLieuDeInf  %evoir lieu d'être
{
  <lemma> {
    entry <- "avoir";
    cat   <- v;
    fam   <- mwen0Vn1des2;
    filter dia = active;
    coanchor ObjNode -> "lieu"/n;
    filter subj = free;
    filter obj = lexicalized;
    filter objtype = canonical;
    filter objstruct = lexN;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter sentobject=free
  }
}


class mweLemmeAppelerASecours
{
  <lemma> {
    entry <- "appeler";
    cat   <- v;
    fam   <- mwen0Van1;
    filter subj = free;
    filter iobj = lexicalized;
    filter iobjstruct = freeDetLexN;
%    equation iobjdettype = detOrPoss;
    coanchor IObjNode -> "secours"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}

class mweLemmeClouerAuSol
{
  <lemma> {
    entry <- "clouer";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexDetLexN;
    coanchor IObjDetNode -> "le"/d;
    coanchor IObjNode -> "sol"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}



class mweLesCarottesSontCuites
{
  <lemma> {
    entry <- "cuire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = passive;
    filter passivetype = short;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "les"/d;
    coanchor ObjNode -> "carottes"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=pl;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeFaireFace
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "face"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeFairePartie
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1den2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter genitive = free;
    filter objstruct = lexN;
    coanchor ObjNode -> "partie"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}

class mweLemmeFaireProfilBas
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexNLexAdj;
    coanchor ObjNode -> "profil"/n;
    coanchor ObjAdjNode -> "bas"/adj;
    equation ObjAdjNode -> gen=m;
    equation ObjAdjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireAppel
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    coanchor ObjNode -> "appel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireAppelAN  %faire appel à quelqu'un
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexN;
    filter iobj = free;
    coanchor ObjNode -> "appel"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}

class mweLemmeFaireLeObjet
{
  <lemma> {
    entry <- "faire";
    cat   <- v;
    fam   <- 	mwen0Vn1den2;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    filter genitive = free;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "objet"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


class mweLemmeMettreEnExamen
{
  <lemma> {
    entry <- "mettre";
    cat   <- v;
    fam   <- mwen0Vn1pn2;
    filter subj = free;
    filter obj = free;
    filter obl = lexicalized;
    filter obltype = canonical;
    filter oblstruct = lexN;
    filter prep2 = en;
    coanchor Prep -> "en"/p;
    coanchor OblNode -> "examen"/n;
    equation OblNode -> gen=m;
    equation OblNode -> num=sg;
    equation OblNode -> modifiable=-
  }
}


class mweLaNuitTombe
{
  <lemma> {
    entry <- "tomber";
    cat   <- v;
    fam   <- mwen0V;
    filter dia = active;
    filter subj = lexicalized;
    filter subjstruct = lexDetLexN;
    coanchor SubjDetNode -> "la"/d;
    coanchor SubjNode -> "nuit"/n;
    equation SubjNode -> gen=f;
    equation SubjNode -> num=sg
  }
}


class mweLemmePousserABout
{
  <lemma> {
    entry <- "pousser";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = free;
    filter iobj = lexicalized;
    filter iobjstruct = lexN;
    coanchor IObjNode -> "bout"/n;
    equation IObjNode -> gen=m;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmePrendreLaPorte
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- 	mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "porte"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical
  }
}


class mweLemmePrendreLaFuite
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "fuite"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    filter objtype = canonical
  }
}


class mweLemmePrendreLaParole
{
  <lemma> {
    entry <- "prendre";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "parole"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-
  }
}


class mweLemmeJoindreLeGesteALaParole
{
  <lemma> {
    entry <- "joindre";
    cat   <- v;
    fam   <- mwen0Vn1an2;
    filter subj = free;
    filter obj = lexicalized;
    filter iobj = lexicalized;
    filter objstruct = lexDetLexN;
    filter iobjstruct = lexDetLexN;
    coanchor ObjDetNode -> "le"/d;
    coanchor ObjNode -> "geste"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg;
    equation ObjNode -> modifiable=-;
    filter objtype = canonical;
    coanchor IObjDetNode -> "la"/d;
    coanchor IObjNode -> "parole"/n;
    equation IObjNode -> gen=f;
    equation IObjNode -> num=sg;
    equation IObjNode -> modifiable=-
  }
}


class mweLemmeTournerLaPage
{
  <lemma> {
    entry <- "tourner";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = lexDetLexN;
    coanchor ObjDetNode -> "la"/d;
    coanchor ObjNode -> "page"/n;
    equation ObjNode -> gen=f;
    equation ObjNode -> num=sg
  }
}

%class mweLemmeMettreTest
%{
%  <lemma> {
%    entry <- "mettre";
%    cat   <- v;
%    fam   <- mwen0Vn1;
%    filter dia = passive;
%    filter passivetype = short;
%    filter subj = free;
%    filter obj = free
%  }
%}


class mweLemmeViderSonSac
{
  <lemma> {
    entry <- "vider";
    cat   <- v;
    fam   <- mwen0Vn1;
    filter dia = active;
    filter subj = free;
    filter obj = lexicalized;
    filter objstruct = freeDetLexN;
    coanchor ObjNode -> "sac"/n;
    equation ObjNode -> gen=m;
    equation ObjNode -> num=sg
  }
}

value LemmeDePrep
value LemmeDe

value mweJeterLEponge
value mweChasserLeNaturel
value mweChargerLaMule
value mweRabaisserLeCaquet
value mweCoulerUnBronze
value mweCouterUnBras
value mweBoireUnCanon
value mweTaillerUneBavette
value mweBattreSonPlein
value mweBalancerLaSauce
value mweEnvoyerLaSauce
value mweSucrerLesFraises
value mweSentirLeFagot
value mweLemmeAvoirLeCoupDeFoudre
value mwePoserUnLapin
value mweAccuserLeCoup
value mweCompterLesHeures
value mweConjurerLeSort
%value mweCouronnerLeTout
value mweCreverLAbces
value mweCroiserLesDoigts
value mweDemanderLaLune
value mweEnfoncerLeClou
value mweEpaterLaGalerie
value mweGagnerLePompon
value mweGrillerLesEtapes
value mweLacherLesRenes
value mweLeverLePied
value mweLouperLeCoche
value mweMettreLesGaz
value mweNoyerLePoisson
value mwePasserLaSeconde
value mwePeignerLaGirafe
value mwePerdreLaTete
value mwePerdreLesPedales
value mwePeterLeFeu
value mwePisserLeSang
value mwePrendreLAir
value mwePrendreLaMouche
value mwePuerLaMerde
value mweRaserLesMurs
value mweRecollerLesMorceaux
value mweRectifierLeTir
value mweRemonterLaPente
value mweRespirerLEnnui
value mweRespirerLaSante
value mweResserrerLesBoulons
value mweSauterLePas
value mweSauverLaFace
value mweSauverLesMeubles
value mweSemerLaMort
value mweSentirLeFauve
value mweSonderLeTerrain
value mweTaquinerLeGoujon
value mweTenirLaChandelle
value mweLemmeFaireLAutruche
value mweLemmeFaireLAne
value mweLemmeAvoirUneMemoireDElephant

value mweTrainerLesPieds

value mweLesMursOntDesOreilles
value mweLaPatienceADesLimites
value mweLArgentBruleLesDoigts
value mweUneQuestionBruleLaLangue
value mweLEstomacCrieFamine
value mweLElevedepasseLeMaitre
value mweUnDouteEffleureLEsprit
value mweLesDeuxfontLaPaire
value mweUnIncendieFaitRage
value mweLesEcaillesTombentDesYeux
value mweUnepenseeTraverselEsprit

value mweAvalerDesKilometres
value mweBalayerUneCritique
value mweBoireUneBouteille
value mweBoireUnCoup
value mweBoireUnPot
value mweBoireUnVerre
value mweBrasserDesMillions
%value mweBrosserUnPortrait
value mweChercherDesHistoires
value mweCourirUnDanger
value mweCourirUnRisque
value mweEtoufferUnScandale
value mweFaitUnCouac
value mweGelerUnCompte
value mweGrillerUnStop
value mweLancerUneMode
value mweLecherlesVitrines
value mweMangerUnMorceau
value mweMonterUnCoup
value mwePerdreUnOeil
%value mwePrendreUnSavon
%value mwePrendreUneVeste
value mweRaviverLaFlamme
value mwePrendreSonPied
value mweCouperLesPonts
value mweAvalerUneCouleuvre
value mweRetournerSaVeste

value mweLemmeAllongerLaSauce
value mweBriserLaGlace
value mweLemmeLaPeurDonneDesAiles

value mweLemmeLePrixFlambe
value mweLemmeLaLangueFourche
value mweLemmeLHeureTourne
%value mweLemmeLaPeurColleALaPeau
value mweLemmeUnMalheurFrappeALaPorte
value mweLemmeLeSangMonteALaTete
value mweLemmeDelierLesCordonsDeSaBourse

value mweLemmePrendreUnBainDeFoule
value mweLemmePrendreUnBainDeSoleil
value mweLemmeCouterLaPeauDesFesses
value mweLemmeCouterLaPeauDuCul
value mweLemmeDiscuterLeBoutDeGras
value mweLemmeDeterrerLaHacheDeGuerre
value mweLemmeEcouterLaVoixDeLaRaison
value mweLemmeFinirLesFondsDeBouteille
value mweLemmeGratterLesFondsDeTiroir
value mweLemmePorterLaMarqueDuGenie
value mweLemmePrendreLeCheminDesEcoliers
value mweLemmePrendreLaPoudreDEscampette
value mweLemmeSonnerLeBranlebasDeCombat
value mweLemmeTenirLesCordonsDeLaBourse
value mweLemmeVoirLeBoutDuTunnel
value mweLemmeAvoirUneFaimDeLoup
value mweLemmeFaireLaChevre
value mweLemmeAvoirUneMemoireDePoisson
value mweLemmeAvoirUneTailleDeGuepe
value mweLemmeFaireLaTete
value mweLemmeFaireLaGueule
value mweLemmePerdreSonSangFroid
value mweLemmeTournerLesTalons
value mweLemmeRecevoirUneChataigne
value mweLemmeEtreUnNavet


value mweLemmeAvoirFoiEndet
value mweLemmeAvoirDesYeuxDeLynx
value mweLemmeResterDeMarbre
value mweLemmeBattreLesOeufsEnNeige
value mweLemmeDonnerSaLangueAuChat
value mweLesDesSontLances
value mweLemmeRendreLaPareille
value mweLemmeAvoirLeCompasDansLeOeil
value mweLemmeAvoirUnChatDansLaGorge
value mweLemmePrendreSesJambesASonCou
value mweLemmePleuvoirDesCordes
value mweLemmeDevoirUneFiereChandelle

value mweLemmeAppelerASecours
value mweLemmeAvoirLieu
value mweLemmeClouerAuSol
value mweLesCarottesSontCuites
value mweLemmeFaireFace
value mweLemmeFaireAppel
value mweLemmeFaireAppelAN
value mweLemmeFaireLeObjet
value mweLemmeFairePartie
value mweLemmeFaireProfilBas
value mweLemmeMettreEnExamen
value mweLaNuitTombe
value mweLemmePousserABout
value mweLemmePrendreLaPorte
value mweLemmePrendreLaFuite
value mweLemmePrendreLaParole
value mweLemmeJoindreLeGesteALaParole
value mweLemmeTournerLaPage

