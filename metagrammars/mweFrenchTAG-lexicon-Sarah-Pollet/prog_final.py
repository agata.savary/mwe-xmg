"""
Created on Wed May 29 14:53:34 2019
@author: Sarah Pollet
"""
# morphèmes et lemmes et leurs évaluations
f_in = open("lefff-final","r", encoding="utf-8")
f_morph_out = open("morph-python-finalokok.mg","w", encoding="utf-8")
f_val_morph = open("val-morph-python-finalokok.mg","w", encoding="utf-8")
f_val_lem = open("lem-sup.mg","w", encoding="utf-8")
f_lem_out = open("lemmes-sup.mg","w", encoding="utf-8")
value="Lemma"
fam=""#famille
mot_fam=""#contient le couples mot+sa catégorie ou mot+sa famille
liste_lemme=[]#liste contenant les mots pour vérifier qu'ils n'apparaissent pas deux fois
token=[]
genre=""
nb=""
l=""
val="="
liste_morph=[]
md=""
cat=""#catégorie
lemme=""
import re

f_val_morph.write("include morph-python-final.mg\n\n")#pour inclure le fichier des formes fléchies dans celui de ses évaluations
f_val_lem.write("include lemmes-python-final.mg\n\n")

def writeMorph():#pour écrire une fois les formes fléchies
    mot_fam=liste_morph[0]+cat
    if not mot_fam in liste_lemme:
        liste_lemme.append(liste_morph[0]+cat)
        f_morph_out.write("class "+liste_morph[0]+cat.capitalize()+"\n{\n  <morpho> {\n\tmorph <-\""+liste_morph[0]+"\";\n\tlemma <-\""+liste_morph[5]+"\";\n\tcat <- "+cat+"\n\t}\n}\n\n\n")
        f_val_morph.write("value "+liste_morph[0]+cat.capitalize()+"\n") #création du fichier des évaluations morph
    
#def writeClass():#pour écrire une fois les lemmes
#    mot_fam=liste_morph[5]+fam
#    if not mot_fam in liste_lemme:
#        liste_lemme.append(liste_morph[5]+fam)
#        f_lem_out.write("class "+value+liste_morph[5].capitalize()+fam.capitalize()+" {\n  <lemma> {\n\t entry <- \""+liste_morph[5]+"\";\n\t cat <- "+cat+";\n\t fam <- "+fam+"\n  }\n}\n\n")
#        f_val_lem.write("value "+value+liste_morph[5].capitalize()+fam.capitalize()+"\n")#création du fichier
#                       

        
for line in f_in:
    if val in line:
        modif=re.sub("_+[a-z_]*[1-9]?_*[1-9]","\t",line)
        liste_morph=modif.split("\t") #découpage au niveau des tabulations
        l=liste_morph[0]
        lemme=liste_morph[5]
        dic=""#dictionnaire
#verbs
        if not " " in liste_morph[0] and not "'" in liste_morph[0] and not " " in liste_morph[5] and not "'" in liste_morph[5]:
            if "v"==liste_morph[2]:
                if "P" in liste_morph[8]:
                    md="ind"
                elif "F" in liste_morph[8]:
                    md="ind"
                elif "J" in liste_morph[8]:
                    md="ind"
                elif "I" in liste_morph[8]:
                    md="ind"
                elif "C" in liste_morph[8]:
                    md="cond"
                elif "Y" in liste_morph[8]:
                    md="imp"
                elif "S" in liste_morph[8]:
                    md="subj"
                elif "T" in liste_morph[8]:
                    md="subj"
                elif "K" in liste_morph[8]:
                    md="ppart"
                elif "G" in liste_morph[8]:
                    md="ppart"
                cat="v"
                tab = ["s","p","1","2","3","W","P","P","F","F","J","J","I","I","C","C","Y","Y","S","S","T","T","K","K","G","G"],["num <- sg;\n\t","num <- pl;\n\t","pers <- 1;\n\t","pers <- 2;\n\t","pers <- 3;\n\t","mode <- inf;\n\t","mode <- ind;\n\t","tense <- pres;\n\t","mode <- ind;\n\t","tense <- future;\n\t","mode <- ind;\n\t","tense <- spast;\n\t","mode <- ind;\n\t","tense <- imp;\n\t","mode <- cond;\n\t","tense <- pres;\n\t","mode <- imp;\n\t","tense <- pres;\n\t","mode <- subj;\n\t","tense <- pres;\n\t","mode <- subj;\n\t","tense <- imp;\n\t","mode <- ppart;\n\t","tense <- past;\n\t","mode <- ppart;\n\t","tense <- pres;\n\t"]
                for n, cle in enumerate(tab[0]):
                    if cle in liste_morph[8]:
                        dic=dic+tab[1][n]
                mot_fam=liste_morph[0]+cat+md                
                if not mot_fam in liste_lemme:#writeMorph() n'est pas utilisé ici car la notation pour les formes fléchies des verbes n'est pas la même que pour les autres catégories de mots
                    liste_lemme.append(liste_morph[0]+cat+md)
                    f_morph_out.write("class "+liste_morph[0]+liste_morph[2].capitalize()+md.capitalize()+"\n{\n  <morpho> {\n\tmorph <-\""+liste_morph[0]+"\";\n\tlemma <-\""+liste_morph[5]+"\";\n\t"+dic+"cat <- "+cat+"\n\t}\n}\n\n\n")
                    f_val_morph.write("value "+liste_morph[0]+liste_morph[2].capitalize()+md.capitalize()+"\n") #création du fichier des évaluations morph          
                
                if lemme==liste_morph[0]:
                    refListe = ["copule","ilV","n0V","n0ClV","n0Vn1","n0ClVn1","n0Van1","n0Vden1","n0clVden1","n0Vloc1","n0Vn1an2","n0Van1den2","n0Vn1den2"]
                    listeVerbes = [[("@AttSuj",True)],[("@impers",True),("Objde",False),("Objà",False),("Obj:",False)],[("@pers",True),("Objde",False),("Objà",False),("Obj:",False),("@pron",False)],[("@pers",True),("Objde",False),("Objà",False),("Obj:",False),("@pron",True)],[("@pers",True),("Objde",False),("Objà",False),(r"Obj:.*sn.*,",True),("@pron",False)],[("@pers",True),("Objde",False),("Objà",False),(r"Obj:.*sn.*,",True),("@pron",True)],[("@pers",True),("Objde",False),("Objà",True),(r"Obj:.*sn.*,",False),("@pron",False)],[("@pers",True),("Objde",True),("Objà",False),(r"Obj:.*sn.*,",False),("@pron",False)],[("@pers",True),("Objde",True),("Objà",False),(r"Obj:.*sn.*,",False),("@pron",True)],[("@pers",True),("Objde",False),("Objà",False),(r"Obj:.*sn.*,",False),("@pron",False),(r"Dloc|Loc",True)],[("@pers",True),("Objde",False),("Objà",True),(r"Obj:.*sn.*,",True),("@pron",False)],[("@pers",True),("Objde",True),("Objà",True),(r"Obj:.*sn.*,",False),("@pron",False)],[("@pers",True),("Objde",True),("Objà",False),(r"Obj:.*sn.*,",True),("@pron",False)]]
                    boo = [True,True,True,True,True,True,True,True,True,True,True,True,True]
                    for n in range(len(listeVerbes)):
                        for regex, cond  in listeVerbes[n]:
                            if re.search(regex,liste_morph[4]):
                                if cond:
                                    boo[n]&=True
                                else:
                                    boo[n]&=False
                            else:
                                if cond:
                                    boo[n]&=False
                                else:
                                    boo[n]&=True
                      
                        if boo[n]:
                            fam=refListe[n]
                    #writeClass()
                                                                     
    # communs nouns               
            elif "nc"==liste_morph[2]:
                if "s" in liste_morph[8]:
                    nb="sg"
                else:
                    nb="pl"
                if "m" in liste_morph[8]:
                    genre="m"
                else:
                    genre="f"
                cat="n"
                fam="noun"
                tab = ["s","p","m","f"],["num <- sg;\n\t","num <- pl;\n\t","gen <- m;\n\t","gen <- f;\n\t"]
                for n, cle in enumerate(tab[0]):                
                    if cle in liste_morph[8]:
                        dic=dic+tab[1][n]            
                mot_fam=liste_morph[0]+cat
                if not mot_fam in liste_lemme:
                    liste_lemme.append(liste_morph[0]+cat)
                    f_morph_out.write("class "+liste_morph[0]+"N"+nb+genre+"\n{\n  <morpho> {\n\tmorph <-\""+liste_morph[0]+"\";\n\tlemma <-\""+liste_morph[5]+"\";\n\t"+dic+"det <- -;\n\t"+"cat <- n\n\t}\n}\n\n\n")
                    f_val_morph.write("value "+liste_morph[0]+"N"+nb+genre+"\n") #création du fichier des évaluations morph
                if lemme==liste_morph[0]:
                    refListe = ["n0vNan1","n0vNden1","noun","n0vN"]
                    listeVerbes = [[("Objà",True)],[("Objde",True)],[("Objde",False),("Objà",False)],[("Obj",False)]]
                    boo = [True,True,True,True]
                    for n in range(len(listeVerbes)):
                        for regex, cond  in listeVerbes[n]:#expression régulière
                            if re.search(regex,liste_morph[4]):
                                if cond:
                                    boo[n]&=True
                                else:
                                    boo[n]&=False
                            else:
                                if cond:
                                    boo[n]&=False
                                else:
                                    boo[n]&=True
                      
                        if boo[n]:
                            fam=refListe[n]
                #writeClass()
    #advs
            elif "adv" in liste_morph[2] or "advm" in liste_morph[2] or "advp" in liste_morph[2]:
                cat="adv"
                writeMorph()
                if lemme==liste_morph[0]:           
                    if not "Obj" in liste_morph[4]:
                        fam="advSPost"      
                        #writeClass()
                    if not "Obj" in liste_morph[4]:
                        fam="advSAnte"      
                        #writeClass()
                   
    
    #adjs
            elif "adj" in liste_morph[2]:
                cat="adj"
                tab = ["s","p","f","m"],["num <- sg;\n\t","num <- pl;\n\t","gen <- f;\n\t","gen <- m;\n\t"]
                for n, cle in enumerate(tab[0]):
                    if cle in liste_morph[8]:
                        dic=dic+tab[1][n]
                mot_fam=liste_morph[0]+cat
                if not mot_fam in liste_lemme:
                    liste_lemme.append(liste_morph[0]+cat)
                    f_morph_out.write("class "+liste_morph[0]+"Adj\n{\n  <morpho> {\n\tmorph <-\""+liste_morph[0]+"\";\n\tlemma <-\""+liste_morph[5]+"\";\n\t"+dic+"det <- -;\n\t"+"cat <- n\n\t}\n}\n\n\n")
                    f_val_morph.write("value "+liste_morph[0]+"Adj\n") #création du fichier des évaluations morph
                if lemme==liste_morph[0]:
                    if "Suj" in liste_morph[4]:
                        fam="n0vA"
                        #writeClass()
#prepositions
            elif "prep" in liste_morph[2]:
                cat="prep"
                writeMorph()
                if lemme==liste_morph[0]:#dans le fichier du lexique, s'il y a morph dans [0] il y a lemme dans [0]
                    if "loc" in liste_morph[4]:
                        fam="prepLoc"      
                        #writeClass()
                    if "Obj" in liste_morph[4]:#on souhaite avoir deux lemmes différents pour un même mot si celui-ci est à la fois "prepLoc" et "s0Pn1"
                        fam="n0Pn1"
                        #writeClass()
f_in.close()
f_morph_out.close()
f_lem_out.close()
f_val_morph.close()
f_val_lem.close()

#mwes
#les mwes sont traitées dans ce second programme car le découpage des phrases ne se fait pas au mêmes endroits que pour les mots simples
#f_in1 = open("prendre-lefff.txt","r", encoding="utf-8")
#f_val_lem1 = open("valuation-lem-python.mg","a", encoding="utf-8")
#f_lem_out1 = open("lemmes-python.mg","a", encoding="utf-8")                    
#for lin in f_in1:
#    if val in lin:                
#        tok=re.sub("=","\t",lin)
#        tok=re.sub("_+[a-z_]*[1-9]","\t",tok)
#        tok=re.sub("]","",tok)
#        tok=re.sub(" ","",tok)
#        token = tok.split("\t")
#        if "cf" in token[2]:
#            if "Suj" in token[5]:#sujet libre
#                mot_fam=token[7]
#                if not mot_fam in liste_lemme:
#                    liste_lemme.append(liste_morph[5])
#                    f_lem_out1.write("class mweLemme"+token[7].capitalize()+"\n{\n   <lemma> {\n\tentry <-"+token[6]+";\n\tcat <- v;\n\tfam <- ???;\n\tfilter subj = free\n   }\n}\n")
#                    f_val_lem1.write("value mweLemme"+token[7].capitalize()+"\n")
#f_in1.close()
#f_lem_out1.close()
#f_val_lem1.close()