#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 11:46:13 2019

@author: sarah
"""
f_in_lg= open("2.csv","r", encoding="utf-8")
f_ep_val = open("ep-val-final.mg","w", encoding="utf-8")
f_ep = open("ep-final.mg","w", encoding="utf-8")
value="Lemma"
fam=""#famille
mot_fam=""#contient le couples mot+sa catégorie ou mot+sa famille
liste=[]#liste contenant les mots pour vérifier qu'ils n'apparaissent pas deux fois
token=[]
genre=""
nb=""
act_pas=""
l=""
val=";"
iobj=""
obj=""
cat=""#catégorie
verbe=""
import re

for line in f_in_lg:
    if val in line:
        liste_morph=re.sub(" ","",line)
        liste_morph=re.sub("§","",line)
        liste_morph=liste_morph.split("\t") #découpage au niveau des tabulations
        ep=liste_morph[11]
        verbe=liste_morph[5]
        mot_fam=liste_morph[11]
        if "+" in liste_morph[2]:
            act_pas="active"
        else:
            act_pas="passive"
        if "+" in liste_morph[3]:
            obj="lexicalized"
        else:
            obj="free"
        if "+" in liste_morph[4]:
            iobj="lexicalized"
        else:
            iobj="free"
            
        if not mot_fam in liste:
                liste.append(liste_morph[5])
                f_ep.write("class mwe"+ep.capitalize()+"\n{\n  <lemma> {\n    entry <- \""+verbe+"\";\n    cat <- mwen0Vn1;\n    filter dia = "+act_pas+";\n    filter subj = lexicalized;\n    filter passivetype= short;\n    filter obj = lexicalized;\n    filter objstruct = "+obj+";\n    coanchor ObjDetNode -> \"?\"/d;\n    coanchor ObjNode -> \"?\"/n;\n    equation ObjNode -> gen=?;\n    equation ObjNode -> num=?;\n    equation ObjNode -> modifiable=-\n  }\n}\n\n")
                f_ep_val.write("value mweLemme"+liste_morph[11].capitalize()+"\n")


#"class mwe"+ep.capitalize()+"\n{\n  <lemma> {\n    entry <- \""+verbe+"\";\n    cat <- mwen0Vn1;\n    filter dia = passive;\n    filter subj = lexicalized;\n    filter passivetype= short;\n    filter obj = lexicalized;\n    filter objstruct = lexDetLexN;\n    coanchor ObjDetNode -> \"?\"/d;\n    coanchor ObjNode -> \"?\"/n;\n    equation ObjNode -> gen=?;\n    equation ObjNode -> num=?;\n    equation ObjNode -> modifiable=-\n  }\n}"
#{
#  <lemma> {
#    entry <- "cuire";
#    cat   <- v;
#    fam   <- mwen0Vn1;
#    filter dia = passive;
#    filter passivetype = short;
#    filter obj = lexicalized;
#    filter objstruct = lexDetLexN;
#    coanchor ObjDetNode -> "les"/d;
#    coanchor ObjNode -> "carottes"/n;
#    equation ObjNode -> gen=f;
#    equation ObjNode -> num=pl;
#    equation ObjNode -> modifiable=-
#  }
#}


f_in_lg.close()
f_ep_val.close()
f_ep.close()