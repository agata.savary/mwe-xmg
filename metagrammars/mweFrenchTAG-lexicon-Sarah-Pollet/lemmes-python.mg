class LemmeJean
{
  <lemma> {
    entry <- "jean";
    cat   <- n;
    fam   <- propername
  }
}

class LemmaViteAdvspost {
  <lemma> {
	 entry <- "vite";
	 cat <- adv;
	 fam <- advSPost
  }
}

class LemmaViteAdvsante {
  <lemma> {
	 entry <- "vite";
	 cat <- adv;
	 fam <- advSAnte
  }
}

class LemmaHibiscusN0vnden1 {
  <lemma> {
	 entry <- "hibiscus";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaHileN0vnden1 {
  <lemma> {
	 entry <- "hile";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaHileNoun {
  <lemma> {
	 entry <- "hile";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaHiloireN0vnden1 {
  <lemma> {
	 entry <- "hiloire";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaHiloireNoun {
  <lemma> {
	 entry <- "hiloire";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaSouvenirN0vnden1 {
  <lemma> {
	 entry <- "souvenir";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaSouvenirN0clvden1 {
  <lemma> {
	 entry <- "souvenir";
	 cat <- v;
	 fam <- n0clVden1
  }
}

class LemmaSouvenirIlv {
  <lemma> {
	 entry <- "souvenir";
	 cat <- v;
	 fam <- ilV
  }
}

class LemmaPleurerN0vn1 {
  <lemma> {
	 entry <- "pleurer";
	 cat <- v;
	 fam <- n0Vn1
  }
}

class LemmaGrelotterN0vden1 {
  <lemma> {
	 entry <- "grelotter";
	 cat <- v;
	 fam <- n0Vden1
  }
}

class LemmaFuirN0vn1 {
  <lemma> {
	 entry <- "fuir";
	 cat <- v;
	 fam <- n0Vn1
  }
}

class LemmaFuirN0v {
  <lemma> {
	 entry <- "fuir";
	 cat <- v;
	 fam <- n0V
  }
}

class LemmaFuirN0vloc1 {
  <lemma> {
	 entry <- "fuir";
	 cat <- v;
	 fam <- n0Vloc1
  }
}

class LemmaPleuvoirN0v {
  <lemma> {
	 entry <- "pleuvoir";
	 cat <- v;
	 fam <- n0V
  }
}

class LemmaPleuvoirIlv {
  <lemma> {
	 entry <- "pleuvoir";
	 cat <- v;
	 fam <- ilV
  }
}

class LemmaSemblerN0van1 {
  <lemma> {
	 entry <- "sembler";
	 cat <- v;
	 fam <- n0Van1
  }
}

class LemmaSemblerCopule {
  <lemma> {
	 entry <- "sembler";
	 cat <- v;
	 fam <- copule
  }
}

class LemmaDormirN0v {
  <lemma> {
	 entry <- "dormir";
	 cat <- v;
	 fam <- n0V
  }
}

class LemmaHormisN0pn1 {
  <lemma> {
	 entry <- "hormis";
	 cat <- prep;
	 fam <- n0Pn1
  }
}

class LemmaHierAdvspost {
  <lemma> {
	 entry <- "hier";
	 cat <- adv;
	 fam <- advSPost
  }
}

class LemmaHierAdvsante {
  <lemma> {
	 entry <- "hier";
	 cat <- adv;
	 fam <- advSAnte
  }
}

class LemmaItouAdvspost {
  <lemma> {
	 entry <- "itou";
	 cat <- adv;
	 fam <- advSPost
  }
}

class LemmaItouAdvsante {
  <lemma> {
	 entry <- "itou";
	 cat <- adv;
	 fam <- advSAnte
  }
}

class LemmaIsolémentAdvspost {
  <lemma> {
	 entry <- "isolément";
	 cat <- adv;
	 fam <- advSPost
  }
}

class LemmaIsolémentAdvsante {
  <lemma> {
	 entry <- "isolément";
	 cat <- adv;
	 fam <- advSAnte
  }
}

class LemmaArborescentN0va {
  <lemma> {
	 entry <- "arborescent";
	 cat <- adj;
	 fam <- n0vA
  }
}

class LemmaArboretumN0vnden1 {
  <lemma> {
	 entry <- "arboretum";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaArboretumNoun {
  <lemma> {
	 entry <- "arboretum";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaArborerNoun {
  <lemma> {
	 entry <- "arborer";
	 cat <- v;
	 fam <- noun
  }
}

class LemmaArboricoleN0va {
  <lemma> {
	 entry <- "arboricole";
	 cat <- adj;
	 fam <- n0vA
  }
}

class LemmaArboricoleN0vnden1 {
  <lemma> {
	 entry <- "arboricole";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaArboricoleNoun {
  <lemma> {
	 entry <- "arboricole";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaArboriculteurN0vnden1 {
  <lemma> {
	 entry <- "arboriculteur";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaArboriculteurNoun {
  <lemma> {
	 entry <- "arboriculteur";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaArboricultureN0vnden1 {
  <lemma> {
	 entry <- "arboriculture";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaArboricultureNoun {
  <lemma> {
	 entry <- "arboriculture";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaArboriserNoun {
  <lemma> {
	 entry <- "arboriser";
	 cat <- v;
	 fam <- noun
  }
}

class LemmaArborisationN0vnden1 {
  <lemma> {
	 entry <- "arborisation";
	 cat <- n;
	 fam <- n0vNden1
  }
}

class LemmaArborisationNoun {
  <lemma> {
	 entry <- "arborisation";
	 cat <- n;
	 fam <- noun
  }
}

class LemmaArboriserN0vn1 {
  <lemma> {
	 entry <- "arboriser";
	 cat <- v;
	 fam <- n0Vn1
  }
}

class LemmaArboriserN0v {
  <lemma> {
	 entry <- "arboriser";
	 cat <- v;
	 fam <- n0V
  }
}

class LemmaPrendreN0v {
  <lemma> {
	 entry <- "prendre";
	 cat <- v;
	 fam <- n0V
  }
}

class LemmaPrendreN0vn1 {
  <lemma> {
	 entry <- "prendre";
	 cat <- v;
	 fam <- n0Vn1
  }
}

