{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Map.Strict as M
import qualified Data.Set as S

import NLP.MweXmg


defaultEnv = extractEnv
  "../../data/input/all_mweoccurs.tsv"
  "../../data/input/parseme-fr.conllu"


defaultSelCfg =
  let occNum typ = case typ of "ID" -> 10; _ -> 2
  in  defaultSortCfg
        { selectNumByType = occNum
        }


main = do
  -- Reading environment
  env <- defaultEnv

  -- Selection config
  let selCfg = defaultSelCfg

  -- Selection itself
  mweMap <- selectMWEs selCfg env
  let mweSet = S.unions $ M.elems mweMap

  -- Sentence division
  -- divideSentences env 5 defaultSplitCfg mweSet
  divideSentencesEnt env selCfg defaultSplitCfg mweSet

