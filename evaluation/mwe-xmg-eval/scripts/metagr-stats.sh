#!/bin/bash
#Producing statistics about the metagrammar

#Directory with the metagrammar files
MGR_DIR="/home/savary/Duesseldorf/XMG/Gitlab/dev"

cd $MGR_DIR
FTAG_CLASSES=`cat adjectifs.mg adverbes.mg misc.mg noms.mg verbes.mg PredArgs.mg | grep -E '^[[:space:]]*class' | grep -v 'mwe' | wc -l`
MWE_CLASSES=`cat adjectifs.mg adverbes.mg misc.mg noms.mg verbes.mg PredArgs.mg mweAdjectifsAttr.mg mweNomsLex.mg mwePredArgs.mg mweVerbesLex.mg  | grep -E '^[[:space:]]*class' | grep mwe  | wc -l`
MWE_LEMMAS=`cat lemmes-class-init.mg | grep -E '^[[:space:]]*class[[:space:]]mwe' | wc -l`

echo "FTAG_CLASSES: " $FTAG_CLASSES
echo "MWE_CLASSES: " $MWE_CLASSES
echo "Total of classes: " $[$FTAG_CLASSES+$MWE_CLASSES]
echo "MWE_LEMMAS: " $MWE_LEMMAS

#z="$(expr $MWE_CLASSES / $FTAG_CLASSES)"
#echo "Increase:  $z"   #$(( $MWE_CLASSES/$FTAG_CLASSES ))


