#/bin/bash
#Selecting only the simplified sentences from a dataset, normalising them, applying a morphological analysis and transforming into an XMG lexicon
#Parameters:
# $1 = file path with no -simplification.txt extension of the input dataset file with the following format:
#	3 lines per MWE: the components, the simplified phrase followed by the axiom to be used in parsing, the originaml sentence
#	Example:
#		mettre en examen 
#			=> les personnes [mises] [en] [examen]	=>axiom: n
#			=> - 18 mars 2002 : La fin de l' instruction est notifiée aux à les quinze personnes [mises] [en] [examen] .  (IDIOMATIC)
#		se faire 
#			=> Paris semble [se] [faire] du mouron	=>axiom: s
#			=> Ainsi , Madrid semble [se] [faire] autant de mouron sinon plus que Rabat sur la question des de les frontières .  (LITERAL/RANDOM)

#Sample call 
# ./morphoSimplified.sh ../../data/output/fourth/morphological-analysis/train

if [ $# != 1 ]; then
	echo "Usage:"
	echo "	$0 <simplified-dataset>"
#	echo "	$0 <language> <simplified-dataset>"
#	echo "		<language> = French | English | <any Unitex language>"
	echo "		<Simplified-dataset> = file path with no .txt extension of the dataset with 3 lines per MWE: the components, the simplified phrase followed by the axiom to be used in parsing, the original sentence"
	exit -1
fi


#The UNitex directory
#UNITEX_DIR=~/Narzedzia/Unitex/Unitex3.0/App
UNITEX_DIR=~/Unitex-GramLab-3.1/
UNITEX_WORKSPACE=~/workspace/Unitex-GramLab/Unitex/
XMG_DIR=../../../dev/
echo $UNITEX_DIR
LANG=French
FILE1=$1-simplification.txt
FILE2=$1-simplified.txt
FILE3=$1-simplified.snt
FILE_DIR=$1-simplified_snt
FILE_NO_PATH=`echo $1 | sed "s/^.*\///g"`


#Extracting the lines with the simplified phrases
#cat $FILE1 | gawk '{if (NR%3 == 2) print}' | cut -d'>' -f 2 | cut -f1 | sed 's/^ //' | sed 's/\[//g' | sed 's/\]//g' > $FILE2
cat $FILE1 | grep '=>'| cut -d'>' -f 2 | cut -f1 | sed 's/^ //' | sed 's/\[//g' | sed 's/\]//g' > $FILE2
rm -rf $FILE_DIR
mkdir $FILE_DIR
echo "Extraction of lines done"

#Tokenization
CUR_DIR=$PWD
#cd $UNITEX_DIR
cp $FILE2 $FILE3
$UNITEX_DIR/App/UnitexToolLogger Tokenize $FILE3 -a $UNITEX_WORKSPACE/$LANG/Alphabet.txt -w -qutf8-no-bom
ls -l $FILE_DIR
#echo "Tokenization done"

#Morphological analysis
$UNITEX_DIR/App/UnitexToolLogger Dico -t $FILE3 -a $UNITEX_WORKSPACE/$LANG/Alphabet.txt $UNITEX_DIR/$LANG/Dela/dela-fr-public.bin -qutf8-no-bom
dos2unix ../../data/output/fourth/$FILE_NO_PATH-simplified_snt/dlf
ls -l $FILE_DIR

#For some reason the analyses fin,.A+z1:ms and fin,.N+z1:ms block parsing of "mettre fin à"
cat ../../data/output/fourth/$FILE_NO_PATH-simplified_snt/dlf | grep -v "fin,.A" | grep -v "fin,.N.*:ms"  > ../../data/output/fourth/$FILE_NO_PATH-simplified_snt/dlf-filtered

#Conversion into an XMG lexicon
gawk -v LEM_CLASS_FILE="../../../dev/lemmes-class-$FILE_NO_PATH.mg" -v LEM_VALUE_FILE="../../../dev/lemmes-value-$FILE_NO_PATH.mg" -v MORPHO_CLASS_FILE="../../../dev/morphs-class-$FILE_NO_PATH.mg" -v MORPHO_VALUE_FILE="../../../dev/morphs-value-$FILE_NO_PATH.mg" -f dlf2morphs.gawk ../../data/output/fourth/$FILE_NO_PATH-simplified_snt/dlf-filtered

#Merging with the previous lexicon
cat $XMG_DIR/morphs-class-init.mg $XMG_DIR/morphs-class-$FILE_NO_PATH.mg $XMG_DIR/morphs-value-init.mg $XMG_DIR/morphs-value-$FILE_NO_PATH.mg  > $XMG_DIR/morphs-$FILE_NO_PATH.mg 
cat $XMG_DIR/lemmes-class-init.mg $XMG_DIR/lemmes-class-$FILE_NO_PATH.mg $XMG_DIR/lemmes-value-init.mg $XMG_DIR/lemmes-value-$FILE_NO_PATH.mg  > $XMG_DIR/lemmes-$FILE_NO_PATH.mg 

#Compiling to TAG
cd $XMG_DIR
xmg compile lex lemmes-train.mg --force #Compiling lemmas
xmg compile mph morphs-train.mg --force #COmpiling inflected forms

#Testing the compilation
tulipa -cyktag -v -g mweValuation.xml -l lemmes-train.xml -m morphs-train.xml -a s -s "les pilotes firent appel"



