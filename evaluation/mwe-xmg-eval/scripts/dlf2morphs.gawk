#Transforming a dlf file stemming from morphological analysis by Unitex to .mg format in XMG
#The new entries are attached to those previously contained in the files indicated by command line variables 
# Parameter:
#    ARGV[1] = file path with the dlf data
#	sample dlf entries:
# 	des,du.PREPDET+z1:mp:fp
#	des,un.DET+z1:mp:fp
#	charpentiers,charpentier.A+z2:mp
#	charpentiers,charpentier.N+Profession:mp
#	charpentiers,charpentier.N+z2:mp
# Variables in command line:
#    LEM_CLASS_FILE = path to the output .mg file with already existing lemma classes 
#    LEM_VALUE_FILE = path to the output .mg file with already existing lemma valuation 
#    MORPHO_CLASS_FILE = path to the output .mg file with already existing classes for inflected forms
#    MORPHO_VALUE_FILE = path to the output .mg file with already existing valuation for inflected forms
#Sample call 
# gawk -v LEM_CLASS_FILE="../../../dev/lemmes-class-eval.mg" -v LEM_VALUE_FILE="../../../dev/lemmes-value-eval.mg" -v MORPHO_CLASS_FILE="../../../dev/morphs-class-eval.mg" -v MORPHO_VALUE_FILE="../../../dev/morphs-value-eval.mg" -f dlf2morphs.gawk ../../data/output/fourth/morphological-analysis/train-simplified_snt/dlf
# less ../../../dev/morphs-class-eval.mg


####################################
# Print the current lemma
function print_lemma() {
	LCLASS_NAME = "Lemma" toupper(substr(LEM,1,1)) substr(LEM,2,length(LEM)) CAT
	print "class " LCLASS_NAME " {" >> LEM_CLASS_FILE
	print "  <lemma> {"  >> LEM_CLASS_FILE
	print "    entry <- \"" LEM "\";" >> LEM_CLASS_FILE
	print "    cat <- " CATS[CAT] ";" >> LEM_CLASS_FILE
 	switch (CATS[CAT]) {
    		case "n":
        		printf "    fam <- noun\n"  >> LEM_CLASS_FILE
        		break
    		case "adj":
        		printf "    fam <- mweAdjAttrRight\n"  >> LEM_CLASS_FILE
        		break
    		case "v":
        		printf "    fam <- n0V\n"  >> LEM_CLASS_FILE
        		break
    		case "p":
        		printf "    fam <- s0Pn1\n"  >> LEM_CLASS_FILE
        		break
    		default:
			printf "    fam <- \n"  >> LEM_CLASS_FILE
        		break
    	}
	printf "  }\n"  >> LEM_CLASS_FILE
	printf "}\n\n"  >> LEM_CLASS_FILE
	LVALUATION[LCLASS_NAME] = 1
}

####################################
# Print the beginning of a morphological entry, with the inflected form, the lemma and teh category
function print_morph_beg() {
	print "{" >> MORPHO_CLASS_FILE
	print "  <morpho> {" >> MORPHO_CLASS_FILE
	print "    morph <- \"" INFL "\";" >> MORPHO_CLASS_FILE
	print "    lemma <- \"" LEM "\";" >> MORPHO_CLASS_FILE
	#print CATS[CAT]
	printf "    cat   <- %s", CATS[CAT] >> MORPHO_CLASS_FILE
	if (CATS[CAT] == "n")
		printf ";\n    det   <- -" >> MORPHO_CLASS_FILE
}

####################################
# Print the current morphological feature and its value
function print_feature (VAL) {
	if (VALS[VAL] == "ppart")
		PPART = 1
	if (PPART == 1 ) {
		if ((VALS[VAL] == "sg") || (VALS[VAL] == "pl"))
			printf ";\n    pp-num <- %s\n", VALS[VAL] >> MORPHO_CLASS_FILE
		else if ((VALS[VAL] == "m") || (VALS[VAL] == "f"))
			printf ";\n    pp-gen <- %s", VALS[VAL] >> MORPHO_CLASS_FILE
	}
	else if (VAL == "F") {
		printf ";\n    mode <- ind" >> MORPHO_CLASS_FILE
		printf ";\n    tense <- future" >> MORPHO_CLASS_FILE #Future
	}
	else if (VAL == "I") {
		printf ";\n    mode <- ind" >> MORPHO_CLASS_FILE
		printf ";\n    tense <- past" >> MORPHO_CLASS_FILE #Imperfect
	}
	else if (VAL == "J") {
		printf ";\n    mode <- ind" >> MORPHO_CLASS_FILE
		printf ";\n    tense <- spast" >> MORPHO_CLASS_FILE #Simple past
	}
	else
		printf ";\n    %s <- %s", FEATS[VAL],  VALS[VAL] >> MORPHO_CLASS_FILE
}

####################################
# Print the end of a morphological entry
function print_morph_end() {
	printf "\n   }\n" >> MORPHO_CLASS_FILE
	print "}\n" >> MORPHO_CLASS_FILE
}


####################################
BEGIN {

MVALUATION[1] = ""

#LEM_FILE = ARGV[2]
#MORPHO_FILE = ARGV[3]

#Morphological categories (indexes are DELAF labels), values are XMG labels
CATS["A"]="adj"; CATS["ADV"]="adv"; CATS["DET"]="d"; CATS["N"]="n"; CATS["PREP"]="p"; CATS["PRO"]="cl"; CATS["V"]="v";
#print "CATS[N]=" CATS["N"]

#Morphological values
#add tense <- past for I; and tense <- fut for F 
VALS["m"]="m"; VALS["f"]="f"; VALS["s"]="sg"; VALS["p"]="pl"; VALS["1"]="1"; VALS["2"]="2"; VALS["3"]="3"; VALS["P"]="ind"; VALS["Y"]="imp"; VALS["S"]="subj"; VALS["W"]="inf"; VALS["K"]="ppart"; VALS["I"]="ind"; VALS["C"]="cond"; VALS["G"]="ger"; VALS["J"]="ind"; VALS["F"]="ind"; #I, J and F should be enhanced

#XMG categories for Unitex values
FEATS["m"]="gen"; FEATS["f"]="gen"; FEATS["s"]="num"; FEATS["p"]="num"; FEATS["1"]="pers"; FEATS["2"]="pers"; FEATS["3"]="pers"; FEATS["P"]="mode"; FEATS["Y"]="mode"; FEATS["S"]="mode"; FEATS["W"]="mode"; FEATS["K"]="mode"; FEATS["I"]="mode"; FEATS["C"]="mode"; FEATS["G"]="mode" 

#print "FEATS[A] = " FEATS["A"] 
print "LEM_CLASS_FILE = " LEM_CLASS_FILE
print "LEM_VALUE_FILE = " LEM_VALUE_FILE
print "" > LEM_CLASS_FILE
print "" > LEM_VALUE_FILE
print "MORPHO_CLASS_FILE = " MORPHO_CLASS_FILE
print "MORPHO_VALUE_FILE = " MORPHO_VALUE_FILE
print "" > MORPHO_CLASS_FILE
print "" > MORPHO_VALUE_FILE

}

####################################
{
#New lemma
LEM_COUNT++

#Get the inflected form
split($0, LINE1, ",")
INFL = LINE1[1]

#Get the lemma
split(LINE1[2], LINE2, ".")
LEM = LINE2[1]
if (LEM == "")
	LEM = INFL

#Get the category
split(LINE2[2], LINE3, ":")
split(LINE3[1], LINE4, "+")
CAT = LINE4[1]
#print "CAT=" LINE4[1]
#Don't process pronouns, adverbs, determiners, verbs and conjunctions (they have a special set of features in FrenchTAG)
if ((CAT == "PRO") || (CAT == "DET") || (CAT == "PREPDET") || (CAT == "ADV") ||  (CAT == "V") || (CAT == "PREP") || (CAT == "CONJC")) 
	next

#Print the lemma
print_lemma()

#Print an entry with no morphological features
if (LINE3[2] == "") {
	print "class " INFL CAT >> MORPHO_CLASS_FILE
	print_morph_beg()
	print_morph_end()
	MVALUATION[NR] = INFL CAT
}


i = 2
while (LINE3[i] != "") {
	#print LINE3[i]

	#Save the valuation name
	if (length(LINE3) < 3)
		MVALUATION[NR] = INFL CAT LINE3[i]
	else
		MVALUATION[NR] = INFL CAT LINE3[i] i-1

	#Print the entry (with a rank, if more than one interpretation)
	if (length(LINE3) > 2)
		print "class " INFL CAT LINE3[i] i-1 >> MORPHO_CLASS_FILE
	else
		print "class " INFL CAT LINE3[i] >> MORPHO_CLASS_FILE	
	print_morph_beg()
	j = 1
	while (j <= length(LINE3[i])) {
		VAL = substr(LINE3[i], j, 1)  #Get the current (1-character) feature
		print_feature(VAL)	
		j++
	}
	print_morph_end()
	PPART = 0
	i++
}
LEM_COUNT++
}

END {
#print "NR = " NR
i = 1
#print "DONE2"
while (i <= NR) {
	if (MVALUATION[i] != "") 
		print "value " MVALUATION[i] >> MORPHO_VALUE_FILE
	i++
}
for (c in LVALUATION)
	print "value " c >> LEM_VALUE_FILE
}
