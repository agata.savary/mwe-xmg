{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}


-- | Dependency configuration extraction module.


module NLP.MweXmg.DepConf
  ( DepConf
  , DepElem(..)
  , Dep(..)
  , extractDepConf
  ) where


import           Control.Monad (guard)

import qualified Data.List as L
import qualified Data.Text as T
import qualified Data.Set as S

import qualified NLP.MweXmg.Conllu as C


-----------------------------------------------------------------------
-- Dependency Configuration
-----------------------------------------------------------------------


-- | A dependency configuration. Our goal is to find MWEs whose occurrences
-- exhibit high variation in terms of such configurations.
type DepConf = [DepElem]


-- | Dependency relation.
data Dep = Dep
  { depType :: T.Text
    -- ^ Relation type (see `C.depTyp`)
-- NOTE: we ignore `depBase` so as to be albe to compare syntactic
-- configurations of different MWEs.
--   , depBase :: Maybe T.Text
--     -- ^ The base form of the target node. We use `Nothing` when the target node
--     -- is not a component of the MWE.
  } deriving (Show, Eq, Ord)


-- | An element of the dependency configuration.
data DepElem = DepElem
--   { base :: T.Text
--     -- ^ The base form of the element (see `C.base`)
  { pos :: T.Text
    -- ^ The part-of-speech of the element (see `C.pos`)
  , children :: S.Set Dep
    -- ^ Information about children
  } deriving (Show, Eq, Ord)


-- | Extract the dependency configuration corresponding to the MWE occurrence
-- marked in the given sentence (see `MweXmg.markOcc`).
extractDepConf
  :: Int
     -- ^ Maximum number of children dependencies to account for
  -> [(C.Tok, Bool)]
  -> DepConf
extractDepConf maxDepNum xs =
  let mweToks = map fst $ filter ((==True) . snd) xs
  in  map (extractDepElem maxDepNum xs) mweToks


-- | Extract a single dependency element.
extractDepElem
  :: Int             -- ^ Maximum number of children dependencies to account for
  -> [(C.Tok, Bool)] -- ^ All the tokens
  -> C.Tok           -- ^ The token to process
  -> DepElem
extractDepElem maxDepNum toks tok = DepElem
  -- { base = C.base tok
  { pos = C.pos tok
  , children = S.fromList . take maxDepNum . nub $ do
--       -- we exclude verbs when computing variation
--       -- NOTE: too arbitrary a choice?
--       guard $ C.pos tok /= "VERB"
      (child, isMWE) <- toks
      guard $ C.parent child == Just (C.RegID (C.tokID tok))
      return $ Dep
        { depType = C.depTyp child
--         , depBase =
--             if isMWE
--             then Just (C.base child)
--             else Nothing
        }
  }
  where
    nub = S.toList . S.fromList
