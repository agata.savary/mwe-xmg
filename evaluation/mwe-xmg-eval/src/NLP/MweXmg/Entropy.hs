module NLP.MweXmg.Entropy
  ( count
  , entropy
  , flatten
  ) where


import           System.Random   (randomRIO)

import qualified Data.Map.Strict as M


-----------------------------------------------------------------------
-- Entropy
---------------------------------------------------------------------


-- | Entropy of a given probability distribution. Elements not present in the
-- map are assumed to have probability 0. The map is not assumed to be
-- normalized (i.e., the sum of weights can be different than 1), but weights
-- must be positive.
entropy :: (Ord a) => M.Map a Double -> Double
entropy m = -sum
  [ prob * log prob
  | (x, v) <- M.toList m
  , let prob = v / len ]
  where
    len = sum . map snd $ M.toList m


-- | Normalized entropy.
normalizedEntropy :: (Ord a) => M.Map a Double -> Double
normalizedEntropy m
  | M.size m == 1 = 1.0
  | otherwise =
      entropy m /
      log (fromIntegral $ M.size m)


-----------------------------------------------------------------------
-- Counting
---------------------------------------------------------------------


-- | Count the elements in the list.
count :: Ord a => [a] -> M.Map a Int
count
  = M.fromListWith (+)
  . map (\x -> (x, 1))


-----------------------------------------------------------------------
-- Flattening
---------------------------------------------------------------------


-- | An inverse of `count`, the function transforms a set of elements (with the
-- corresponding counts) to a list such that:
--
--   * The output list contains the same elements as the map (taking
--     the counts into account)
--   * Equal elements (from the `Ord` perspective) are balanced out
--     over the entire list
flatten :: Ord a => M.Map a Int -> IO [a]
flatten m
  | M.null m = return []
  | otherwise = do
      x <- pick m
      (x:) <$> flatten (remove x m)


-- | Randomly pick an element from the map, with the probability of taking an
-- element proportional to its count.  The map must be non-empty!
pick :: Ord a => M.Map a Int -> IO a
pick m = do
  let freqs = M.toList m
  idx <- randomRIO (0, sum (map snd freqs) - 1)
  return $ indexFreqs idx freqs
  where
    -- Borrowed from:
    -- https://jaspervdj.be/posts/2013-11-21-random-element-frequency-list.html
    indexFreqs :: Int -> [(a, Int)] -> a
    indexFreqs _   [] = error "pick: empty input map"
    indexFreqs idx ((x, f) : xs)
      | idx < f     = x
      | otherwise   = indexFreqs (idx - f) xs


-- | Remove (once!) the element from the map. If the element is not present in
-- the map, the map is not changed.
remove :: Ord a => a -> M.Map a Int -> M.Map a Int
remove x =
  M.update upd x
  where
    upd k
      | k > 1 = Just (k - 1)
      | otherwise = Nothing
