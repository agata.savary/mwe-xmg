module NLP.MweXmg.Align
  ( align
  ) where


import qualified Data.Char as C
import qualified Data.Text as T


-- | Align two lists of tokens.
align
  :: (a -> T.Text)
     -- ^ The orthographic form of `a`
  -> (b -> T.Text)
     -- ^ The orthographic form of `b`
  -> [a]
  -> [b]
  -> [([a], [b])]
align _ _ [] [] = []
align _ _ [] _  = error "align: null xs, not null ys"
align _ _ _  [] = error "align: not null xs, null ys"
align orthX orthY xs ys =
    let (x, y) = match orthX orthY xs ys
        rest   = align orthX orthY (drop (length x) xs) (drop (length y) ys)
    in  (x, y) : rest


-- | Find the shortest, length-matching prefixes in the two input lists.
match
  :: (a -> T.Text)
     -- ^ The orthographic form of `a`
  -> (b -> T.Text)
     -- ^ The orthographic form of `b`
  -> [a]
  -> [b]
  -> ([a], [b])
match orthX orthY  xs' ys' =
    doIt 0 xs' 0 ys'
  where
    doIt i (x:xs) j (y:ys)
        | n == m    = ([x], [y])
        | n <  m    = addL x $ doIt n xs j (y:ys)
        | otherwise = addR y $ doIt i (x:xs) m ys
      where
        n = i + size orthX x
        m = j + size orthY y
    doIt _ [] _ _   = error "match: the first argument is null"
    doIt _ _  _ []  = error "match: the second argument is null"
    size orth w = T.length . T.filter isImportantChar $ orth w
    addL x (xs, ys) = (x:xs, ys)
    addR y (xs, ys) = (xs, y:ys)


-- | Tells whether the given character is important from the comparison point of view.
isImportantChar :: Char -> Bool
isImportantChar x = not $ or
  [ C.isSpace x
  , x == '_'
  ]
