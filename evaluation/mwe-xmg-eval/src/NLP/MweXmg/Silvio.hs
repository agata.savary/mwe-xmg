{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}


-- | Extracting MWE occurrences based on the Silvio's script (see
-- http://aclweb.org/anthology/W/W17/W17-7610.pdf).


module NLP.MweXmg.Silvio
  ( Occ(..)
  , showOcc
  , getRawSent
  , parseOccFile
  ) where


import           Control.Monad (forM)
import           Data.Maybe (catMaybes)

import qualified Data.List as L
import qualified Data.Char as C
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Set as S


-- | Information about an occurrence of a MWE, as provided in the output data
-- from Silvio's script (see http://aclweb.org/anthology/W/W17/W17-7610.pdf).
data Occ = Occ
  { mwe :: T.Text
    -- ^ The MWE in its "base" form
  , pos :: [T.Text]
    -- ^ POS tag, which is actually a list of POS tags assigned to the
    -- individual elements of the MWE
  , cat :: T.Text
    -- ^ Category of the MWE (ID for idiom, LVC for light verb construction,
    -- etc.)
  , idiomatic :: Bool
    -- ^ Is it an idiomatic occurrence? If `False`, it might either be a literal
    -- occurrence or an accidental co-occurrence of the component words.
  , annoMethods :: [T.Text]
    -- ^ With which annotation methods this MWE is captured?
  , sentence :: [(T.Text, Bool)]
    -- ^ Sentence in which the occurrence is found. To each word information
    -- whether it is a part of the MWE occurrence or not is marked.
  , source :: T.Text
    -- ^ Information about the origin of the occurrence.
  } deriving (Show, Eq, Ord)


-- | Retrieve the corresponding raw sentence.
getRawSent :: Occ -> T.Text
getRawSent = T.unwords . map fst . sentence


-- | A convenient one-line representation of the occurrence.
showOcc :: Occ -> T.Text
showOcc Occ{..} =
  T.unwords
  [ sentTxt
  , if idiomatic
    then " (IDIOMATIC)"
    else " (LITERAL/RANDOM)"
  ]
  where
    sentTxt = T.unwords $ map showTok sentence
    showTok (tok, isMWE) =
      if isMWE
      then T.concat ["[", tok, "]"]
      else tok


-- | Return the set of MWE occurrences stored in the given file.
parseOccFile :: FilePath -> IO (S.Set Occ)
parseOccFile filePath = do
  contents <- T.readFile filePath
  -- -- note that, below, we drop the header line
  -- let xs = map parseOcc . drop 1 $ T.lines contents
  let xs = map parseOcc $ T.lines contents
  occs <- fmap catMaybes . forM xs $ \case
    Left err -> Nothing <$ T.putStrLn err
    Right occ -> Just <$> return occ
  return (S.fromList occs)


-- | Parse a single occurrence. Return `Left err` if it is not possible to parse
parseOcc :: T.Text -> Either T.Text Occ
parseOcc txt = case T.split (=='\t') txt of
  [_mwe, _pos, _cat, _idio, _anno, _sent, _sour] -> do
    -- sent <- preprocessSent <$> parseSent _sent
    sent <- parseSent _sent
    return $ Occ
      { mwe = _mwe
      , pos = T.split (==' ') _pos
      , cat = _cat
      , idiomatic = _idio == "IDIOMAT"
      , annoMethods = T.split (==',') _anno
      , sentence = sent
      , source = _sour
      }
  _ ->
    Left $ "Cannot parse: " `T.append` txt


-- | Parse the given sentence: tokenize and identify which tokens form the MWE occurrence.
parseSent :: T.Text -> Either T.Text [(T.Text, Bool)]
parseSent txt = do
  let toks = T.split (==' ') txt
  mapM parseTok toks
  where
    parseTok tok
      | T.null tok =
          Left "Impossible happened: empty token"
      | first tok == '[' && last tok == ']' = do
          let tok' = T.reverse . T.drop 1 . T.reverse . T.drop 1 $ tok
          return (tok', True)
      | otherwise =
          return (tok, False)
    first = T.head
    last = first . T.reverse


-- -- | Preprocess the sentence. In particular, join adjacent numbers into single
-- -- tokens.
-- preprocessSent :: [(T.Text, Bool)] -> [(T.Text, Bool)]
-- preprocessSent
--   = map join
--   . L.groupBy areNumbers
--   where
--     join toks =
--       let (xs, bs) = unzip toks
--       in  (T.unwords xs, or bs)
--     areNumbers x y = isNumber (fst x) && isNumber (fst y)
--     isNumber x = T.all C.isNumber x
