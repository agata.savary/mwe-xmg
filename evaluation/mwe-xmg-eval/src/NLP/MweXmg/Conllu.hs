{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}


-- | Support for the Conllu format, in which PARSEME shared task data is
-- provided.


module NLP.MweXmg.Conllu
  ( ID
  , ParID (..)
  , Tok(..)
  , Sent
  , parseConllu
  , parseConlluFile
  , getRawSent
  ) where


import qualified Data.List as L
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Set as S
import qualified Data.Map.Strict as M


-- | A sentence as stored in the Conllu format. Could be a map, but we don't
-- want to change the order of tokens (and IDs are simply integers).
type Sent = [Tok]


-- | A token identifier.
type ID = [Int]


-- | An attribute name.
type Att = T.Text


-- | An attribute value.
type Val = T.Text


data ParID
  = Root      -- ^ Dummy root node
  | RegID ID  -- ^ Regular node
  deriving (Show, Eq, Ord)


-- | A token (word) as stored in the Conllu format.
-- TODO: The two last columns seem not to be used in PARSEME.
data Tok = Tok
  { tokID :: ID
    -- ^ ID of the token
  , orth :: T.Text
    -- ^ The orthographic form
  , base :: T.Text
    -- ^ The base form
  , pos  :: T.Text
    -- ^ Part-of-speech (e.g. C -- conjunction, CL -- clitique)
  , subPos :: T.Text
    -- ^ Sub part-of-speech (e.g. CS -- subordinate conjunction, CLS -- clitique sujet)
  , atts  :: M.Map Att Val
    -- ^ Additional attribute-value map
  , parent :: Maybe ParID
    -- ^ ID of the parent (governor) token; `Nothing` can occurr for discarded
    -- words (which do not make a part of the dependency tree)
  , depTyp :: T.Text
    -- ^ Relation type which links the token/node with its parent
  } deriving (Show, Eq, Ord)


-- | Parse the *.conllu file.
parseConlluFile :: FilePath -> IO (S.Set Sent)
parseConlluFile path = do
  res <- parseConllu <$> T.readFile path
  case res of
    Left err -> do
      fail $ "[ERROR]: " ++ T.unpack err
    Right sentSet -> return sentSet


-- | Parse the conllu sentence.
parseConllu :: T.Text -> Either T.Text (S.Set Sent)
parseConllu = fmap S.fromList . mapM parseSent . getSentences


-- | Filter out comments and divide the conllu file into sentences.
getSentences :: T.Text -> [[T.Text]]
getSentences
  = filter (not . L.null)
  . breakOn T.null
  . filter (not . ("#" `T.isPrefixOf`))
  . map T.strip
  . T.lines


-- | Parse the conllu sentence.
parseSent :: [T.Text] -> Either T.Text Sent
parseSent = mapM parseToken


-- | Parse the token line.
parseToken :: T.Text -> Either T.Text Tok
parseToken txt = case T.split (=='\t') txt of
  [_id, _orth, _base, _pos, _subPos, _attsRaw, _parentRaw, _depTyp, _, _] -> do
    _atts <- parseAtts _attsRaw
    _tokID <- parseID _id
    _parent <- case _parentRaw of
      "_" -> return $ Nothing
      "0" -> return $ Just Root
      _   -> Just .  RegID <$> parseID _parentRaw
    return $ Tok
      { tokID = _tokID
      , orth = _orth
      , base = _base
      , pos = _pos
      , subPos = _subPos
      , atts = _atts
      , parent = _parent
      , depTyp = _depTyp
      }
  _ ->
    Left $ "Cannot parse token: " `T.append` txt


-- | Parse the attribute/value map.
parseAtts :: T.Text -> Either T.Text (M.Map Att Val)
parseAtts "_" = Right M.empty
parseAtts txt = do
  let xs = T.split (=='|') txt
  M.fromList <$> mapM parseAttVal xs
  where
    parseAttVal attVal = case T.split (=='=') attVal of
      [att, val] -> return (att, val)
      _ -> Left $ "Cannot parse attrubute/value pair: " `T.append` attVal


-- | Parse the parent ID.
parseID :: T.Text -> Either T.Text ID
parseID txt = do
  let xs = T.split (=='-') txt
  mapM parseElem xs
  where
    parseElem elem =
      case reads (T.unpack elem) of
        [(i, "")] -> return i
        _ -> Left $ "Cannot parse token ID: " `T.append` txt


-- -- | Parse the parent ID.
-- parseID :: T.Text -> Either T.Text ID
-- parseID = return . id


-------------------
-- Sentence
-------------------


-- | Retrieve the corresponding raw sentence.
getRawSent :: Sent -> T.Text
getRawSent =
  T.unwords . simplify
  where
    simplify = map orth
--     simplify
--       = concatMap
--       $ T.split (=='_')
--       . orth


-------------------
-- Utils
-------------------


-- | Break on all the list elements which satisfy the given predicate.
-- In addition, such elements are removed from the list.
breakOn :: (a -> Bool) -> [a] -> [[a]]
breakOn p lst =
  case L.break p lst of
    ([], []) -> []
    ([], ys) -> [tail ys]
    (xs, ys) -> xs : breakOn p (tail ys)
