{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DeriveFunctor #-}


module NLP.MweXmg
  (

  -- * MWE sorting/selection
    SortCfg(..)
  , defaultSortCfg
  , sortMWEs
  , selectMWEs
  , mweSentences
  , mweConfigs
  , mwesConfigs

  -- * Data splitting
  , Split(..)
  , defaultSplitCfg
  , split
  , divideSentences
  , divideSentencesEnt
  , pickSentencesByClasses

  -- * File reading
  , Env(..)
  , extractEnv
  , extractConlluSentences
  , extractSilvioOccurrences

  -- * Statistics
  , Stats(..)
  , totalOccNum
  , getStats

  -- * Utils
  , checkPresense
  ) where


import Control.Monad (forM, forM_, unless, when, guard)
import qualified Control.Monad.State.Strict as ST

import           Data.Maybe (catMaybes)
import           Data.Ord (comparing)
import qualified Data.List as L
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Set as S
import qualified Data.Map.Strict as M

import qualified System.Random as R
import qualified System.Random.Shuffle as RS

import qualified NLP.MweXmg.Silvio as I
import qualified NLP.MweXmg.Conllu as C
import qualified NLP.MweXmg.Align as A
import qualified NLP.MweXmg.DepConf as Dep
import qualified NLP.MweXmg.Entropy as Ent


-----------------------------------------------------------------------
-- MWE selection
-----------------------------------------------------------------------


-- | Base form
type Base = T.Text


-- | MWE type
type Type = T.Text


-- | MWE-related statistics
data Stats = Stats
  { idiomOccNum :: Int
    -- ^ Number of idiomatic occurrences of the MWE
  , nonIdiomOccNum :: Int
    -- ^ Number of non-idiomatic occurrences of the MWE
  , depEntropy :: Double
    -- ^ Entropy of the MWE's dependency configurations (limited to idiomatic
    -- occurrences only)
  } deriving (Show, Eq, Ord)


-- | The total number of occurrences.
totalOccNum :: Stats -> Int
totalOccNum Stats{..} = idiomOccNum + nonIdiomOccNum


-- | Calculate stats of the given set of occurrence.
calcStats
  :: Int
     -- ^ Maximum number of children dependencies parameter
  -> M.Map T.Text C.Sent
     -- ^ The set of Conllu sentences, to which the occurrences should correspond
  -> S.Set I.Occ
     -- ^ The occurrences for which the stats are to be computed
  -> IO Stats
calcStats maxDepNum sentMap occSet = do
  depConfs <- fmap catMaybes . forM (S.toList occSet) $ \occ -> do
    occSent sentMap occ >>= \case
      Nothing -> return Nothing
      Just sent -> do
        let marked = markOcc occ sent
            depCfg = Dep.extractDepConf maxDepNum marked
        return $ Just (depCfg, I.idiomatic occ)
  let isIdiom = (==True) . snd
  return $ Stats
    { idiomOccNum = length $ filter isIdiom depConfs
    , nonIdiomOccNum = length $ filter (not . isIdiom) depConfs
    , depEntropy = Ent.entropy
                . fmap fromIntegral
                . Ent.count
                . map (\(dep, _) -> dep)
                -- JW 27/02/2018: due to annotation errors, many idiomatic
                -- occurrences are marked as literal. Therefore it's better to
                -- take them into account when computing entropy as well.
                -- . filter isIdiom
                $ depConfs
    }


-- | Group the MWE occurrence by their base forms.
groupMWEs :: S.Set I.Occ -> M.Map Base (S.Set I.Occ)
groupMWEs
  = M.fromListWith S.union
  . map (\occ -> (I.mwe occ, S.singleton occ))
  . S.toList


-- | Check if every sentence present in Silvio's output is also present in the
-- conllu file.
getStats :: Int -> Env -> IO (M.Map Base Stats)
getStats maxDepNum env = fmap M.fromList $ do
  forM (M.toList . groupMWEs $ occSet env) $
    \(base, occSet) -> do
      stats <- calcStats maxDepNum (sentMap env) occSet
      return (base, stats)


-----------------------------------------------------------------------
-- MWE selection
-----------------------------------------------------------------------


-- | MWE sorting/selection configuration
data SortCfg = SortCfg
  { minTotalOcc :: Int
    -- ^ The minimal number of occurrences required to consider the MWE
  , minIdiomOcc :: Int
    -- ^ The minimal number of idiomatic occurrences
  , minIdiomRate :: Double
    -- ^ The minimal idiomaticity rate (between 0.0 and 1.0)
  , maxIdiomRate :: Double
    -- ^ The maximal idiomaticity rate (between 0.0 and 1.0)
  , roundEntropy :: Int
    -- ^ To the given number of decimal places
  , selectNumByType :: Type -> Int
    -- ^ Number of MWEs to select by type
  , maxDepNum :: Int
    -- ^ The maximal number of children dependencies to take when
    -- computing dependency configurations
  , retryNum :: Int
    -- ^ How many retries to find a balanced subset
  , targetSentNum :: Int
    -- ^ The number of sentences to sentences
  }


-- | A default MWE selection config.
defaultSortCfg = SortCfg
  { minTotalOcc = 5
  , minIdiomOcc = 3
  , minIdiomRate = 0.1
  , maxIdiomRate = 1.0
  , roundEntropy = 2
  , selectNumByType = const 10
  , maxDepNum = 2
  , retryNum = 2500
  , targetSentNum = 50
  }


-- | Select MWEs w.r.t. to the given parameters. The resulting list is sorted
-- from the most to the least relevant MWEs.
sortMWEs
  :: SortCfg  -- ^ Selection parameters
  -> M.Map Base Stats -- ^ MWE->Stats map
  -> [(Base, Stats)]
sortMWEs SortCfg{..}
  = reverse
  . L.sortBy (comparing $ cmpKey . snd)
  . filter (\(_, stats) -> totalOccNum stats >= minTotalOcc)
  . filter (\(_, stats) -> idiomOccNum stats >= minIdiomOcc)
  . filter (\(_, stats) -> idiomOccNum stats ./. totalOccNum stats >= minIdiomRate)
  . filter (\(_, stats) -> idiomOccNum stats ./. totalOccNum stats <= maxIdiomRate)
  . M.toList
  where
    (./.) x y = fromIntegral x / fromIntegral y
    roundTo n f = (fromInteger $ round $ f * (10^n)) / (10.0^^n)
    -- we ignore entropy in the end...
    cmpKey stats = totalOccNum stats
--     cmpKey stats =
--       ( roundTo roundEntropy . depEntropy $ stats
--       , totalOccNum stats )


-- | Select MWEs w.r.t. to the given parameters. The resulting map groups MWEs
-- by type.
selectMWEs
  :: SortCfg  -- ^ Selection parameters
  -> Env
  -> IO (M.Map Type (S.Set Base))
selectMWEs srtCfg env = do
  statMap <- getStats (maxDepNum srtCfg) env
  flip ST.execStateT M.empty . forM_ (sortMWEs srtCfg statMap) $
    \(mwe, stats) -> do
      let nub = S.toList . S.fromList
      cats <- ST.liftIO $
        nub . map I.cat . S.toList
        <$> mweOccurrences env mwe
      case cats of
        [] -> return ()
        cat : _ -> do
          num <- ST.gets (maybe 0 S.size . M.lookup cat)
          when (num < selectNumByType srtCfg cat) $ do
            let addMWE (Just mweSet) = Just $ S.insert mwe mweSet
                addMWE Nothing = Just $ S.singleton mwe
            ST.modify $ M.alter addMWE cat
            ST.liftIO $ do
              T.putStr mwe
              T.putStr " => "
              putStr $ "entropy=" ++ show (depEntropy stats)
              T.putStr ", "
              putStr $ "idiomOccNum=" ++ show (idiomOccNum stats)
              T.putStr ", "
              putStr $ "nonIdiomOccNum=" ++ show (nonIdiomOccNum stats)
              T.putStr ", "
              putStr $ "type=" ++ show (T.intercalate "|" cats)
              putStrLn ""


-- | Extract and print the sentences with the occurrences of the given MWE.
mweSentences
  :: Env
  -> Base
  -> IO ()
mweSentences env mweBase = do
  forM_ (occSet env) $ \occ -> do
    when (I.mwe occ == mweBase) $ do
      occSent (sentMap env) occ >>= \case
        Nothing -> return ()
        Just _ -> T.putStrLn $ I.showOcc occ


-- | Extract and print the sentences with the occurrences of the given MWE.
mweConfigs
  :: Int -- ^ Max dependency children
  -> Env
  -> Base
  -> IO ()
mweConfigs maxDepNum env mweBase = do
  forM_ (occSet env) $ \occ -> do
    when (I.mwe occ == mweBase) $ do
      occSent (sentMap env) occ >>= \case
        Nothing -> return ()
        Just sent -> do
          let marked = markOcc occ sent
              depCfg = Dep.extractDepConf maxDepNum marked
          putStr (show depCfg)
          T.putStr " => "
          T.putStr $ I.showOcc occ
          putStrLn ""


-- | Select the occurrences of the given MWE.
mweOccurrences
  :: Env  -- ^ Environment
  -> Base -- ^ MWE base form
  -> IO (S.Set I.Occ)
mweOccurrences env mweBase = do
  flip ST.execStateT S.empty $ do
    forM_ (occSet env) $ \occ -> do
      when (I.mwe occ == mweBase) $ do
        ST.liftIO (occSent (sentMap env) occ) >>= \case
          Nothing -> return ()
          Just _ -> ST.modify' (S.insert occ)


-- | Retrieve the sentence with the given occurrence.
occSent
  :: M.Map T.Text C.Sent
     -- ^ The set of Conllu sentences, to which the occurrences should correspond
  -> I.Occ
     -- ^ The occurrence
  -> IO (Maybe C.Sent)
occSent sentMap occ = do
  let rawSent = I.getRawSent occ
  case M.lookup rawSent sentMap of
    Nothing -> do
      T.putStrLn $ T.concat
        [ "WARNING: no Conllu sentence found for '"
        , rawSent
        , "'" ]
      return Nothing
    Just sent -> return $ Just sent


-----------------------------------------------------------------------
-- Data division
--
-- The goal is to divide sentences with the occurrences of the given
-- MWE into test/dev/train.
-----------------------------------------------------------------------


-- | A random shuffle of the given list.
shuffle :: [a] -> IO [a]
shuffle = RS.shuffleM


-- | A random sublist of (at most) a given length.
randomSub :: Int -> [a] -> IO [a]
randomSub k xs = take k <$> shuffle xs


-- | Split a given list into two lists, given the splitting parameter.
split2
  :: Double
     -- ^ The target relative size (w.r.t. to the total size) of the first
     -- output list; If it is not possible to obtain precisely the given
     -- relative size, the first output list will have a slightly higher
     -- relative size.
  -> [a]
  -> ([a], [a])
split2 relSize xs =
  let n = ceiling $ relSize * fromIntegral (length xs)
  in  (take n xs, drop n xs)


-- | Splitting configuration/data.
data Split a = Split
  { train :: a
  , dev   :: a
  , test  :: a
  } deriving (Show, Eq, Ord, Functor)


-- | Default splitting configuration.
defaultSplitCfg :: Split Double
defaultSplitCfg = Split
  { train = 0.3
  , dev = 0.2
  , test = 0.5
  }


-- | Split the list with respect to the given configuration.
split
  :: Split Double
     -- ^ Splitting config; all sizes are relative
  -> [a]
     -- ^ The list to split
  -> Split [a]
split splCfg xs =
  let
    (tr, rest) = split2 (train splCfg) xs
    (dv, te) = split2 (dev splCfg / (dev splCfg + test splCfg)) rest
  in
    Split tr dv te


-- | Divide sentences corresponding to the occurrences of the given MWEs into
-- train/dev/test.
divideSentences
  :: Env          -- ^ The underlying environment
  -> Int          -- ^ Maximal number of sentences per MWE
  -> Split Double -- ^ Splitting configuration (how much for train/dev/test)
  -> S.Set Base   -- ^ The MWE base forms
  -> IO ()
divideSentences env maxSentPerMwe splCfg mweSet = do
  occs <- fmap S.unions . forM (S.toList mweSet) $ \mweBase -> do
    mweOccs <-
      randomSub maxSentPerMwe . S.toList
      =<< mweOccurrences env mweBase
    return . S.fromList . map (mweBase,) $ mweOccs
  spl <- split splCfg <$> shuffle (S.toList occs)
  showOccs "TRAIN" (train spl)
  showOccs "DEV" (dev spl)
  showOccs "TEST" (test spl)
  where
    showOccs txt occs = do
      T.putStrLn $ T.concat ["# ", txt]
      forM_ (L.sortBy (comparing fst) occs) $ \(mwe, occ) -> do
        T.putStr mwe
        T.putStr " => "
        T.putStrLn (I.showOcc occ)


-- | A version of `divideSentences` which tries to maximalize the entropy of the
-- selected subset of occurrences/sentences before it divides them into
-- train/dev/test.
divideSentencesEnt
  :: Env          -- ^ The underlying environment
  -> SortCfg      -- ^ Top-level selection configuration
  -> Split Double -- ^ Splitting configuration (how much for train/dev/test)
  -> S.Set Base   -- ^ The MWE base forms
  -> IO ()
divideSentencesEnt env sortCfg splCfg mweSet = do
  -- First take the set of paris (MWE base form, MWE occurrence)
  allOccs <- forM (S.toList mweSet) $ \mweBase -> do
    mweOccs <- mweOccurrences env mweBase
    return . S.fromList . map (mweBase,) . S.toList $ mweOccs
  -- Then try to select an entropy-balanced subset
  occs <- balancedSubset
    (retryNum sortCfg)
    (targetSentNum sortCfg)
    (\(_, occ) -> getDepConf occ)
    allOccs
  -- And, finally, divide
  spl <- split splCfg <$> shuffle (S.toList occs)
  showOccs "TRAIN" (train spl)
  showOccs "DEV" (dev spl)
  showOccs "TEST" (test spl)
  where
    showOccs txt occs = do
      T.putStrLn $ T.concat ["# ", txt]
      forM_ (L.sortBy (comparing fst) occs) $ \(mwe, occ) -> do
        T.putStr mwe
        T.putStr " => "
        T.putStrLn (I.showOcc occ)
    getDepConf occ = do
      occSent (sentMap env) occ >>= \case
        Nothing -> return Nothing
        Just sent -> do
          let marked = markOcc occ sent
              depCfg = Dep.extractDepConf (maxDepNum sortCfg) marked
          return $ Just depCfg


-- -- | Another version of `divideSentences` which tries to maximalize the entropy
-- -- of the selected occurrence subsets for the individual MWEs.
-- divideSentencesEnt
--   :: Env          -- ^ The underlying environment
--   -> SortCfg      -- ^ Top-level selection configuration
--   -> Split Double -- ^ Splitting configuration (how much for train/dev/test)
--   -> S.Set Base   -- ^ The MWE base forms
--   -> IO ()
-- divideSentencesEnt env sortCfg splCfg mweSet = do
--   occs <- fmap S.unions . forM (S.toList mweSet) $ \mweBase -> do
--     mweOccs <-
--       -- randomSub maxSentPerMwe . S.toList
--       balancedSubset
--         (retryNum sortCfg)
--         (targetSentNum sortCfg)
--         (\occ -> getDepConf occ)
--         =<< mweOccurrences env mweBase
--     return . S.fromList . map (mweBase,) . S.toList $ mweOccs
--   spl <- split splCfg <$> shuffle (S.toList occs)
--   showOccs "TRAIN" (train spl)
--   showOccs "DEV" (dev spl)
--   showOccs "TEST" (test spl)
--   where
--     showOccs txt occs = do
--       T.putStrLn $ T.concat ["# ", txt]
--       forM_ (L.sortBy (comparing fst) occs) $ \(mwe, occ) -> do
--         T.putStr mwe
--         T.putStr " => "
--         T.putStrLn (I.showOcc occ)
--     getDepConf occ = do
--       occSent (sentMap env) occ >>= \case
--         Nothing -> return Nothing
--         Just sent -> do
--           let marked = markOcc occ sent
--               depCfg = Dep.extractDepConf (maxDepNum sortCfg) marked
--           return $ Just depCfg


-- | Retrieve the set of all dependency configurations, together with the
-- corresponding counts.
mwesConfigs
  :: Env          -- ^ The underlying environment
  -> SortCfg      -- ^ Top-level selection configuration
  -> S.Set Base   -- ^ The MWE base forms
  -> IO (M.Map Dep.DepConf Int)
mwesConfigs env sortCfg mweSet = do
  -- First take the set of occurrences
  allOccs <- S.unions <$> mapM (mweOccurrences env) (S.toList mweSet)
  -- Then get the set of the corresponding configurations
  M.fromListWith (+) . catMaybes <$>
    mapM getDepConf (S.toList allOccs)
  where
    getDepConf occ = do
      occSent (sentMap env) occ >>= \case
        Nothing -> return Nothing
        Just sent -> do
          let marked = markOcc occ sent
              depCfg = Dep.extractDepConf (maxDepNum sortCfg) marked
          return $ Just (depCfg, 1)


-- | Pick MWE occurrences evenly from the different dependency configuration
-- classes.
pickSentencesByClasses
  :: Env          -- ^ The underlying environment
  -> SortCfg      -- ^ Top-level selection configuration
  -> Split Double -- ^ Splitting configuration (how much for train/dev/test)
  -> S.Set Base   -- ^ The MWE base forms
  -> IO () -- (S.Set (Base, I.Occ))
pickSentencesByClasses env selCfg splCfg mweSet = do
  -- First take the set of paris (MWE base form, MWE occurrence)
  allOccs <- fmap S.unions . forM (S.toList mweSet) $ \mweBase -> do
    mweOccs <- mweOccurrences env mweBase
    return . S.fromList . map (mweBase,) . S.toList $ mweOccs
  -- Then get the map of the corresponding configurations
  depMap <- M.fromListWith (+) . catMaybes <$>
    mapM getDepConf (map snd $ S.toList allOccs)
  -- And then select occurrences
  occs <- go
    (targetSentNum selCfg) allOccs S.empty
    (loop . reverse . map fst . L.sortBy (comparing snd) $ M.toList depMap)
  -- Finally divide
  spl <- split splCfg <$> shuffle (S.toList occs)
  showOccs "TRAIN" (train spl)
  showOccs "DEV" (dev spl)
  showOccs "TEST" (test spl)
  where
    go k occLeft occSet deps
      | k <= 0 = return occSet
      | otherwise = do
          let (depCfg : deps') = deps
          pickOcc depCfg occLeft >>= \case
            Nothing ->
              go k occLeft occSet deps'
            Just occ -> do
              T.putStr (fst occ)
              T.putStr " :> "
              putStr (show depCfg)
              T.putStr " => "
              T.putStrLn . I.showOcc $ snd occ
              go (k-1) (S.delete occ occLeft) (S.insert occ occSet) deps'
    pickOcc depCfg occSet = do
      corrSet <- fmap (S.fromList . catMaybes) . forM (S.toList occSet) $
        \(mwe, occ) -> do
          mayDepCfg <- getDepConf occ
          return $ case mayDepCfg of
            Nothing -> Nothing
            Just (depCfg', _) ->
              if depCfg' == depCfg
              then Just (mwe, occ)
              else Nothing
      pick (S.toList corrSet)
    getDepConf occ = do
      occSent (sentMap env) occ >>= \case
        Nothing -> return Nothing
        Just sent -> do
          let marked = markOcc occ sent
              depCfg = Dep.extractDepConf (maxDepNum selCfg) marked
          return $ Just (depCfg, 1)
    showOccs txt occs = do
      T.putStrLn $ T.concat ["# ", txt]
      forM_ (L.sortBy (comparing fst) occs) $ \(mwe, occ) -> do
        T.putStr mwe
        T.putStr " => "
        T.putStrLn (I.showOcc occ)


-- | Forever loop the given list.
loop :: [a] -> [a]
loop xs = xs ++ loop xs


-- | Randomly pick an element from the given list (unless empty).
pick :: [a] -> IO (Maybe a)
pick [] = return Nothing
pick xs = do
  k <- R.randomRIO (0, length xs - 1)
  return . Just $ xs !! k


-- | Try to obtain a balanced subset from the given set. The input set actually
-- takes the form of a list of sets -- this is because we want to draw an equal
-- number of items from each input set.
balancedSubset
  :: (Ord a, Ord x)
  => Int
     -- ^ Retry how many times before setting down for a particular subset?
  -> Int
     -- ^ How large the subset should be?
  -> (a -> IO x)
     -- ^ A function which gives the corresponding dependency configuration
  -> [S.Set a]
     -- ^ The input set, represented as a list of sets from which an equal
     -- number of elements should be drawn
  -> IO (S.Set a)
balancedSubset retry targetSize depCfg sets = do
  go retry
  where
    elemSize = ceiling
      ( fromIntegral targetSize
        / fromIntegral (length sets) )
    go k = do
      sub1 <- trySubset
      if k > 0 then do
        sub2 <- go (k-1)
        ent1 <- setEntropy sub1
        ent2 <- setEntropy sub2
        if ent1 > ent2
          then return sub1
          else return sub2
        else return sub1
    trySubset = do
      fmap S.unions . forM sets $ \set -> do
        S.fromList . take elemSize
        <$> shuffle (S.toList set)
    setEntropy xset = do
      depConfs <- mapM depCfg (S.toList xset)
      return $
        Ent.entropy
        . fmap fromIntegral
        . Ent.count
        $ depConfs


---------------------------------------------------------------------
-- File reading / Environment
-----------------------------------------------------------------------


-- | Calculation environment.
data Env = Env
  { sentMap :: M.Map T.Text C.Sent
  , occSet  :: S.Set I.Occ
  }


-- | Extract all that is necessary to perform the calculations.
extractEnv
  :: FilePath -- ^ Silvio
  -> FilePath -- ^ Conllu
  -> IO Env
extractEnv silvioPath conlluPath = do
  occSet <- extractSilvioOccurrences silvioPath
  sentMap <- extractConlluSentences conlluPath
  return $ Env sentMap occSet


-- | Extract the map of sentences from the Conllu file. The keys of the
-- resulting map should facilitate looking up Silvio's occurrences.
extractConlluSentences :: FilePath -> IO (M.Map T.Text C.Sent)
extractConlluSentences conlluPath = do
  putStrLn "Parsing Conllu file..."
  conllu <- C.parseConlluFile conlluPath
  putStrLn "Computing sentence map..."
  let sentMap
        = M.fromList
        . map (\sent -> (C.getRawSent sent, sent))
        $ S.toList conllu
  return sentMap


-- | Extract the map of sentences from the Conllu file. The keys of the
-- resulting map should facilitate looking up Silvio's occurrences.
extractSilvioOccurrences :: FilePath -> IO (S.Set I.Occ)
extractSilvioOccurrences silvioPath = do
  putStr "Parsing Silvio's file"
  putStrLn " (keeping only IDIOMAT/BagOfDeps occurrences)..."
  occSet <- S.fromList
    . filter (\occ -> I.idiomatic occ || "BagOfDeps" `elem` I.annoMethods occ)
    . S.toList
    <$> I.parseOccFile silvioPath
  return occSet


-----------------------------------------------------------------------
-- Presense
-----------------------------------------------------------------------


-- | Check if every sentence present in Silvio's output is also present in the
-- conllu file.
checkPresense
  :: Int      -- ^ Max dependency children
  -> FilePath -- ^ Silvio
  -> FilePath -- ^ Conllu
  -> IO ()
checkPresense maxDepNum silvioPath conlluPath = do
  occSet <- extractSilvioOccurrences silvioPath
  sentMap <- extractConlluSentences conlluPath
  M.size sentMap `seq`
    putStrLn "Marking occurrences:"
  forM_ (S.toList occSet) $ \occ -> do
    let rawSent = I.getRawSent occ
    T.putStrLn rawSent
    case M.lookup rawSent sentMap of
      Nothing -> do
        T.putStrLn $ T.concat
          [ "WARNING: no Conllu sentence found for '"
          , rawSent
          , "'" ]
      Just sent -> do
        let marked = markOcc occ sent
            depCfg = Dep.extractDepConf maxDepNum marked
        print depCfg
  putStrLn "Done!"


-----------------------------------------------------------------------
-- Alignment
-----------------------------------------------------------------------


-- | Align the given occurrence with the Conllu sentence. The resulting list
-- contains information about where the MWE is.
markOcc :: I.Occ -> [C.Tok] -> [(C.Tok, Bool)]
markOcc occ sent =
  go $ A.align fst C.orth (I.sentence occ) sent
  where
    go [] = []
    go ((xs, ys) : rest) =
      [ (y, isMWE)
      | let isMWE = or (map snd xs)
      , y <- ys
      ] ++ go rest
