* Replace parents with children in the depency configurations.
  -> DONE
* Implement dividing sentences into dev/train/test
  * You will need to implement random shuffle ([a] -> [a])
    -> DONE (random-shuffly package)
  * Then a splitting function (Double -> [a] -> ([a], [a])) which, applied
    twice, will give you test/dev/train for each MWE.
    -> DONE
  * And now, you need to obtain *balanced* sets w.r.t. idiomaticity rate
    and dependency configurations.
