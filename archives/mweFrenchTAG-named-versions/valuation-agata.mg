 include header.mg
 include PredArgs.mg
 include verbes.mg
 include adjectifs.mg
 include noms.mg
 include adverbes.mg
 include misc.mg
 include mwe-agata.mg

 
% VALUATION
%%%%%%%%%%%%

%MWEs
value prendreLaPorte
value avoirLesBoules		%avoir les boules
value laNuitTombe
value allerDeLeAvant

%Mother classes
%value TopLevelClass
%value CanonicalArgument
%value CanonicalSubject
%value CanonicalnonSubjectArg
%value mweCanonicalnonSubjectArgWithDet
%value NonInvertedNominalSubject
%value RealisedNonExtractedSubject
%value SubjectAgreement
%value VerbalArgument
%value mweVerbalMorphology
%value mweactiveVerbMorphology

