%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sample encoding of MWEs, compatible with FrenchTAG
% 16/10/2017
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%
% prendre
%%%%%%%%%%%%%%%%%%%%%%%%
class prendre
import mwen0Vn1[prendre]


%%%%%%%%%%%%%%%%%%%%%%%%%
% prendre la porte
% TODO: express that "la porte" cannot be modified and that the subject is human
%%%%%%%%%%%%%%%%%%%%%%%%
class prendreLaPorte
import mwen0VDetNActive[prendre,la,porte]

%%%%%%%%%%%%%%%%%%%%%%%%%
% avoir les boules
%%%%%%%%%%%%%%%%%%%%%%%%
class avoirLesBoules
import mwen0VDetNActive[avoir,les,boules]

%%%%%%%%%%%%%%%%%%%%%%%%%
% la nuit tombe
%%%%%%%%%%%%%%%%%%%%%%%%
%class laNuitTombe
%import DetNV[la,nuit,tombe]

%%%%%%%%%%%%%%%%%%%%%%%%%
% aller au charbon
%%%%%%%%%%%%%%%%%%%%%%%%
class allerAuCharbon
import mwen0Van1DetNActive[aller,le,charbon]

%%%%%%%%%%%%%%%%%%%%%%%%%
% clouer quelqu'un au sol
%%%%%%%%%%%%%%%%%%%%%%%%
class clouerAuSol
import mwen0Vn1an2DetNActive[clouer,le,sol]

%%%%%%%%%%%%%%%%%%%%%%%%%
% pousser quelqu'un au bout
%%%%%%%%%%%%%%%%%%%%%%%%
class pousserAuBout
import mwen0Vn1an2DetNActive[pousser,le,bout]

%%%%%%%%%%%%%%%%%%%%%%%%%
% battre en retraite
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% prendre quelque chose en compte
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% clouer quelqu'un au sol
%%%%%%%%%%%%%%%%%%%%%%%%
%class clouern1ALeSol
%import n0Vn1PrepDetN[clouer,a,le,sol]


