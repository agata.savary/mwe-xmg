%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partly or fully saturated (lexicalized) vebal phrases 
% These classed are variants of Benoit's verbes.mg classed, but having lexicalized verbs and some lexicalized arguments
% They may also allow only restricted ranges of variants (e.g. diatheses)
% 16/10/2017
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBAL MORPHOLOGY

%%%%%%%%%%%%%%%%%%%%%%%%%
% Verbal morphology with no anchor - inspired by Benoit's VerbalMorphology and ActiveVerbMorphology but with mark=anchor deleted (temporary solution until we have a working lexicon)
% Implements the percolation of features between the main verb, the verbal phrase and the sentence.
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweVerbalMorphology[Lemma]
export
   xS xVN xV
declare
   ?xS ?xVN ?xV ?fU ?fV ?fW ?fX
{
        <syn>{
                node xS(color=black)[cat = s,bot=[mode=?fX]]{
                        node xVN(color=black)[cat = vn,top=[mode=?fX,neg-adv = -],bot=[neg-nom = -]]{
                                node xV(color=black)[cat = v]{
				     node (mark=flex, color=red) [cat = Lemma]
				     }
                        }
                }
        }       
}


class mweActiveVerbMorphology[Lemma] 
import
	mweVerbalMorphology[Lemma] 
declare 
	?fX ?fY ?fZ ?fW ?fS ?fT ?fO ?fP ?fK ?fG
{
	<syn>{
		node xVN[top=[neg-nom = ?fG],bot=[mode=?fX, num = ?fY,gen = ?fZ,pers=?fW,pp-gen = ?fS,pp-num=?fT,inv = ?fO,neg-adv = ?fK]]{
			node xV[top=[mode=?fX,num = ?fY,gen = ?fZ,pers=?fW,inv = ?fO,pp-gen= ?fS, pp-num = ?fT,neg-adv = ?fK,neg-nom = ?fG],bot=[inv = -, aux-refl= -]]
		}			
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBAL VALENCY

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs fragment in active voice, with a non-lexicalized subject, a lexicalized verb and a non-lexicalized direct object 
% e.g. prendre
% Based on dian0Vn1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1[lemmaV] 
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	CanonicalObject[]			%The direct object is not lexicalized
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb and lexicalized direct object 
% e.g. prendre la porte
% Based on dian0Vn1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0VDetNActive[lemmaV,lemmaDet,lemmaN] 
declare
	?lexObj ?lexNP
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	?lexObj = mweCanonicalObjectLex[];	%The direct object is lexicalized (does not expect a substitution)
	?lexNP = mweDetNoun[lemmaDet,lemmaN];   %Lexicalized Det+N phrase
        <syn>{
		
		?lexObj.xtop=?lexNP.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb and lexicalized indirect object 
% e.g. aller au charbon
% Based on dian0Van1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Van1DetNActive[lemmaV,lemmaDet,lemmaN] 
declare
	?lexIObj ?lexNP
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	?lexIObj = mweCanonicalIObjectLex[];	%The indirect object is lexicalized (does not expect a substitution)
	?lexNP = mweDetNoun[lemmaDet,lemmaN];   %Lexicalized Det+N phrase
        <syn>{		
		?lexIObj.xArg=?lexNP.xN		%Unifying the subject with the lexicalized Det+N phrase
						%In lexNP: node xN(color=red)[cat = n,bot=[det = +,pers = 3,wh = -,bar = 0]]{
						%In lexIObj (from mweCanonicalPPLex): node xArg(color=red)[cat = n, top = [det = +]] 
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb, a no-lexicalized direct object and a lexicalized indirect object 
% e.g. clouer quelqu'un au sol, pousser quelqu'un au bout
% TDO: add the passive voice: il a été poussé au bout
% Based on n0Vn1an2
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1an2DetNActive[lemmaV,lemmaDet,lemmaN] {
	mwen0Van1DetNActive[lemmaV,lemmaDet,lemmaN];
	CanonicalObject[]
}


