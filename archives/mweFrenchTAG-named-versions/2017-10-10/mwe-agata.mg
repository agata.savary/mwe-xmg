%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sample encoding of MWEs, compatible with FrenchTAG
% 4/10/2017
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%
% Verbal morphology with no anchor - inspired from Benoit's grammar but with mark=anchor deleted (temporary solution until we have a working lexicon)
% Implements the percolation of features between the main verb, the verbal phrase and the sentence.
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweVerbalMorphology[Lemma]
export
   xS xVN xV
declare
   ?xS ?xVN ?xV ?fU ?fV ?fW ?fX
{
        <syn>{
                node xS(color=black)[cat = s,bot=[mode=?fX]]{
                        node xVN(color=black)[cat = vn,top=[mode=?fX,neg-adv = -],bot=[neg-nom = -]]{
                                node xV(color=black)[cat = v]{
				     node (mark=flex, color=red) [cat = Lemma]
				     }
                        }
                }
        }       
}


class mweActiveVerbMorphology[Lemma]
import
	mweVerbalMorphology[Lemma] 
declare 
	?fX ?fY ?fZ ?fW ?fS ?fT ?fO ?fP ?fK ?fG
{
	<syn>{
		node xVN[top=[neg-nom = ?fG],bot=[mode=?fX, num = ?fY,gen = ?fZ,pers=?fW,pp-gen = ?fS,pp-num=?fT,inv = ?fO,neg-adv = ?fK]]{
			node xV[top=[mode=?fX,num = ?fY,gen = ?fZ,pers=?fW,inv = ?fO,pp-gen= ?fS, pp-num = ?fT,neg-adv = ?fK,neg-nom = ?fG],bot=[inv = -, aux-refl= -]]
		}			
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Det and N
% Based on Benoit's noun withour mark=anchor
%%%%%%%%%%%%%%%%%%%%%%%%
class mweDetNoun[lemmaDet, lemmaN]
export
	?xN
declare
	?xN
{
	<syn>{
		node xN(color=red)[cat = n,bot=[det = -,pers = 3,wh = -,bar = 0]]{
			node (color=red) [cat = det] {
				node (mark=flex, color=red) [cat = lemmaDet]
			}
			node (color=red) [cat = n]  {
				node (mark=flex, color=red) [cat = lemmaN]
			}
		}	
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs with a lexicalized verb and direct object 
%%%%%%%%%%%%%%%%%%%%%%%%
class n0VDetN[lemmaV,lemmaDet,lemmaN]
import 
	CanonicalSubject[]
        CanonicalnonSubjectArg[]
declare
	?lexObj
{
	mweActiveVerbMorphology[lemmaV];	%Percolating features from the verb to the verb phrase and the sentence
	?lexObj = mweDetNoun[lemmaDet,lemmaN];  %Lexicalized Det+N phrase
        <syn>{
		
		?xtop=?lexObj.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs with a lexicalized subject and verb
%%%%%%%%%%%%%%%%%%%%%%%%
class DetNV[lemmaDet,lemmaN,lemmaV]
import 
	RealisedNonExtractedSubject[]		%Subject that expects no substitution
declare
	?lexSubj
{
	mweActiveVerbMorphology[lemmaV];	%Percolating features from the verb to the verb phrase and the sentence
	?lexSubj = mweDetNoun[lemmaDet, lemmaN];  %Lexicalized Det+N phrase
        <syn>{
		?xSubj=?lexSubj.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% prendre la porte
% TODO: express that "la porte" cannot be modified and that the subject is human
%%%%%%%%%%%%%%%%%%%%%%%%
class prendreLaPorte
import n0VDetN[prendre,la,porte]

%%%%%%%%%%%%%%%%%%%%%%%%%
% avoir les boules
%%%%%%%%%%%%%%%%%%%%%%%%
class avoirLesBoules
import n0VDetN[avoir,les,boules]

%%%%%%%%%%%%%%%%%%%%%%%%%
% la nuit tombe
%%%%%%%%%%%%%%%%%%%%%%%%
class laNuitTombe
import DetNV[la,nuit,tombe]



