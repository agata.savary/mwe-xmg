%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sample encoding of MWEs, compatible with FrenchTAG
% 4/10/2017
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%
% Verbal morphology with no anchor - inspired by Benoit's VerbalMorphology and ActiveVerbMorphology but with mark=anchor deleted (temporary solution until we have a working lexicon)
% Implements the percolation of features between the main verb, the verbal phrase and the sentence.
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweVerbalMorphology[Lemma]
export
   xS xVN xV
declare
   ?xS ?xVN ?xV ?fU ?fV ?fW ?fX
{
        <syn>{
                node xS(color=black)[cat = s,bot=[mode=?fX]]{
                        node xVN(color=black)[cat = vn,top=[mode=?fX,neg-adv = -],bot=[neg-nom = -]]{
                                node xV(color=black)[cat = v]{
				     node (mark=flex, color=red) [cat = Lemma]
				     }
                        }
                }
        }       
}


class mweActiveVerbMorphology[Lemma]
import
	mweVerbalMorphology[Lemma] 
declare 
	?fX ?fY ?fZ ?fW ?fS ?fT ?fO ?fP ?fK ?fG
{
	<syn>{
		node xVN[top=[neg-nom = ?fG],bot=[mode=?fX, num = ?fY,gen = ?fZ,pers=?fW,pp-gen = ?fS,pp-num=?fT,inv = ?fO,neg-adv = ?fK]]{
			node xV[top=[mode=?fX,num = ?fY,gen = ?fZ,pers=?fW,inv = ?fO,pp-gen= ?fS, pp-num = ?fT,neg-adv = ?fK,neg-nom = ?fG],bot=[inv = -, aux-refl= -]]
		}			
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Det and N
% Based on Benoit's "noun" class without mark=anchor
%%%%%%%%%%%%%%%%%%%%%%%%
class mweDetNoun[lemmaDet, lemmaN]
export
	?xN
declare
	?xN
{
	<syn>{
		node xN(color=red)[cat = n,bot=[det = -,pers = 3,wh = -,bar = 0]]{
			node (color=red) [cat = det] {
				node (mark=flex, color=red) [cat = lemmaDet]
			}
			node (color=red) [cat = n]  {
				node (mark=flex, color=red) [cat = lemmaN]
			}
		}	
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Prep, Det and N
% Based on Benoit's CanonicalPP without mark=subst
%%%%%%%%%%%%%%%%%%%%%%%%
class mwePrepDetNoun[lemmaPrep, lemmaDet, lemmaN]
import
        CanonicalnonSubjectArg[]
export
        xArg xX
declare
        ?xArg ?xX ?ppObj
{
	?ppObj = mweDetNoun[lemmaDet,lemmaN];  %Lexicalized Det+N phrase
        <syn>{
                node xtop[cat = pp]{
                        node xX (color=red)[cat = p] {
				node (color=red) [cat = lemmaPrep]
			}
                        node xArg(color=red)[cat = n, top = [det = +]] 
                };
		?xArg=?ppObj.xN		%Unifying the preposition's argument with the lexicalized Det+N phrase  
        }
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs with a lexicalized verb and direct object 
%%%%%%%%%%%%%%%%%%%%%%%%
class n0VDetN[lemmaV,lemmaDet,lemmaN]
import 
	CanonicalSubject[]
        CanonicalnonSubjectArg[]
declare
	?lexObj
{
	mweActiveVerbMorphology[lemmaV];	%Percolating features from the verb to the verb phrase and the sentence
	?lexObj = mweDetNoun[lemmaDet,lemmaN];  %Lexicalized Det+N phrase
        <syn>{
		
		?xtop=?lexObj.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs with a lexicalized verb and pp object 
%%%%%%%%%%%%%%%%%%%%%%%%
class n0VPrepDetN[lemmaV,lemmaPrep,lemmaDet,lemmaN]
import 
	CanonicalSubject[]
export PP
declare ?PrepDetNoun ?PP
{
	?PrepDetNoun=mwePrepDetNoun[lemmaPrep, lemmaDet, lemmaN];
	?PP=PrepDetNoun.xtop;
	mweActiveVerbMorphology[lemmaV]	%Percolating features from the verb to the verb phrase and the sentence
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs with a lexicalized verb and pp object, and unlexicalized direct object
%%%%%%%%%%%%%%%%%%%%%%%%
class n0Vn1PrepDetN[lemmaV,lemmaPrep,lemmaDet,lemmaN]
import 
	n0VPrepDetN[lemmaV,lemmaPrep,lemmaDet,lemmaN]
	CanonicalObject[]
{
  <syn>{
    ?xtop >> ?PP}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs with a lexicalized subject and verb
%%%%%%%%%%%%%%%%%%%%%%%%
class DetNV[lemmaDet,lemmaN,lemmaV]
import 
	RealisedNonExtractedSubject[]		%Subject that expects no substitution
declare
	?lexSubj
{
	mweActiveVerbMorphology[lemmaV];	%Percolating features from the verb to the verb phrase and the sentence
	?lexSubj = mweDetNoun[lemmaDet, lemmaN];  %Lexicalized Det+N phrase
        <syn>{
		?xSubj=?lexSubj.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% prendre la porte
% TODO: express that "la porte" cannot be modified and that the subject is human
%%%%%%%%%%%%%%%%%%%%%%%%
class prendreLaPorte
import n0VDetN[prendre,la,porte]

%%%%%%%%%%%%%%%%%%%%%%%%%
% avoir les boules
%%%%%%%%%%%%%%%%%%%%%%%%
class avoirLesBoules
import n0VDetN[avoir,les,boules]

%%%%%%%%%%%%%%%%%%%%%%%%%
% la nuit tombe
%%%%%%%%%%%%%%%%%%%%%%%%
class laNuitTombe
import DetNV[la,nuit,tombe]

%%%%%%%%%%%%%%%%%%%%%%%%%
% aller de l'avant
%%%%%%%%%%%%%%%%%%%%%%%%
class allerDeLeAvant
import n0VPrepDetN[aller,de,le,avant]

%%%%%%%%%%%%%%%%%%%%%%%%%
% battre en retraite
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% prendre quelque chose en compte
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
% clouer quelqu'un au sol
%%%%%%%%%%%%%%%%%%%%%%%%
class clouern1ALeSol
import n0Vn1PrepDetN[clouer,a,le,sol]


