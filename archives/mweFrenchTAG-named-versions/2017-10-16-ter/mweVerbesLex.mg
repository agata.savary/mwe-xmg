%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partly or fully saturated (lexicalized) vebal phrases 
% These classed are variants of Benoit's verbes.mg classed, but having lexicalized verbs and some lexicalized arguments
% They may also allow only restricted ranges of variants (e.g. diatheses)
% 16/10/2017
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBAL MORPHOLOGY

%%%%%%%%%%%%%%%%%%%%%%%%%
% Verbal morphology with no anchor - inspired by Benoit's VerbalMorphology and ActiveVerbMorphology but with mark=anchor deleted (temporary solution until we have a working lexicon)
% Implements the percolation of features between the main verb, the verbal phrase and the sentence.
%%%%%%%%%%%%%%%%%%%%%%%%%
class mweVerbalMorphology[Lemma]
export
   xS xVN xV
declare
   ?xS ?xVN ?xV ?fU ?fV ?fW ?fX
{
        <syn>{
                node xS(color=black)[cat = s,bot=[mode=?fX]]{
                        node xVN(color=black)[cat = vn,top=[mode=?fX,neg-adv = -],bot=[neg-nom = -]]{
                                node xV(color=black)[cat = v]{
				     node (mark=flex, color=red) [cat = Lemma]
				     }
                        }
                }
        }       
}


class mweActiveVerbMorphology[Lemma] 
import
	mweVerbalMorphology[Lemma] 
declare 
	?fX ?fY ?fZ ?fW ?fS ?fT ?fO ?fP ?fK ?fG
{
	<syn>{
		node xVN[top=[neg-nom = ?fG],bot=[mode=?fX, num = ?fY,gen = ?fZ,pers=?fW,pp-gen = ?fS,pp-num=?fT,inv = ?fO,neg-adv = ?fK]]{
			node xV[top=[mode=?fX,num = ?fY,gen = ?fZ,pers=?fW,inv = ?fO,pp-gen= ?fS, pp-num = ?fT,neg-adv = ?fK,neg-nom = ?fG],bot=[inv = -, aux-refl= -]]
		}			
	}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERBAL VALENCY

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs fragment in active voice, with a non-lexicalized subject, a lexicalized verb and a non-lexicalized direct object 
% e.g. prendre
% Based on dian0Vn1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1[lemmaV] 
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	CanonicalObject[]			%The direct object is not lexicalized
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs fragment in active voice, with a non-lexicalized subject, a lexicalized verb, a non-lexicalized direct object and a non-lexicalized indirect object
% e.g. donner
% Based on dian0Vn1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1an2[lemmaV] 
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	CanonicalObject[];			%The direct object is not lexicalized
	CanonicalIobject[]			%The indirect object is not lexicalized
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb and lexicalized direct object 
% e.g. prendre la porte
% Based on dian0Vn1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0VDetNActive[lemmaV,lemmaDet,lemmaN] 
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	mweCanonicalObjectLexDetN[lemmaDet,lemmaN] %The direct object is lexicalized (does not expect a substitution)
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb and lexicalized indirect object 
% e.g. aller au charbon
% Based on dian0Van1Active
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0VaDetNActive[lemmaV,lemmaDet,lemmaN] 
{
	CanonicalSubject[];  			%The subject is not lexicalized
	mweActiveVerbMorphology[lemmaV];	%The verb is lexicalized
	mweCanonicalIObjectLexDetN[lemmaDet,lemmaN]	%The indirect object is lexicalized (with a Det-N pharse)
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb, a no-lexicalized direct object and a lexicalized indirect object 
% e.g. clouer quelqu'un au sol, pousser quelqu'un au bout
% TDO: add the passive voice: il a été poussé au bout
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0Vn1aDetNActive[lemmaV,lemmaDet,lemmaN] {
	mwen0VaDetNActive[lemmaV,lemmaDet,lemmaN];
	CanonicalObject[]
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a non-lexicalized subject, a lexicalized verb, a lexicalized direct object and a lexicalized indirect object 
% e.g. joindre le geste à la parole
%%%%%%%%%%%%%%%%%%%%%%%%
class mwen0VDetNaDetNActive[lemmaV,lemmaDet1,lemmaN1,lemmaDet2,lemmaN2] {
	mwen0VDetNActive[lemmaV,lemmaDet1,lemmaN1];  %joindre le geste
	mweCanonicalIObjectLexDetN[lemmaDet2,lemmaN2] %joindre à la parole
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% MWEs only in active voice, with a lexicalized subject 
% e.g. la nuit tombe à quelqu'un
% No model: why?
%%%%%%%%%%%%%%%%%%%%%%%%
class mweDetNVActive[lemmaDet,lemmaN,lemmaV] 
{
	mweCanonicalSubjectLexDetN[lemmaDet,lemmaN];  		%The subject is lexicalized
	mweActiveVerbMorphology[lemmaV]				%The verb is lexicalized
}


