%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lexicalized predicate arguments
% These classed are variants of Benoit's PredArgs.mg classes, but expecting no substitution and allowing for restricted ranges of variants (e.g. diatheses)
% 16/10/2017
% by Agata Savary and Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBJECT

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized subject (not expecting a substitution)
% Based on CanonicalSubject (without mark=subst)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalSubjectLex
import
        RealisedNonExtractedSubject[]
declare 
	?fG ?fK
{
        <syn>{
	     node xVN [top=[neg-nom = ?fG],bot=[neg-adv = ?fK]];
             node xSubj[cat = n,top=[neg-nom = ?fG,neg-adv = ?fK, wh = -,func=suj]]
	 }
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical subject lexicalized with a Det-N group
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalSubjectLexDetN[lemmaDet,lemmaN]
import
	mweCanonicalSubjectLex[]
declare
	?lexSubj ?lexNP
{
	?lexSubj = mweCanonicalSubjectLex[];
	?lexNP = mweDetNoun[lemmaDet,lemmaN];   %Lexicalized Det+N phrase
        <syn>{
		
		?lexSubj.xSubj=?lexNP.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DIRECT OBJECT

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized direct object (not expecting a substitution)
% Based on CanonicalObject (without mark=subst)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalObjectLex
import
        CanonicalnonSubjectArg[]
declare 
	?fK ?fG
{
        <syn>{
		node xVN[top=[neg-nom = ?fK],bot=[neg-adv = ?fG]];
                node xtop[cat = n,top=[det = +,neg-adv = ?fG,neg-nom = ?fK,func=obj]]
        }
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical direct object lexicalized with a Det-N group
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalObjectLexDetN[lemmaDet,lemmaN]
declare
	?lexObj ?lexNP
{
	?lexObj = mweCanonicalObjectLex[];
	?lexNP = mweDetNoun[lemmaDet,lemmaN];   %Lexicalized Det+N phrase
        <syn>{
		
		?lexObj.xtop=?lexNP.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INDIRECT OBJECT

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized PP (not expecting a substitution)
% Based on CanonicalPP (without mark=subst)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalPPLex
import
        CanonicalnonSubjectArg[]
export
        xArg xX
declare
        ?xArg ?xX ?fG ?fK
{
        <syn>{
                node xtop[cat = pp]{
                        node xX (color=red)[cat = p]
                        node xArg(color=red)[cat = n, top = [det = +]]  

                }
        };
	<syn>{
		node xArg[top = [neg-adv = ?fG,neg-nom = ?fK]];
		node xVN [top = [neg-nom = ?fK],bot = [neg-adv = ?fG]]
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized non oblique PP (not expecting a substitution)
% Based on CanonicalPPnonOblique (imports mweCanonicalPPLex instead of CanonicalPP)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalPPnonObliqueLex
import 
	mweCanonicalPPLex[]
export 
	xPrep
declare
	?xPrep
{
	<syn>{
		node xX{
			node xPrep(color=red,mark=flex)
		}
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical lexicalized indirect object (not expecting a substitution)
% Based on CanonicalIobject (imports mweCanonicalPPnonObliqueLex instead of CanonicalPPnonOblique)
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalIObjectLex
import 
	mweCanonicalPPnonObliqueLex[]

{
	<syn>{
		node xPrep[cat = à];
		node xArg[top = [func = iobj]]	
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Canonical indirect object lexicalized with a Det-N group
%%%%%%%%%%%%%%%%%%%%%%%%
class mweCanonicalIObjectLexDetN[lemmaDet,lemmaN]
declare 
	?lexIObj ?lexNP
{
	?lexIObj = mweCanonicalIObjectLex[];
	?lexNP = mweDetNoun[lemmaDet,lemmaN];   %Lexicalized Det+N phrase
        <syn>{
		
		?lexIObj.xArg=?lexNP.xN		%Unifying the subject with the lexicalized Det+N phrase
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



