%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partly or fully saturated (lexicalized) noun phrases 
% These classed are variants of Benoit's noms.mg classed but having lexicalized noun heads and modifiers
% 16/10/2017
% by Agata Savary, Simon Petitjean
%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%
% Any argument with lexicalized Det and N
% Based on Benoit's "noun" class without mark=anchor and with det=+
%%%%%%%%%%%%%%%%%%%%%%%%
class mweDetNoun[lemmaDet, lemmaN]
export
	?xN
declare
	?xN
{
	<syn>{
		node xN(color=red)[cat = n,bot=[det = +,pers = 3,wh = -,bar = 0]]{
			node (color=red) [cat = det] {
				node (mark=flex, color=red) [cat = lemmaDet]
			}
			node (color=red) [cat = n]  {
				node (mark=flex, color=red) [cat = lemmaN]
			}
		}	
	}
}




