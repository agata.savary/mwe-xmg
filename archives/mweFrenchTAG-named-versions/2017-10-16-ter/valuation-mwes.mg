 include header.mg
 include PredArgs.mg
 include verbes.mg
 include adjectifs.mg
 include noms.mg
 include adverbes.mg
 include misc.mg
 include mweVerbesLex.mg
 include mwePredArgsLex.mg
 include mweNomsLex.mg
 include mwes.mg
% include test.mg

 
% VALUATION
%%%%%%%%%%%%

%MWEs
value prendre
value donner
value prendreLaPorte
value avoirLesBoules		%avoir les boules
value allerAuCharbon
value clouerAuSol
value pousserAuBout
value joindreLeGesteALaParole
value laNuitTombe


%Mother classes
%value TopLevelClass
%value CanonicalArgument
%value CanonicalSubject
%value CanonicalnonSubjectArg
%value mweCanonicalnonSubjectArgWithDet
%value NonInvertedNominalSubject
%value RealisedNonExtractedSubject
%value SubjectAgreement
%value VerbalArgument
%value mweVerbalMorphology
%value mweactiveVerbMorphology

